<?php


if (!defined('_PS_VERSION_')) {
    exit;
}

class devismod extends PaymentModule
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'devismod';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'brunorafaelap@gmail.com';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Enosis devis');
        $this->description = $this->l('Ce module vous permet d\'accepter les devis sur votre boutique Prestashop.');

        $this->confirmUninstall = $this->l('Êtes-vous sûrs de vouloir supprimer ces détails ?');

        $this->limited_countries = array('FR');
        $this->limited_currencies = array('EUR');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false)
        {
            $this->_errors[] = $this->l('This module is not available in your country');
            return false;
        }
        

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            mkdir(_PS_DOWNLOAD_DIR_.$this->name, 0755) &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('actionPaymentConfirmation') &&
            $this->registerHook('displayAdminOrderContentOrder') &&
            Configuration::updateValue('DEVISMOD_EMAIL', Configuration::get('PS_SHOP_EMAIL')) &&
            Configuration::updateValue('DEVISMOD_MODE', false) &&
            $this->registerHook('displayPaymentReturn');
    }

    public function uninstall()
    {

        if (file_exists(_PS_DOWNLOAD_DIR_.$this->name))
        {
            $folder = new DirectoryIterator(_PS_DOWNLOAD_DIR_.$this->name);
            foreach($folder as $file)
                if($file->isFile() && !$file->isDot())
                    unlink($file->getPathname());
                    
            rmdir(_PS_DOWNLOAD_DIR_.$this->name);
        }

        Configuration::deleteByName('DEVISMOD_MODE');
        Configuration::deleteByName('DEVISMOD_EMAIL'); 

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /*public function clearCache()
    {
        parent::_clearCache('backOfficeHeader.tpl', 'template_1');
        
    }*/

    /**
     * Load the configuration form
     */
    public function getContent()
    {
    

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submit'.$this->name)) == true) {
            $this->postProcess();

        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit'.$this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'DEVISMOD_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'label' => $this->l('Email'),
                        'desc' => $this->l('Enter a valid email address for receive customer request.'),
                        'name' => 'DEVISMOD_EMAIL',
                        'size' => 60,
                        'required' => true
                    ),                   
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    
                ),
            ),            
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        
        return array(
            'DEVISMOD_MODE' => Configuration::get('DEVISMOD_MODE'),
            'DEVISMOD_EMAIL' => Configuration::get('DEVISMOD_EMAIL'),
            
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }
    
    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
            return false;

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        if ($this->active == false)
            return;

        $order = $params['objOrder'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR'))
            $this->smarty->assign('status', 'ok');

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    public function hookActionPaymentConfirmation()
    {
        /* Place your code here. */
    }

    public function hookDisplayAdminOrderContentOrder()
    {
        /* Place your code here. */
    }

    public function hookDisplayPaymentReturn()
    {
        /* Place your code here. */
    }
}
