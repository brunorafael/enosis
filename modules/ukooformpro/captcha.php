<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../config/settings.inc.php');
require_once(dirname(__FILE__).'/../../classes/Cookie.php');

$captcha = new Captcha(Tools::getValue('background'), Tools::getValue('text'));

class Captcha
{

	public function __construct($background, $text)
	{
		$mot = $this->random();
		$captcha = new Cookie('captcha');
		$captcha->__set('value', $mot);
		$captcha->write();
		$this->captchas($mot, $background, $text);
	}

	private function random($characters = 6, $letters = '23456789ABCDFGHJKMNPQRSTVWYZ')
	{
		$str = '';
		for ($i = 0; $i < $characters; $i++)
			$str .= Tools::substr($letters, mt_rand(0, Tools::strlen($letters) - 1), 1);
		return $str;
	}

	private function captchas($mot, $background = '#ffffff', $text = '#000000')
	{
		$background = $this->hex2rgb($background);
		$text = $this->hex2rgb($text);

		$size = 30;
		$marge = 25;
		$font = './views/fonts/Acidic.TTF';

//		Utiliser pour le flou gaussien
//		$matrix_blur = array(
//			array(1, 1, 1),
//			array(1, 1, 1),
//			array(1, 1, 1));

		$box = imagettfbbox($size, 0, $font, $mot);
		$largeur = $box[2] - $box[0];
		$hauteur = $box[1] - $box[7];
		$largeur_lettre = round($largeur / Tools::strlen($mot));

		$img = imagecreate($largeur + $marge, $hauteur + $marge);
		imagecolorallocate($img, $background[0], $background[1], $background[2]);
		$noir = imagecolorallocate($img, $text[0], $text[1], $text[2]);

		$couleur = array(
//			Cooleur du text
//			imagecolorallocate($img, 0x99, 0x00, 0x66),
//			imagecolorallocate($img, 0xCC, 0x00, 0x00),
//			imagecolorallocate($img, 0x00, 0x00, 0xCC),
//			imagecolorallocate($img, 0x00, 0x00, 0xCC),
//			imagecolorallocate($img, 0xBB, 0x88, 0x77),
			imagecolorallocate($img, $text[0], $text[1], $text[2])
		);

		$mot_strlen = Tools::strlen($mot);
		for ($i = 0; $i < $mot_strlen; ++$i)
		{
			$l = $mot[$i];
			$angle = mt_rand(-35, 35);
			imagettftext($img, mt_rand($size - 7, $size),
				$angle,
				($i * $largeur_lettre) + $marge, $hauteur + mt_rand(0, $marge / 2),
				$couleur[array_rand($couleur)],
				$font, $l);
		}

		imageline($img, 2, mt_rand(2, $hauteur), $largeur + $marge, mt_rand(2, $hauteur), $noir);
		imageline($img, 2, mt_rand(2, $hauteur), $largeur + $marge, mt_rand(2, $hauteur), $noir);

//		Flou gaussien
//		imageconvolution($img, $matrix_blur, 9, 0);
//		imageconvolution($img, $matrix_blur, 9, 0);

		imagepng($img);
		imagedestroy($img);

		die();
	}

	private function hex2rgb($hex)
	{
		$hex = str_replace('#', '', $hex);

		if (Tools::strlen($hex) == 3)
		{
			$r = hexdec(Tools::substr($hex, 0, 1).Tools::substr($hex, 0, 1));
			$g = hexdec(Tools::substr($hex, 1, 1).Tools::substr($hex, 1, 1));
			$b = hexdec(Tools::substr($hex, 2, 1).Tools::substr($hex, 2, 1));
		}
		else
		{
			$r = hexdec(Tools::substr($hex, 0, 2));
			$g = hexdec(Tools::substr($hex, 2, 2));
			$b = hexdec(Tools::substr($hex, 4, 2));
		}
		$rgb = array($r, $g, $b);
		//return implode(",", $rgb); // returns the rgb values separated by commas
		return $rgb; // returns an array with the rgb values
	}

}

?>