<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT / Guillaume Heid - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 *
 * "In Ukoo we trust!"
 *
 *  V1.1.4 - changelog
 *		[-] EmailValidator
 *		[+] Ajout wysiwig Champ text libre, template email html
 *  V1.1.3 - changelog
 *		[*] Correction captcha
 *		[*] Modification du champ default_Value en TEXT
 *		[*] Correction affichage des attributs cols et rows dans la modale
 *		[*] Correction affichage des attributs class dans la modale
 *		[*] Correction affichage class dans les selects et radios front office
 *
 *  V1.1.2 - changelog [2015-04-17]
 *		[*] Correction erreurs lors de l'installation du module sur une boutique dons la langue est différent du français et de l'anglais
 *		[*]	Correction des enregistrements des templates de mails sur boutique multilangue avec des langues désactivées
 *
 *  V1.1.1 - changelog [2015-04-01]
 *		[*] Ajout d'un fonction de la fonction EmailValidator
 *
 *  v1.1.0 - changelog (2015-02-16)
 *		[*] Correction compatiblité php < 5.4
 *		[*] Ajout de fonctionnalité export de donnés des soumissions par formulaire
 *		[*] Ajout de htaccess dans le dossier des fichiers
 *		[*] Correction de la fonction cookie des captchas suite à mise à jour Prestashop 1.6.0.14
 *		[*] Mise en place envoie d'email multiple, et de bcc.
 *		[*] Modification de la fonction de suppression des formulaires
 *
 *  v1.0.2 - changelog (2015-01-08)
 *		[*] Correction des attributs des formulaires
 *		[*] Corrections de bugs mineurs dans les tpl FO et BO
 *		[*] Appel à l'url FO des formulaires à travers la classe Link::getModuleLink()
 *		[*] Passage au validateur W3C
 *		[*] Correction empêchant l'affichage des textes libre une fois le formulaire soumis
 *		[*] Correction des liens d'appel aux fichiers CSS pour résoudre les soucis du CCC et de l'override
 *		[/] Modifications des traductions
 *      [+] Compatible jusqu'à PS1.6.11
 *
 *  v1.0.1 - changelog (2015-01-06)
 *      [*] Corrections de sécurité (cast des variables dans les requêtes SQL)
 *		[*] Correction dans le fichier JS du BO
 *		[*] Corrections de traductions dans le menu BO
 *		[*] Les objets des emails client et admin n'étaient pas enregistrés dans les bons champs en BDD
 *		[*] Corrections de bugs mineurs dans les tpl FO et BO
 *		[*] Correction d'un bug sur les checkboxes, radio et select lorsque le label était un entier
 *		[/] Le fichier css du front était appelé dans le fichier TPL, à présent appelé greffé au hook header
 *		[+] Ajout d'un fichier CSS dans le BO
 *		[-] Suppression des champs admin BCC et client BCC (non-utilisé par la classe Mail de presta)
 *
 *  v1.0 - changelog
 *      [+] Première version stable
 */

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__).'/controllers/admin/AdminUkooFormProManagementController.php';
require_once dirname(__FILE__).'/controllers/admin/AdminParentUkooFormProManagementController.php';
require_once dirname(__FILE__).'/controllers/admin/AdminUkooFormProElementsController.php';
require_once dirname(__FILE__).'/controllers/admin/AdminUkooFormProSubmissionController.php';
require_once dirname(__FILE__).'/controllers/admin/AdminUkooFormProDownloadFileController.php';
require_once dirname(__FILE__).'/classes/Form.php';
require_once dirname(__FILE__).'/classes/Elements.php';
require_once dirname(__FILE__).'/classes/Submission.php';
require_once dirname(__FILE__).'/classes/UkooFormProFile.php';
require_once dirname(__FILE__).'/classes/UkooFormProExport.php';
require_once dirname(__FILE__).'/controllers/front/downloadfile.php';
require_once dirname(__FILE__).'/controllers/front/form.php';

class UkooFormPro extends Module
{

	public function __construct()
	{
		$this->name = 'ukooformpro';
		$this->tab = 'administration';
		$this->version = '1.1.4';
		$this->author = 'Ukoo';
		$this->module_key = 'd2ec00b20ef4fe8043793f27f02405a4';
		$this->need_instance = 1;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Ukoo Form Pro');
		$this->description = $this->l('This Module can easily create and personalize contact form');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}

	public function install()
	{
		if (!parent::install()
			|| !AdminParentUkooFormProManagementController::menuInstall()
			|| !AdminUkooFormProManagementController::installInBO()
			|| !AdminUkooFormProElementsController::installInBO()
			|| !AdminUkooFormProSubmissionController::installInBO()
			|| !AdminUkooFormProDownloadFileController::installInBO()
			|| !Form::createDBTableForm()
			|| !Elements::createDBTableElements()
			|| !Submission::createDBTableSubmission()
			|| !Configuration::updateValue('UKOOFORMPRO_0', Tools::strtoupper(Tools::passwdGen(16)))
			|| !$this->registerHook('displayBackOfficeHeader'))
			return false;
		else
			return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall()
			|| !UkooFormProFile::removeAllFille()
			|| !AdminParentUkooFormProManagementController::menuUninstall()
//			|| !AdminUkooFormProManagementController::removeFromBO()
//			|| !AdminUkooFormProElementsController::removeFromBO()
//			|| !AdminUkooFormProSubmissionController::removeFromBO()
			|| !Form::removeDBTableForm()
			|| !Elements::removeDBTableElements()
			|| !Submission::removeDBTableSubmission()
			|| !Configuration::deleteByName('UKOOFORMPRO_0'))
			return false;
		else
			return true;
	}

	public function hookDisplayBackOfficeHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/adminukooformpro.css');

	}
}

