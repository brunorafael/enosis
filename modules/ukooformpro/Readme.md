# UkooFormPro

## About

Create your personnal forms in front office

 *  V1.1.4 - changelog
 *		[-] EmailValidator
 *		[+] Ajout wysiwig Champ text libre, template email html
 *  V1.1.3 - changelog
 *		[*] Correction captcha
 *		[*] Modification du champ default_Value en TEXT
 *		[*] Correction affichage des attributs cols et rows dans la modale
 *		[*] Correction affichage des attributs class dans la modale
 *		[*] Correction affichage class dans les selects et radios front office
 *
 *  V1.1.2 - changelog [2015-04-17]
 *		[*] Correction erreurs lors de l'installation du module sur une boutique dons la langue est différent du français et de l'anglais
 *		[*]	Correction des enregistrements des templates de mails sur boutique multilangue avec des langues désactivées
 *
 *  V1.1.1 - changelog [2015-04-01]
 *		[*] Ajout d'un fonction de la fonction EmailValidator
 *
 *  v1.1.0 - changelog (2015-02-16)
 *		[*] Correction compatiblité php < 5.4
 *		[*] Ajout de fonctionnalité export de donnés des soumissions par formulaire
 *		[*] Ajout de htaccess dans le dossier des fichiers
 *		[*] Correction de la fonction cookie des captchas suite à mise à jour Prestashop 1.6.0.14
 *		[*] Mise en place envoie d'email multiple, et de bcc.
 *		[*] Modification de la fonction de suppression des formulaires
 *
 *  v1.0.2 - changelog (2015-01-08)
 *		[*] Correction des attributs des formulaires
 *		[*] Corrections de bugs mineurs dans les tpl FO et BO
 *		[*] Appel à l'url FO des formulaires à travers la classe Link::getModuleLink()
 *		[*] Passage au validateur W3C
 *		[*] Correction empêchant l'affichage des textes libre une fois le formulaire soumis
 *		[*] Correction des liens d'appel aux fichiers CSS pour résoudre les soucis du CCC et de l'override
 *		[/] Modifications des traductions
 *      [+] Compatible jusqu'à PS1.6.11
 *
 *  v1.0.1 - changelog (2015-01-06)
 *      [*] Corrections de sécurité (cast des variables dans les requêtes SQL)
 *		[*] Correction dans le fichier JS du BO
 *		[*] Corrections de traductions dans le menu BO
 *		[*] Les objets des emails client et admin n'étaient pas enregistrés dans les bons champs en BDD
 *		[*] Corrections de bugs mineurs dans les tpl FO et BO
 *		[*] Correction d'un bug sur les checkboxes, radio et select lorsque le label était un entier
 *		[/] Le fichier css du front était appelé dans le fichier TPL, à présent appelé greffé au hook header
 *		[+] Ajout d'un fichier CSS dans le BO
 *		[-] Suppression des champs admin BCC et client BCC (non-utilisé par la classe Mail de presta)
 *
 *  v1.0 - changelog
 *      [+] Première version stable