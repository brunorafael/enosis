<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class Submission extends ObjectModel
{

	public $id_ukooformpro_submission;
	public $id_ukooformpro_form;
	public $date_submission;
	public $customer_ip;
	public $id_customer;
	public $id_lang;
	public $status;
	public static $definition = array(
		'table' => 'ukooformpro_submission',
		'primary' => 'id_ukooformpro_submission',
		'mulitilang' => false,
		'field' => array(
			'id_ukooformpro_submission' => array('type' => self::TYPE_INT, 'required' => true),
			'id_ukooformpro_form' => array('type' => self::TYPE_INT, 'required' => true),
			'date_submission' => array('type' => self::TYPE_DATE, 'required' => true),
			'customer_ip' => array('type' => self::TYPE_STRING, 'required' => true),
			'id_customer' => array('type' => self::TYPE_INT, 'required' => false),
			'id_lang' => array('type' => self::TYPE_INT, 'required' => true),
			'status' => array('type' => self::TYPE_INT, 'required' => true)
		)
	);

	public function createDBTableSubmission()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_submission` (
				  `id_ukooformpro_submission` INT(11) NOT NULL AUTO_INCREMENT,
				  `id_ukooformpro_form` INT(11) NOT NULL,
				  `date_submission` DATETIME NOT NULL,
				  `customer_ip` VARCHAR(25) NULL DEFAULT NULL,
				  `id_customer` INT(11) NULL DEFAULT 0,
				  `id_lang` INT(11) NOT NULL DEFAULT 0,
				  `status` TINYINT(1) NOT NULL DEFAULT 1,
				  PRIMARY KEY (`id_ukooformpro_submission`),
				  INDEX `id_ukooformpro_form_idx` (`id_ukooformpro_form` ASC))
				ENGINE = InnoDB
				AUTO_INCREMENT = 1
				DEFAULT CHARACTER SET = utf8;

				SHOW WARNINGS;

				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_submission_value` (
					`id_ukooformpro_submission` int(11) NOT NULL,
					`id_ukooformpro_elements` int(11) NOT NULL,
					`position` int(3) NOT NULL,
					`value` text,
					`label` varchar(255) NOT NULL,
					`type` int(2) NOT NULL,
					PRIMARY KEY (`id_ukooformpro_submission`,`position`),
					KEY `id_ukooformpro_submission_idx` (`id_ukooformpro_submission`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;

				SHOW WARNINGS;

				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_submission_files` (
					`id_ukooformpro_submission_files` int(11) NOT NULL AUTO_INCREMENT,
					`id_ukooformpro_submission` int(11) NOT NULL,
					`name` varchar(255) NOT NULL,
					`original_name` varchar(255) NOT NULL,
					`extention` varchar(5) NOT NULL,
					`type` varchar(255) NOT NULL,
					`id_ukooformpro_elements` int(11) NOT NULL,
					PRIMARY KEY (`id_ukooformpro_submission_files`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

				';
		return Db::getInstance()->execute($sql);
	}

	public function removeDBTableSubmission()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_submission`;
				DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_submission_files`;
				DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_submission_value`;';
		return Db::getInstance()->execute($sql);
	}

	public static function insertSubmission($post, $id_customer, $id_lang)
	{
		$sql = 'INSERT INTO `'._DB_PREFIX_.'ukooformpro_submission`
				(`id_ukooformpro_form`, `date_submission`, `customer_ip`, `id_customer`, `id_lang`, `status`)
				VALUES (\''.(int)$post['id_ukooformpro_form'].'\', CURRENT_TIMESTAMP(), \''.pSQL(Tools::getRemoteAddr()).'\', \''.(int)$id_customer.'\',
				\''.(int)$id_lang.'\', \'1\');';
		unset($post['id_ukooformpro_form']);
		unset($post['MAX_FILE_SIZE']);
		$position = 1;
		foreach ($post as $key => $value)
		{
			$key = Tools::substr($key, 3);
			if (is_array($value))
				$value = serialize($value);
			$sql .= '
					INSERT INTO `'._DB_PREFIX_.'ukooformpro_submission_value`
					(`id_ukooformpro_submission`, `id_ukooformpro_elements`, `position`, `value`, `label`, `type`)
					VALUES (LAST_INSERT_ID(), \''.(int)$key.'\', '.(int)$position.', \''.pSQL($value).'\',
					(SELECT `label` FROM `'._DB_PREFIX_.'ukooformpro_elements_lang`
					WHERE `id_ukooformpro_elements` = '.(int)$key.' AND `id_lang` = '.(int)ConfigurationCore::get('PS_LANG_DEFAULT').'),
					(SELECT `type` FROM `'._DB_PREFIX_.'ukooformpro_elements` WHERE `id_ukooformpro_elements` = '.(int)$key.'));
					';
			$position++;
		}
		return DB::getInstance()->execute($sql);
	}

	public static function changeSubmissionStatus($id_ukooformpro_submission, $status)
	{
		$sql = 'UPDATE `'._DB_PREFIX_.'ukooformpro_submission` SET `status` = \''.(int)$status.'\'
				WHERE id_ukooformpro_submission = '.(int)$id_ukooformpro_submission;
		return DB::getInstance()->execute($sql);
	}

	public static function selectSubmissionValue($id_ukooformpro_submission)
	{
		$sql = 'SELECT `label` AS label, `value` AS value, `type` AS type, `id_ukooformpro_elements` AS id_ukooformpro_elements
				FROM `'._DB_PREFIX_.'ukooformpro_submission_value`
				WHERE `id_ukooformpro_submission` = '.(int)$id_ukooformpro_submission;
		return DB::getInstance()->executeS($sql);
	}

	public static function fileSubmission($file)
	{
		$sql = 'INSERT INTO `'._DB_PREFIX_.'ukooformpro_submission_files` (`id_ukooformpro_submission`, `name`,`original_name`, 
`extention`, `type`, `id_ukooformpro_elements`)
				VALUES (
					(SELECT max(`id_ukooformpro_submission`) FROM `'._DB_PREFIX_.'ukooformpro_submission`),
					\''.pSQL($file['names']).'\',
					\''.pSQL($file['name']).'\',
					\''.pSQL($file['extention']).'\',
					\''.pSQL($file['type']).'\',
					'.(int)$file['id_ukooformpro_elements'].'
				);';

		if (DB::getInstance()->execute($sql))
		{
			$sql = 'SELECT LAST_INSERT_ID() FROM `'._DB_PREFIX_.'ukooformpro_submission_files`';
			return DB::getInstance()->executeS($sql);
		}
		else
			return false;
	}

	public static function selectFile($id_ukooformpro_submission)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'ukooformpro_submission_files`
				WHERE `id_ukooformpro_submission` = '.(int)$id_ukooformpro_submission;
		return DB::getInstance()->executeS($sql);
	}

	public static function selectDownloadFile($id_ukooformpro_submission_files)
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'ukooformpro_submission_files`
WHERE `id_ukooformpro_submission_files` = '.(int)$id_ukooformpro_submission_files;
		return Db::getInstance()->getRow($sql);
	}

	public static function deleteSubmission($id)
	{
		$sql = 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_submission`
				WHERE `id_ukooformpro_submission` = '.(int)$id.';
				DELETE FROM `'._DB_PREFIX_.'ukooformpro_submission_value`
				WHERE `id_ukooformpro_submission` = '.(int)$id.';';
		return DB::getInstance()->execute($sql);
	}

	public static function deleteSubmissionFile($id)
	{
		$sql = 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_submission_files`
				WHERE `id_ukooformpro_submission` = '.(int)$id;
		return DB::getInstance()->execute($sql);
	}

}
