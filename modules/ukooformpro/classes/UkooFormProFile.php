<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class UkooFormProFile
{

	public static function createIndexFile($path)
	{
		$index = array(
			'<?php',
			'/**',
			'*',
			'* 2007-2014 PrestaShop',
			'*',
			'* This source file is subject to the Academic Free License (AFL 3.0)',
			'* that is bundled with this package in the file LICENSE.txt.',
			'* It is also available through the world-wide-web at this URL:',
			'* http://opensource.org/licenses/afl-3.0.php',
			'* If you did not receive a copy of the license and are unable to',
			'* obtain it through the world-wide-web, please send an email',
			'* to license@prestashop.com so we can send you a copy immediately.',
			'*',
			'* DISCLAIMER',
			'*',
			'* Do not edit or add to this file if you wish to upgrade PrestaShop to newer',
			'* versions in the future. If you wish to customize PrestaShop for your',
			'* needs please refer to http://www.prestashop.com for more information.',
			'*',
			'* @author    PrestaShop SA <contact@prestashop.com>',
			'* @copyright 2007-2014 PrestaShop SA',
			'* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)',
			'* International Registered Trademark & Property of PrestaShop SA',
			'*/',
			'',
			'header(\'Expires: Mon, 26 Jul 1997 05:00:00 GMT\');',
			'header(\'Last-Modified: \'.gmdate(\'D, d M Y H:i:s\').\' GMT\');',
			'header(\'Cache-Control: no-store, no-cache, must-revalidate\');',
			'header(\'Cache-Control: post-check=0, pre-check=0\', false);',
			'header(\'Pragma: no-cache\');',
			'header(\'Location: ../\');',
			'exit;',
		);

		$myfile = fopen($path.'index.php', 'w');
		foreach ($index as $index_line)
			fwrite($myfile, $index_line);
		fclose($myfile);
	}

	/*	 * Create a new directory withe permission
	 *
	 * @param type $path path of directory
	 * @param type $permission permission example 0777 Default 0775
	 * @param type $recursive boolean Default false
	 */

	public static function createDirectory($path, $permission = 0775, $recursive = false)
	{
		$oldmask = umask(0);
		$return = mkdir($path, $permission, $recursive);
		umask($oldmask);
		UkooFormProFile::createIndexFile($path.'/');
		return $return;
	}

	/*	 * Create a new file
	 *
	 * @param type $path path of file
	 * @param type $content content's file can be an array
	 */

	public static function createFile($path, $contents)
	{
		$handle = fopen($path, 'w');
		if (is_array($contents))
			foreach ($contents as $content)
				fwrite($handle, $content);
		else
			fwrite($handle, $contents);
		fclose($handle);
	}

	/*	 * Read a file
	 *
	 * @param type $path path of file
	 * @return type String file content
	 */

	public static function readFile($path)
	{
		if (file_exists($path))
		{
			$handle = fopen($path, 'r');
			$file_size = filesize($path);
			if ($file_size == 0)
				$file_size = 1;
			$read = fread($handle, $file_size);
			fclose($handle);
			return $read;
		}
		else
			return false;
	}

	/*	 * Remove file
	 *
	 * @param type $path path of file
	 */

	public static function removeFile($path)
	{
		if (is_file($path))
			unlink($path);
	}

	/*	 * Empty directory
	 *
	 * @param type $path path of directory
	 */

	public static function emptyDirectory($path)
	{
		if (Tools::substr($path, -1) == '/')
			$path .= '*';
		else
			$path .= '/*';
		$return = true;
		$files = glob($path);
		foreach ($files as $file)
		{
			if (Tools::substr($file, -9) != 'index.php')
				if (is_dir($file.'/'))
				{
					if (UkooFormProFile::emptyFolder($file.'/*'))
					{
						if (!rmdir($file.'/'))
							$return = false;
					}
					else
						$return = false;
				}
				else if (is_file($file))
				{
					if (!unlink($file))
						$return = false;
				}
		}
		return $return;
	}

	public static function emptyFolder($path)
	{
		$return = true;
		$files = glob($path);
		foreach ($files as $file)
		{
			if (is_dir($file.'/'))
			{
				if (UkooFormProFile::emptyFolder($path.'/*'))
				{
					if (!rmdir($file.'/'))
						$return = false;
				}
				else
					$return = false;
			}
			else if (is_file($file))
			{
				if (!unlink($file))
					$return = false;
			}
		}
		return $return;
	}

	public static function removeAllFille()
	{
		$return = true;
		if (!UkooFormProFile::emptyEmailDirectory(dirname(__FILE__).'/../mails/'))
			$return = false;
		if (!UkooFormProFile::emptyDirectory(dirname(__FILE__).'/../files/'))
			$return = false;
		if (!UkooFormProFile::emptyDirectory(dirname(__FILE__).'/../views/templates/front/form_tpl/'))
			$return = false;
		return $return;
	}

	public static function emptyEmailDirectory($path)
	{
		if (Tools::substr($path, -1) == '/')
			$path .= '*';
		else
			$path .= '/*';
		$return = true;
		$files = glob($path);
		foreach ($files as $file)
		{
			if (Tools::substr($file, -9) != 'index.php')
				if (is_dir($file.'/'))
				{
					if (Tools::substr($file, -2) == 'fr' || Tools::substr($file, -2) == 'en')
					{
						if (!UkooFormProFile::emptyEmailFolder($file))
							$return = false;
					}
					else
					{
						if (UkooFormProFile::emptyFolder($file.'/*'))
						{
							if (!rmdir($file.'/'))
								$return = false;
						}
						else
							$return = false;
					}
				}
				else if (is_file($file))
				{
					if (!unlink($file))
						$return = false;
				}
		}
		return $return;
	}

	public static function emptyEmailFolder($path)
	{
		$email_files = array(
			$path.'/authentification_error.html',
			$path.'/authentification_error.txt',
			$path.'/authentification_success.html',
			$path.'/authentification_success.txt',
			$path.'/limited.html',
			$path.'/limited.txt',
			$path.'/unlimited.html',
			$path.'/unlimited.txt',
			$path.'/index.php'
		);
		if (Tools::substr($path, -1) == '/')
			$path .= '*';
		else
			$path .= '/*';
		$return = true;
		$files = glob($path);

		foreach ($files as $file)
		{
			if (!in_array($file, $email_files))
			{
				if (is_dir($file.'/'))
				{
					if (UkooFormProFile::emptyFolder($path.'/*'))
					{
						if (!rmdir($file.'/'))
							$return = false;
					}
					else
						$return = false;
				}
				else if (is_file($file))
				{
					if (!unlink($file))
						$return = false;
				}
			}
		}
		return $return;
	}

}
