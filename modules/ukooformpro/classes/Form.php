<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class Form extends ObjectModel
{

	public $id_ukooformpro_form;
	public $name;
	public $show_ty_message;
	public $save_datas;
	public $active;
	public $admin_email_send;
	public $client_email_send;
	public $use_custom_tpl;
	public $title;
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
	public $required_sign;
	public $error_message;
	public $attr_class;
	public $attr_id;
	public $attr_name;
	public $attr_action;
	public $other_attr;
	public $admin_email_tpl;
	public $admin_email_to;
	public $admin_email_to_name;
	public $admin_email_cc;
	public $admin_email_bcc;
	public $admin_email_from_mail;
	public $admin_email_from_name;
	public $admin_email_replyto_mail;
	public $admin_email_replyto_name;
	public $admin_email_subject;
	public $client_email_tpl;
	public $client_email_to;
	public $client_email_to_name;
	public $client_email_cc;
	public $client_email_bcc;
	public $client_email_from_mail;
	public $client_email_from_name;
	public $client_email_replyto_mail;
	public $client_email_replyto_name;
	public $client_email_subject;
	public $ty_message;
	public static $definition = array(
		'table' => 'ukooformpro_form',
		'primary' => 'id_ukooformpro_form',
		'multilang' => true,
		'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true),
			'show_ty_message' => array('type' => self::TYPE_BOOL, 'required' => true),
			'save_datas' => array('type' => self::TYPE_BOOL, 'required' => true),
			'active' => array('type' => self::TYPE_BOOL, 'required' => true),
			'admin_email_send' => array('type' => self::TYPE_BOOL, 'required' => true),
			'client_email_send' => array('type' => self::TYPE_BOOL, 'required' => true),
			'use_custom_tpl' => array('type' => self::TYPE_BOOL, 'required' => true),
			'title' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'meta_title' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'meta_description' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'required_sign' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'error_message' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'attr_class' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'attr_id' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'attr_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'attr_action' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'other_attr' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_tpl' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_to' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_to_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_cc' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_bcc' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_from_mail' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_from_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_replyto_mail' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_replyto_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'admin_email_subject' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_tpl' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_to' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_to_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_cc' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_bcc' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_from_mail' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_from_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_replyto_mail' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_replyto_name' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'client_email_subject' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'ty_message' => array('type' => self::TYPE_HTML, 'required' => false, 'lang' => true)
		)
	);

	public static function createDBTableForm()
	{
		$sql = '-- -----------------------------------------------------
				-- Table`ukooformpro_form`
				-- -----------------------------------------------------
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_form` (
				  `id_ukooformpro_form` INT(11) NOT NULL AUTO_INCREMENT,
				  `name` VARCHAR(25) NOT NULL,
				  `show_ty_message` TINYINT(1) NOT NULL DEFAULT 1,
				  `save_datas` TINYINT(1) NOT NULL DEFAULT 0,
				  `active` TINYINT(1) NOT NULL DEFAULT 1,
				  `admin_email_send` tinyint(1) NOT NULL DEFAULT 1,
				  `client_email_send` tinyint(1) NOT NULL DEFAULT 0,
				  `use_custom_tpl` tinyint(1) NOT NULL DEFAULT 0,
				  PRIMARY KEY (`id_ukooformpro_form`))
				ENGINE = InnoDB
				AUTO_INCREMENT = 1
				DEFAULT CHARACTER SET = utf8;

				SHOW WARNINGS;

				-- -----------------------------------------------------
				-- Table`ukooformpro_form_lang`
				-- -----------------------------------------------------
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_form_lang` (
				  `id_ukooformpro_form` INT(11) NOT NULL AUTO_INCREMENT,
				  `id_lang` INT(11) NOT NULL,
				  `title` VARCHAR(255) NOT NULL,
				  `meta_title` VARCHAR(255) NULL DEFAULT NULL,
				  `meta_description` VARCHAR(255) NULL DEFAULT NULL,
				  `meta_keywords` VARCHAR(255) NULL DEFAULT NULL,
				  `required_sign` VARCHAR(25) NOT NULL DEFAULT \'*\',
				  `error_message` VARCHAR(255) NULL DEFAULT NULL,
				  `attr_class` VARCHAR(25) NULL DEFAULT NULL,
				  `attr_id` VARCHAR(25) NULL DEFAULT NULL,
				  `attr_name` VARCHAR(25) NULL DEFAULT NULL,
				  `attr_action` VARCHAR(25) NULL DEFAULT NULL,
				  `other_attr` TEXT NULL DEFAULT NULL,
				  `admin_email_tpl` TEXT NULL DEFAULT NULL,
				  `admin_email_to` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_to_name` VARCHAR(1024) DEFAULT NULL,
				  `admin_email_cc` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_bcc` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_from_mail` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_from_name` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_replyto_mail` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_replyto_name` VARCHAR(1024) NULL DEFAULT NULL,
				  `admin_email_subject` VARCHAR(1024) NULL DEFAULT NULL,
				  `client_email_tpl` VARCHAR(1024) NOT NULL,
				  `client_email_to` VARCHAR(1024) NOT NULL,
				  `client_email_to_name` VARCHAR(1024) NOT NULL,
				  `client_email_cc` VARCHAR(1024) NOT NULL,
				  `client_email_bcc` VARCHAR(1024) NOT NULL,
				  `client_email_from_mail` VARCHAR(1024) NOT NULL,
				  `client_email_from_name` VARCHAR(1024) NOT NULL,
				  `client_email_replyto_mail` VARCHAR(1024) NOT NULL,
				  `client_email_replyto_name` VARCHAR(1024) NOT NULL,
				  `client_email_subject` VARCHAR(1024) NOT NULL,
				  `ty_message` TEXT NOT NULL,
				  PRIMARY KEY (`id_ukooformpro_form`, `id_lang`))
				ENGINE = InnoDB
				AUTO_INCREMENT = 1
				DEFAULT CHARACTER SET = utf8;

				SHOW WARNINGS;';

		return DB::getInstance()->execute($sql);
	}

	public static function removeDBTableForm()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_form` ;
				DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_form_lang` ;';
		return DB::getInstance()->execute($sql);
	}

	public static function selectForm($id_ukooformpro_form, $id_lang)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'ukooformpro_form` AS a
				INNER JOIN `'._DB_PREFIX_.'ukooformpro_form_lang` AS b
				ON a.`id_ukooformpro_form` = b.`id_ukooformpro_form`
				WHERE a.`id_ukooformpro_form` = '.(int)$id_ukooformpro_form.'
				AND b.`id_lang` = '.(int)$id_lang.'
				AND a.active = 1';
		return DB::getInstance()->executeS($sql);
	}

	public static function selectNameForm($id_ukooformpro_form)
	{
		$sql = 'SELECT `name`
				FROM `'._DB_PREFIX_.'ukooformpro_form`
				WHERE `id_ukooformpro_form` = '.(int)$id_ukooformpro_form;
		return DB::getInstance()->executeS($sql);
	}

	public static function selectLastInsert()
	{
		$sql = 'SELECT MAX(`id_ukooformpro_form`) AS id
				FROM `'._DB_PREFIX_.'ukooformpro_form`';
		return DB::getInstance()->executeS($sql);
	}

	public static function deleteForm($id_ukooformpro_form)
	{
		$sql = 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_form`
				WHERE `id_ukooformpro_form` = '.(int)$id_ukooformpro_form.';
				DELETE FROM `'._DB_PREFIX_.'ukooformpro_form_lang`
				WHERE `id_ukooformpro_form` = '.(int)$id_ukooformpro_form.';';
		return DB::getInstance()->execute($sql);
	}

	public static function selectNameID()
	{
		$sql = 'SELECT id_ukooformpro_form, name FROM '._DB_PREFIX_.'ukooformpro_form';
		return Db::getInstance()->executeS($sql);
	}

}
