<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class UkooFormProExport extends ObjectModel
{

	public $id_ukooformpro_form;
	public $date_from;
	public $date_to;
	public $fields;
	public $name;

	public function __construct($post)
	{
		$this->id_ukooformpro_form = $post['id_ukooformpro_form'];
		if (Tools::isEmpty($post['date_from']))
			$this->date_from = null;
		else
			$this->date_from = $post['date_from'];
		if (Tools::isEmpty($post['date_to']))
			$this->date_to = null;
		else
			$this->date_to = $post['date_to'];
		$this->fields = $this->SELECTFields();
		$this->name = $this->SELECTName();
	}

	public function SELECTName()
	{
		$sql = 'SELECT name FROM '._DB_PREFIX_.'ukooformpro_form WHERE id_ukooformpro_form = '.(int)$this->id_ukooformpro_form;
		return Db::getInstance()->getValue($sql);
	}

	public function SELECTFields()
	{
		$sql = 'SELECT a.id_ukooformpro_elements, a.type FROM '._DB_PREFIX_.'ukooformpro_elements AS a
WHERE id_ukooformpro_form = '.(int)$this->id_ukooformpro_form.'
ORDER BY a.position ASC';
		return Db::getInstance()->executeS($sql);
	}

	public function exportCSV()
	{
		$link = new Link();
		$select = '
SELECT a.id_ukooformpro_submission AS id_submission, a.date_submission, a.customer_ip AS ip_customer, a.id_customer, a.id_lang, a.status';
		$from = '
FROM '._DB_PREFIX_.'ukooformpro_submission AS a';
		$where = '
WHERE a.id_ukooformpro_form = '.(int)$this->id_ukooformpro_form;
		if ($this->date_from != null)
			$where .= '
AND a.date_submission >= '.pSQL($this->date_from);
		if ($this->date_to != null)
			$where .= '
AND a.date_submission <= '.pSQL($this->date_to);
		$sql = $select.$from.$where;
		$submissions = Db::getInstance()->executeS($sql);
		foreach ($submissions as $key => $submission)
		{
			foreach ($this->fields as $field)
			{
				if ($field['type'] == 6)
				{
					$sql = 'SELECT id_ukooformpro_submission_files FROM '._DB_PREFIX_.'ukooformpro_submission_files
WHERE id_ukooformpro_submission = '.(int)$submission['id_submission'].' AND id_ukooformpro_elements = '.(int)$field['id_ukooformpro_elements'];
					$result = Db::getInstance()->getValue($sql);
					$params = array(
						'id_file' => $result,
						'securekey' => ConfigurationCore::get('UKOOFORMPRO_0')
					);
					$result = $link->getModuleLink('ukooformpro', 'downloadfile', $params);
				}
				else
				{
					$sql = 'SELECT value FROM '._DB_PREFIX_.'ukooformpro_submission_value
WHERE id_ukooformpro_submission = '.(int)$submission['id_submission'].' AND id_ukooformpro_elements = '.(int)$field['id_ukooformpro_elements'];
					$result = Db::getInstance()->getValue($sql);
					if ($field['type'] == 3 || $field['type'] == 5)
					{
						$result = unserialize($result);
						if (is_array($result))
						{
							$i = 0;
							foreach ($result as $value)
							{
								if ($i == 0)
									$result = $value;
								else
									$result .= ' - '.$value;
								$i++;
							}
						}
					}
				}
				$name = 'elt'.$field['id_ukooformpro_elements'];
				$submissions[$key] = array_merge($submissions[$key], array($name => $result));
			}
		}
		$filename = $this->name.'_data_export_'.date('Y-m-d').'.csv';

		$now = gmdate('D, d M Y H:i:s');
		header('Expires: Tue, 03 Jul 2001 06:00:00 GMT');
		header('Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate');
		header('Last-Modified: '.$now.' GMT');

		// force download
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream');
		header('Content-Type: application/download');

		// disposition / encoding on response body
		header('Content-Disposition: attachment;filename='.$filename);
		header('Content-Transfer-Encoding: binary');

		echo $this->csv($submissions);
		die();
	}

	public function csv($submissions)
	{
		ob_start();
		$csv = fopen('php://output', 'w');
		fputcsv($csv, array_keys($submissions[0]));
		foreach ($submissions as $submission)
			fputcsv($csv, $submission);
		fclose($csv);
		return ob_get_clean();
	}

}
