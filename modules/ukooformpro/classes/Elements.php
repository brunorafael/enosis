<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class Elements extends ObjectModel
{

	public $id_ukooformpro_elements;
	public $id_ukooformpro_form;
	public $position;
	public $type;
	public $name;
	public $class;
	public $required;
	public $attribute;
	public $additional_attribute;
	public $active;
	public $label;
	public $hint;
	public $validation_message;
	public $default_value;
	public static $definition = array(
		'table' => 'ukooformpro_elements',
		'primary' => 'id_ukooformpro_elements',
		'multilang' => true,
		'fields' => array(
			'id_ukooformpro_form' => array('type' => self::TYPE_INT, 'required' => true),
			'position' => array('type' => self::TYPE_INT, 'required' => true),
			'type' => array('type' => self::TYPE_INT, 'required' => true),
			'name' => array('type' => self::TYPE_STRING, 'required' => false),
			'class' => array('type' => self::TYPE_STRING, 'required' => false),
			'required' => array('type' => self::TYPE_INT, 'required' => true),
			'attribute' => array('type' => self::TYPE_STRING, 'required' => false),
			'additional_attribute' => array('type' => self::TYPE_STRING, 'required' => false),
			'active' => array('type' => self::TYPE_INT, 'required' => true),
			'label' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'hint' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'validation_message' => array('type' => self::TYPE_STRING, 'required' => false, 'lang' => true),
			'default_value' => array('type' => self::TYPE_HTML, 'required' => false, 'lang' => true)
		)
	);

	public static function createDBTableElements()
	{
		$sql = '-- -----------------------------------------------------
				-- Table `mydb`.`ukooformpro_elements`
				-- -----------------------------------------------------
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_elements` (
				  `id_ukooformpro_elements` INT(11) NOT NULL AUTO_INCREMENT,
				  `id_ukooformpro_form` INT NOT NULL,
				  `position` INT NOT NULL,
				  `type` INT NOT NULL,
				  `name` VARCHAR(45) NOT NULL,
				  `class` VARCHAR(45) NOT NULL,
				  `required` INT NOT NULL,
				  `attribute` MEDIUMTEXT NOT NULL,
				  `additional_attribute` MEDIUMTEXT NULL,
				  `active` INT(1) NULL DEFAULT 1,
				  PRIMARY KEY (`id_ukooformpro_elements`),
				  INDEX `id_ukooformpro_form_idx` (`id_ukooformpro_form` ASC))
				ENGINE = InnoDB
				DEFAULT CHARACTER SET = utf8;

				SHOW WARNINGS;

				-- -----------------------------------------------------
				-- Table `mydb`.`ukooformpro_elements_lang`
				-- -----------------------------------------------------
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ukooformpro_elements_lang` (
				  `id_ukooformpro_elements` INT NOT NULL,
				  `id_lang` INT NOT NULL,
				  `label` VARCHAR(255) NULL,
				  `hint` VARCHAR(255) NULL,
				  `validation_message` VARCHAR(255) NULL,
				  `default_value` TEXT NULL,
				  PRIMARY KEY (`id_lang`, `id_ukooformpro_elements`),
				  INDEX `id_ukooformpro_elements_idx` (`id_ukooformpro_elements` ASC))
				ENGINE = InnoDB;

				SHOW WARNINGS;';

		return DB::getInstance()->execute($sql);
	}

	public static function removeDBTableElements()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_elements` ;
				DROP TABLE IF EXISTS `'._DB_PREFIX_.'ukooformpro_elements_lang` ;';
		return DB::getInstance()->execute($sql);
	}

	/**
	 *
	 * @param type $id_form
	 * @param type $id_lang
	 * @return type
	 */
	public static function fieldFormList($id_form, $id_lang)
	{
		$sql = 'SELECT a.`id_ukooformpro_elements`, a.`position`, a.`type`, a.`required`, a.`active`, b.`label`, b.`hint`, b.`default_value`, a.`attribute`
				FROM `'._DB_PREFIX_.'ukooformpro_elements` AS a
				INNER JOIN `'._DB_PREFIX_.'ukooformpro_elements_lang` AS b
				ON a.`id_ukooformpro_elements` = b.`id_ukooformpro_elements`
				WHERE a.`id_ukooformpro_form` = '.(int)$id_form.'
				AND b.`id_lang` = '.(int)$id_lang.'
				ORDER BY a.`position` ASC';
		return DB::getInstance()->executeS($sql);
	}

	/**
	 *
	 * @param type $id
	 * @param type $active_update
	 * @return type
	 */
	public static function activeUpdate($id, $active_update)
	{
		$sql = 'UPDATE `'._DB_PREFIX_.'ukooformpro_elements` SET `active` = '.(int)(bool)$active_update.' WHERE `id_ukooformpro_elements` = '.(int)$id;
		return DB::getInstance()->execute($sql);
	}

	/**
	 *
	 * @param type $positions
	 * @return type
	 */
	public function positionUpdate($positions)
	{
		$sql = '';
		foreach ($positions as $position => $id)
		{
			$sql .= 'UPDATE `'._DB_PREFIX_.'ukooformpro_elements` SET `position` = '.((int)$position + 1).'
					WHERE `id_ukooformpro_elements` = '.(int)Tools::substr($id, 4).'; ';
		}
		return DB::getInstance()->execute($sql);
	}

	/**
	 *
	 * @param type $id_ukooformpro_elements
	 * @param type $positions
	 * @return type
	 */
	public function removeElements($id_ukooformpro_elements, $positions)
	{
		$sql = 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements`
				WHERE `id_ukooformpro_elements` = '.(int)$id_ukooformpro_elements.';
				DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements_lang`
				WHERE `id_ukooformpro_elements` = '.(int)$id_ukooformpro_elements.';';
		$i = 0;
		while (Tools::substr($positions[$i], 4) != $id_ukooformpro_elements)
			$i++;

		$count = count($positions);
		for ($j = ($i + 1); $j < $count; $j++)
			$sql .= 'UPDATE `'._DB_PREFIX_.'ukooformpro_elements` SET `position` = '.(int)$j.'
					WHERE `id_ukooformpro_elements` = '.(int)Tools::substr($positions[$j], 4).';';
		return DB::getInstance()->execute($sql);
	}

	/**
	 *
	 * @param type $id_ukooformpro_elements
	 * @param type $id_lang
	 * @return type
	 */
	public static function selectElements($id_ukooformpro_elements, $id_lang)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'ukooformpro_elements` AS a
				INNER JOIN `'._DB_PREFIX_.'ukooformpro_elements_lang` AS b
				ON a.`id_ukooformpro_elements` = b.`id_ukooformpro_elements`
				WHERE a.`id_ukooformpro_form` = '.(int)$id_ukooformpro_elements.'
				AND a.`active` = 1 AND b.`id_lang` = '.(int)$id_lang.'
				ORDER BY `position`';
		return DB::getInstance()->executeS($sql);
	}

	public function removeSelectElements($elements, $positions)
	{
		$sql = '';
		foreach ($elements as $element)
		{
			$sql .= 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements`
				WHERE `id_ukooformpro_elements` = '.(int)$element['value'].';
				DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements_lang`
				WHERE `id_ukooformpro_elements` = '.(int)$element['value'].';';
			foreach ($positions as $key => $value)
				$i = 0;
			while (Tools::substr($positions[$i], 4) != $element['value'])
				$i++;
			$count = count($positions);
			for ($j = ($i + 1); $j < $count; $j++)
				$positions[$j - 1] = $positions[$j];
			unset($positions[$j - 1]);
		}
		foreach ($positions as $key => $value)
			if (!Tools::isEmpty($value))
				$sql .= 'UPDATE `'._DB_PREFIX_.'ukooformpro_elements` SET `position` = '.((int)$key + 1).'
					WHERE `id_ukooformpro_elements` = '.(int)Tools::substr($value, 4).';';
		return DB::getInstance()->execute($sql);
	}

	public static function selectID($id_ukooformpro_form)
	{
		$sql = 'SELECT `id_ukooformpro_elements` AS id
				FROM `'._DB_PREFIX_.'ukooformpro_elements`
				WHERE `id_ukooformpro_form` = '.(int)$id_ukooformpro_form;
		return DB::getInstance()->executeS($sql);
	}

	public static function deleteElements($id_ukooformpro_form)
	{
		$ids = Elements::selectID($id_ukooformpro_form);
		$sql = 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements`
				WHERE `id_ukooformpro_form` = '.(int)$id_ukooformpro_form.';';

		foreach ($ids as $id)
			$sql .= 'DELETE FROM `'._DB_PREFIX_.'ukooformpro_elements_lang`
					WHERE `id_ukooformpro_elements` = '.(int)$id['id'].';';

		return Db::getInstance()->execute($sql);
	}

}
