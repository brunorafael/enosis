<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2015
 * @license   Ukoo - Tous droits réservés
 */

class AdminUkooFormProDownloadFileController extends ModuleAdminController
{

	public function __construct()
	{
		parent::__construct();
		if (Tools::isSubmit('id_file'))
		{
			$file = Submission::selectDownloadFile(Tools::getValue('id_file'));
			$local_file = '../modules/ukooformpro/files/file_'.$file['id_ukooformpro_submission_files'].'.'.$file['extention'];
			$download_file = $file['original_name'];
			if (file_exists($local_file) && is_file($local_file))
			{
				header('Cache-control: private');
				header('Content-Type: '.$file['type']);
				header('Content-Length: '.filesize($local_file));
				header('Content-Disposition: filename='.$download_file);
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				readfile($local_file);
				exit;
			}
		}
	}

	public static function installInBO()
	{
		$new_menu = new Tab();
		$new_menu->id_parent = Tab::getIdFromClassName('AdminParentUkooFormProManagement');
		$new_menu->class_name = 'AdminUkooFormProDownloadFile';
		$new_menu->module = 'ukooformpro';
		$new_menu->active = 0;

		$languages = Language::getLanguages(false);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = 'Offer';

		return $new_menu->save();
	}

}
