<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class AdminUkooFormProSubmissionController extends ModuleAdminController
{

	public function __construct()
	{
		$this->table = 'ukooformpro_submission';
		$this->className = 'Submission';
		$this->lang = false;
		$this->bootstrap = true;

		$this->addRowAction('view');
		$this->addRowAction('delete');
		$this->_orderWay = 'DESC';

		parent::__construct();

		$this->_select = 'b.`name`';

		$this->_join = 'INNER JOIN `'._DB_PREFIX_.'ukooformpro_form` AS b
				ON a.`id_ukooformpro_form` = b.`id_ukooformpro_form`';

		$this->fields_list = array(
			'id_ukooformpro_submission' => array(
				'title' => $this->l('ID'),
				'align' => 'text-center',
				'class' => 'fixed-width-xs'
			),
			'id_ukooformpro_form' => array(
				'title' => $this->l('Form ID'),
				'align' => 'text-center',
				'class' => 'fixed-width-xs'
			),
			'name' => array(
				'title' => $this->l('Form name'),
				'align' => 'text-center'
			),
			'id_customer' => array(
				'title' => $this->l('Customer ID'),
				'align' => 'text-center'
			),
			'id_lang' => array(
				'title' => $this->l('Lang'),
				'callback' => 'getLang'
			),
			'customer_ip' => array(
				'title' => $this->l('Customer IP')
			),
			'status' => array(
				'title' => $this->l('Status'),
				'type' => 'text',
				'callback' => 'getStatus'
			),
			'date_submission' => array(
				'title' => $this->l('Date')
			)
		);

		$this->bulk_actions = array(
			'delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')),
			'enableSelection' => array('text' => $this->l('Enable selection')),
			'disableSelection' => array('text' => $this->l('Disable selection'))
		);
	}

	public static function installInBO()
	{
		$trads = array(
			'fr' => 'Gérer les soumissions',
			'en' => 'Manage submissions'
		);

		$new_menu = new Tab();
		$new_menu->id_parent = Tab::getIdFromClassName('AdminParentUkooFormProManagement');
		$new_menu->class_name = 'AdminUkooFormProSubmission';
		$new_menu->module = 'ukooformpro';
		$new_menu->active = 1;

		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = (isset($trads[$language['iso_code']]) ? $trads[$language['iso_code']] : $trads['en']);

		return $new_menu->save();
	}

	public static function removeFromBO()
	{
		$remove_id = Tab::getIdFromClassName('AdminUkooFormProSubmission');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}

	public function getLang($id_lang)
	{
		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			if ($language['id_lang'] == $id_lang)
				return $language['iso_code'];
		return $id_lang;
	}

	public function getStatus($status)
	{
		switch ($status)
		{
			case 1:
				return '<span  class="label color_field" style="background-color:#DC143C;color:white">'.$this->l('New').'</span>';
			case 2:
				return '<span class="label color_field" style="background-color:#8A2BE2;color:white">'.$this->l('Read').'</span>';
			case 3:
				return '<span class="label color_field" style="background-color:#108510;color:white">'.$this->l('Treat').'</span>';
		}
	}

	public function renderList()
	{
		$this->addJqueryUI('ui.datepicker');
		$forms = Form::selectNameID();
		$this->context->smarty->assign(
			array(
				'forms' => $forms
			)
		);
		return parent::renderList();
	}

	public function renderView()
	{
		$files = Submission::selectFile(Tools::getValue('id_ukooformpro_submission'));
		if ($this->loadObject(true)->status == 1)
			if (Submission::changeSubmissionStatus(Tools::getValue('id_ukooformpro_submission'), 2))
				$this->loadObject(true)->status = 2;
		if ($table_values = Submission::selectSubmissionValue(Tools::getValue('id_ukooformpro_submission')))
			foreach ($table_values as $key => $value)
				if ($table_values[$key]['type'] == 3 || $table_values[$key]['type'] == 5)
					$table_values[$key]['value'] = unserialize($value['value']);
				else if ($table_values[$key]['type'] == 6)
					foreach ($files as $file)
						if ($file['id_ukooformpro_elements'] == $value['id_ukooformpro_elements'])
							$table_values[$key]['id'] = $file['id_ukooformpro_submission_files'];
		$name = '';

		if ($name = Form::selectNameForm($this->loadObject(true)->id_ukooformpro_form))
			$name = $name[0];

		$lang_iso = '';
		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			if ($language['id_lang'] == $this->loadObject(true)->id_lang)
				$lang_iso = $language['iso_code'];

		$this->context->smarty->assign(
			array(
				'id_ukooformpro_submission' => Tools::getValue('id_ukooformpro_submission'),
				'currentToken' => $this->token,
				'currentObject' => $this->loadobject(true),
				'table_values' => $table_values,
				'form_name' => $name,
				'lang_iso' => $lang_iso,
				'tokenCustomers' => Tools::getAdminTokenLite('AdminCustomers'),
				'tokken2' => Tools::getAdminTokenLite('AdminUkooFormProDownloadFile'),
				'files' => $files
			)
		);
		return parent::renderView();
	}

	public function displayAjaxChangeStatus()
	{
		$status = Tools::getValue('status');
		if (!Submission::changeSubmissionStatus(Tools::getValue('submission'), $status['0']['value']))
			echo $this->l('Error  for update status restart later');
	}

	public function postProcess()
	{
		if (Tools::isSubmit('deleteukooformpro_submission'))
			$this->deleteSubmission(Tools::getValue('id_ukooformpro_submission'));
		else if (Tools::isSubmit('ukooformpro_submissionBox'))
		{
			$submissions = Tools::getValue('ukooformpro_submissionBox');
			foreach ($submissions as $submission)
				$this->deleteSubmission($submission);
		}
		else if (Tools::isSubmit('submitExportUkooFormPro'))
		{
			$export = new UkooFormProExport($_POST);
			$export->exportCSV();
		}
		else
			parent::postProcess();
	}

	public function deleteSubmission($id)
	{
		if (Submission::deleteSubmission($id))
		{
			$files = Submission::selectFile($id);

			if (count($files) > 0)
			{
				foreach ($files as $file)
				{
					$path = './../modules/ukooformpro/files/file_'.$file['id_ukooformpro_submission_files'].'.'.$file['extention'];
					UkooFormProFile::removeFile($path);
				}
				Submission::deleteSubmissionFile($id);
			}
		}
	}

	public function setMedia()
	{
		parent::setMedia();

		$this->addJS(_PS_MODULE_DIR_.'ukooformpro/views/js/adminukooformpro.js');
		$this->addCSS(_PS_MODULE_DIR_.'ukooformpro/views/css/adminukooformpro.css');
	}

}
