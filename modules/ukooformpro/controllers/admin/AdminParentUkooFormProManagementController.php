<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2015
 * @license   Ukoo - Tous droits réservés
 */

class AdminParentUkooFormProManagementController extends ModuleAdminController
{
	public function __construct()
	{
		parent::__construct();
		$link = new Link();
		Tools::redirectAdmin($link->getAdminLink('AdminUkooFormProManagement'));
	}

	public static function menuInstall()
	{
		$trads = array(
			'fr' => 'Formulaires',
			'en' => 'Forms'
		);

		$new_menu = new Tab();
//		$new_menu->id_parent = Tab::getIdFromClassName('AdminTools');
		$new_menu->class_name = 'AdminParentUkooFormProManagement';
		$new_menu->module = 'ukooformpro';
		$new_menu->active = 1;

		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = (isset($trads[$language['iso_code']]) ? $trads[$language['iso_code']] : $trads[$language['en']]);

		return $new_menu->save();
	}

	public static function menuUninstall()
	{
		$remove_id = Tab::getIdFromClassName('AdminParentUkooFormProManagement');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}
}
