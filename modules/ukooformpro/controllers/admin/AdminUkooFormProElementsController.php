<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class AdminUkooFormProElementsController extends ModuleAdminController
{

	public function __construct()
	{
		$this->table = 'ukooformpro_elements';
		$this->className = 'Elements';
		$this->lang = true;
		$this->bootstrap = true;

		parent::__construct();

		$this->fields_list = array(
			'id_ukooformpro_elements' => array(
				'title' => $this->l('ID'),
			),
		);

		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Elements'),
				'icon' => 'icon-envelope'
			),
			'submit' => array(
				'title' => $this->l('save'),
			)
		);
	}

	public static function installInBO()
	{
		$trads = array(
			'fr' => 'Gérer les éléments de formulaires',
			'en' => 'Manage forms elements'
		);

		$new_menu = new Tab();
		$new_menu->id_parent = Tab::getIdFromClassName('AdminParentUkooFormProManagement');
		$new_menu->class_name = 'AdminUkooFormProElements';
		$new_menu->module = 'ukooformpro';
		$new_menu->active = 0;

		$languages = Language::getLanguages(false);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = (isset($trads[$language['iso_code']]) ? $trads[$language['iso_code']] : $trads['en']);

		return $new_menu->save();
	}

	public static function removeFromBO()
	{
		$remove_id = Tab::getIdFromClassName('AdminUkooFormProElements');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}

	/**
	 * function for add or update modalform
	 */
	public function displayAjax()
	{
		switch (Tools::getValue('type'))
		{
			case 1 :
				$_POST['attribute'] = serialize(array('size' => Tools::getValue('size'),
					'max_size' => Tools::getValue('max_size'),
					'validation_rule' => Tools::getValue('validation_rule')));
				break;
			case 2 :
				$_POST['attribute'] = serialize(array('cols' => Tools::getValue('cols'), 'rows' => Tools::getValue('rows')));
				break;
			case 3 :
				$_POST['attribute'] = serialize(array('flow' => Tools::getValue('flow')));
			case 4 :
				$_POST['attribute'] = serialize(array('flow' => Tools::getValue('flow')));
				break;
			case 5 :
				$_POST['attribute'] = serialize(array('size' => Tools::getValue('size'), 'multiple' => Tools::getValue('multiple')));
				break;
			case 6 :
				$_POST['attribute'] = serialize(array('multiple' => Tools::getValue('multiple'),
					'extensions' => Tools::getValue('extensions'),
					'size' => Tools::getValue('file_size')));
				break;
			case 8 :
				$_POST['attribute'] = '';
				break;
			case 9 :
				$_POST['attribute'] = '';
				break;
			case 10 :
				$refresh = array();
				foreach ($_POST as $key => $value)
				{
					if (Tools::substr($key, 0, 7) == 'refresh')
						$refresh[Tools::substr($key, 16)] = $value;
				}
				$_POST['attribute'] = serialize(array(
					'refresh_text' => $refresh,
					'background_color' => Tools::substr(Tools::getValue('background_color'), 1),
					'text_color' => Tools::substr(Tools::getValue('text_color'), 1)
				));
				break;
			case 11 :
				$_POST['attribute'] = serialize(array('button_type' => Tools::getValue('button_type')));
				break;
		}

		if (Tools::getValue('id_ukooformpro_elements') == 0)
		{
			unset($_POST['id_ukooformpro_elements']);
			if ($this->processAdd())
			{
				$this->context->smarty->assign(
					array(
						'fieldFormList' => $this->fieldformlist(Tools::getValue('id_ukooformpro_form'), $this->context->language->id),
					)
				);
				$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/listelements.tpl');
			}
		}
		else
		{
			if ($this->processUpdate())
			{
				$this->context->smarty->assign(
					array(
						'fieldFormList' => $this->fieldformlist(Tools::getValue('id_ukooformpro_form'), $this->context->language->id)
					)
				);
				$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/listelements.tpl');
			}
		}
	}

	/**
	 * function for change active status
	 */
	public function displayAjaxActive()
	{
		if (Tools::getValue('active') == 1)
			$active_update = 0;
		else
			$active_update = 1;

		if (Elements::activeUpdate(Tools::getValue('id'), $active_update))
		{
			$field = array(
				'id_ukooformpro_elements' => Tools::getValue('id'),
				'active' => $active_update
			);
			$this->context->smarty->assign(
				array(
					'field' => $field,
					'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
					'languages' => Language::getLanguages(),
					'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
					'currentToken' => $this->token,
					'currentTab' => $this,
				)
			);
			$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/activeelement.tpl');
		}
	}

	/**
	 * function for show udpdate Modal
	 */
	public function displayAjaxUpdate()
	{
		$element = new Elements(Tools::getValue('id'));
		$attribute = unserialize($element->attribute);

		$this->context->smarty->assign(
			array(
				'id_ukooformpro_form' => $element->id_ukooformpro_form,
				'id_ukooformpro_elements' => $element->id_ukooformpro_elements,
				'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
				'languages' => Language::getLanguages(),
				'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
				'position' => $element->position,
				'type' => $element->type,
				'name' => $element->name,
				'required' => $element->required,
				'additional_attribute' => $element->additional_attribute,
				'active' => $element->active,
				'label' => $element->label,
				'hint' => $element->hint,
				'validation_message' => $element->validation_message,
				'default_value' => $element->default_value,
				'size' => $attribute['size'],
				'max_size' => $attribute['max_size'],
				'validation_rule' => $attribute['validation_rule'],
				'flow' => $attribute['flow'],
				'multiple' => $attribute['multiple'],
				'button_type' => $attribute['button_type'],
				'refresh_captcha' => $attribute['refresh_text'],
				'extensions' => $attribute['extensions'],
				'background_color' => $attribute['background_color'],
				'text_color' => $attribute['text_color'],
				'rows' => $attribute['rows'],
				'cols' => $attribute['cols'],
				'class' => $element->class
			)
		);
		$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/modalform.tpl');
	}

	/**
	 * function ajax for show Modal
	 * call by ajaxElementModal(type)
	 */
	public function displayAjaxModal()
	{
		$this->context->smarty->assign(
			array(
				'id_ukooformpro_form' => Tools::getValue('id_ukooformpro_form'),
				'id_ukooformpro_elements' => 0,
				'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
				'languages' => Language::getLanguages(),
				'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
				'type' => Tools::getValue('type'),
				'position' => Tools::getValue('position'),
			)
		);
		$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/modalform.tpl');
		die();
	}

	/**
	 * function ajax for update sorted elements position
	 */
	public function displayAjaxSorted()
	{
		$this->loadObject(true)->positionUpdate(Tools::getValue('sortedIDs'));
		die();
	}

	/**
	 * function ajax for delete element
	 */
	public function displayAjaxDelete()
	{
		if ($this->loadObject(true)->removeElements(Tools::getValue('id_ukooformpro_elements'), Tools::getValue('sortedIDs')))
		{
			$current_object = $this->loadObject(true);
			$this->context->smarty->assign(
				array(
					'id_ukooformpro_form' => $current_object->id_ukooformpro_form,
					'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
					'languages' => Language::getLanguages(),
					'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
					'currentToken' => $this->token,
					'currentObject' => $current_object,
					'currentTab' => $this,
					'fieldFormList' => $this->fieldformlist(Tools::getValue('id_ukooformpro_form'), $this->context->language->id)
				)
			);
			$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/listelements.tpl');
		}
		die();
	}

	public function displayAjaxDeleteSelected()
	{
		$elements = Tools::getValue('form');
		$positions = Tools::getValue('positions');
		Elements::removeSelectElements($elements, $positions);
		$current_object = $this->loadObject(true);
//		$fieldformlist = Elements::fieldFormList(Tools::getValue('id_ukooformpro_form'), $this->context->language->id);
		$this->context->smarty->assign(
			array(
				'id_ukooformpro_form' => $current_object->id_ukooformpro_form,
				'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
				'languages' => Language::getLanguages(),
				'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
				'currentToken' => $this->token,
				'currentObject' => $current_object,
				'currentTab' => $this,
				'fieldFormList' => $this->fieldformlist(Tools::getValue('id_ukooformpro_form'), $this->context->language->id),
			)
		);
		$this->context->smarty->display('../modules/ukooformpro/views/templates/admin/ukoo_form_pro_management/helpers/form/listelements.tpl');
	}

	private function fieldformlist($id_ukooformpro_form, $id_lang)
	{
		$field_form_list = Elements::fieldFormList($id_ukooformpro_form, $id_lang);
		foreach ($field_form_list as $field_form_list_key => $field_form_list_value)
		{
			$field_form_list[$field_form_list_key]['attribute'] = unserialize($field_form_list_value['attribute']);
			if ($field_form_list_value['type'] == 3 || $field_form_list_value['type'] == 4 || $field_form_list_value['type'] == 5)
			{
				$defaultvalues = explode(';', $field_form_list_value['default_value']);
				$tmp2 = array();
				foreach ($defaultvalues as $defaultvalue)
				{
					if ($defaultvalue != '')
					{
						$tmp = explode('=', $defaultvalue);
						$tmp2 = array_merge($tmp2, array($tmp[0] => $tmp[1]));
					}
				}
				$field_form_list[$field_form_list_key]['default_value'] = $tmp2;
			}
		}
		return $field_form_list;
	}

}
