<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class AdminUkooFormProManagementController extends ModuleAdminController
{

	public function __construct()
	{
		$this->table = 'ukooformpro_form';
		$this->className = 'Form';
		$this->lang = true;
		$this->bootstrap = true;

		parent::__construct();

		$this->addRowAction('edit');
		$this->addRowAction('address');
		$this->addRowAction('delete');
		$this->_orderWay = 'DESC';

		/*
		 * Vue liste - définition des colonnes
		 */
		$this->fields_list = array(
			'id_ukooformpro_form' => array(
				'title' => $this->l('ID'),
			),
			'name' => array(
				'title' => $this->l('Name'),
			),
			'save_datas' => array(
				'title' => $this->l('Save datas'),
				'type' => 'bool'
			),
			'active' => array(
				'title' => $this->l('Active'),
				'type' => 'bool',
				'active' => 'status',
			)
		);

		/*
		 * Vue liste - Boutons d'actions
		 */
		$this->bulk_actions = array(
			'delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')),
			'enableSelection' => array('text' => $this->l('Enable selection')),
			'disableSelection' => array('text' => $this->l('Disable selection'))
		);

		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Form Configartion'),
				'icon' => 'icon-envelope'
			),
			'input' => array(
				array(
					'type' => 'textarea',
					'name' => 'default_value',
					'label' => $this->l('Default Value'),
					'autoload_rte' => true
				),
			),
			'submit' => array(
				'title' => $this->l('save')
			)
		);
	}

	public static function installInBO()
	{
		$trads = array(
			'fr' => 'Gérer les formulaires',
			'en' => 'Manage forms'
		);

		$new_menu = new Tab();
		$new_menu->id_parent = Tab::getIdFromClassName('AdminParentUkooFormProManagement');
		$new_menu->class_name = 'AdminUkooFormProManagement';
		$new_menu->module = 'ukooformpro';
		$new_menu->active = 1;

		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = (isset($trads[$language['iso_code']]) ? $trads[$language['iso_code']] : $trads['en']);

		return $new_menu->save();
	}

	public static function removeFromBO()
	{
		$remove_id = Tab::getIdFromClassName('AdminUkooFormProManagement');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}

	public function initPageHeaderToolbar()
	{
		if (empty($this->display))
			$this->page_header_toolbar_btn['new_rule'] = array(
				'href' => self::$currentIndex.'&addukooformpro_form&token='.$this->token,
				'desc' => $this->l('Add new form', null, null, false),
				'icon' => 'process-icon-new'
			);
		parent::initPageHeaderToolbar();
	}

	/**
	 * Custom order way
	 * @param string $token use for validator
	 * @param type $id
	 * @return type tpl
	 */
	public function displayAddressLink($token = null, $id)
	{
		$tpl = $this->createTemplate('helpers/list/list_action_address.tpl');
		if (!array_key_exists('Address', self::$cache_lang))
			self::$cache_lang['Address'] = $this->l('Show form address');

		$tpl->assign(array(
			'href' => $this->context->link->getModuleLink('ukooformpro', 'form', array('id_form' => (int)$id)),
			'action' => self::$cache_lang['Address'],
			'id' => (int)$id,
			'token' => $token
		));

		return $tpl->fetch();
	}

	public function renderForm()
	{
		$error = $this->l('You are currently logged in with the following domain name:').' <span style="color:red;">'.$_SERVER['HTTP_HOST'].'</span><br />'.
			$this->l('This is different than the one configured in "SEO & URLs", which can cause malfunctions. So that PrestaShop modules work properly,
						please login to your administration using the same URL that you declared in your configuration.');
		if (str_replace('http://', '', _PS_BASE_URL_) != $_SERVER['HTTP_HOST'])
			$this->warnings = array_merge($this->warnings, array($error));

		$current_object = $this->loadObject(true);
		$field_form_list = '';
		if (!is_writable('./../modules/ukooformpro/mails'))
		{
			$error = $this->l('The permissions are wrong for this file or directory : ').'/modules/ukooformpro/mails';
			$this->warnings = array_merge($this->warnings, array($error));
		}
		if (!is_writable('./../modules/ukooformpro/files'))
		{
			$error = $this->l('The permissions are wrong for this file or directory : ').'/modules/ukooformpro/files';
			$this->warnings = array_merge($this->warnings, array($error));
		}
		if (!is_writable('./../modules/ukooformpro/views/templates/front/form_tpl'))
		{
			$error = $this->l('The permissions are wrong for this file or directory : ').'/modules/ukooformpro/views/templates/front/form_tpl';
			$this->warnings = array_merge($this->warnings, array($error));
		}

		if (Tools::getIsset('updateukooformpro_form'))
		{
			if (file_exists('./../modules/ukooformpro/views/templates/front/form_tpl/form_id'.Tools::getValue('id_ukooformpro').'.tpl') &&
				!is_writable('./../modules/ukooformpro/views/templates/front/form_tpl/form_id'.Tools::getValue('id_ukooformpro').'.tpl'))
			{
				$error = $this->l('The permissions are wrong for this file or directory : ');
				$error .= '/modules/ukooformpro/views/templates/front/form_tpl/form_id'.Tools::getValue('id_ukooformpro_form').'.tpl';
				$this->warnings = array_merge($this->warnings, array($error));
			}

			foreach ($this->context->controller->_languages as $language)
			{
				if (file_exists('./../modules/ukooformpro/mails/'.$language['iso_code']) && !is_writable('./../modules/ukooformpro/mails/'.$language['iso_code']))
				{
					$error = $this->l('The permissions are wrong for this file or directory : ');
					$error .= '/modules/ukooformpro/mails/'.$language['iso_code'];
					$this->warnings = array_merge($this->warnings, array($error));
				}
				else if (!file_exists('./../modules/ukooformpro/mails/'.$language['iso_code']))
					UkooFormProFile::createDirectory('./../modules/ukooformpro/mails/'.$language['iso_code']);
				if (file_exists('./../modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.html') &&
					!is_writable('./../modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.html'))
				{
					$error = $this->l('The permissions are wrong for this file or directory : ');
					$error .= '/modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.html';
					$this->warnings = array_merge($this->warnings, array($error));
				}
				if (file_exists('./../modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.txt') &&
					!is_writable('./../modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.txt'))
				{
					$error = $this->l('The permissions are wrong for this file or directory : ');
					$error .= '/modules/ukooformpro/mails/'.$language['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.txt';
					$this->warnings = array_merge($this->warnings, array($error));
				}
				if (file_exists('./../modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.html') &&
					!is_writable('./../modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.html'))
				{
					$error = $this->l('The permissions are wrong for this file or directory : ');
					$error .= '/modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.html';
					$this->warnings = array_merge($this->warnings, array($error));
				}
				if (file_exists('./../modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.txt') &&
					!is_writable('./../modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.txt'))
				{
					$error = $this->l('The permissions are wrong for this file or directory : ');
					$error .= '/modules/ukooformpro/mails/'.$language['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.txt';
					$this->warnings = array_merge($this->warnings, array($error));
				}
			}

			$update = true;
			$field_form_list = Elements::fieldFormList($current_object->id_ukooformpro_form, $this->context->language->id);
			foreach ($field_form_list as $field_form_list_key => $field_form_list_value)
			{
				$field_form_list[$field_form_list_key]['attribute'] = unserialize($field_form_list_value['attribute']);
				// Traitement spécifique pour les checkbox, radio et select
				if ($field_form_list_value['type'] == 3 || $field_form_list_value['type'] == 4 || $field_form_list_value['type'] == 5)
				{
					$defaultvalues = explode(';', $field_form_list_value['default_value']);
					$tmp2 = array();
					foreach ($defaultvalues as $defaultvalue)
					{
						if ($defaultvalue != '')
						{
							$tmp = explode('=', $defaultvalue);
							$tmp2[$tmp[0]] = $tmp[1];
						}
					}
					$field_form_list[$field_form_list_key]['default_value'] = $tmp2;
				}
			}
		}
		else
			$update = false;

		if (Tools::isSubmit('id_ukooformpro_form'))
		{
			$admin_email_tpl_html = '';
			$admin_email_tpl_txt = '';
			$client_email_tpl_html = '';
			$client_email_tpl_txt = '';
			foreach (Language::getLanguages(true) as $lang)
			{
				$id_lang = $lang['id_lang'];
				$iso_code = $lang['iso_code'];
				$path = './../modules/ukooformpro/mails/'.$iso_code.'/admin'.Tools::getValue('id_ukooformpro_form').'.html';
				$admin_email_tpl_html[$id_lang] = UkooFormProFile::readFile($path);
				$path = './../modules/ukooformpro/mails/'.$iso_code.'/admin'.Tools::getValue('id_ukooformpro_form').'.txt';
				$admin_email_tpl_txt[$id_lang] = UkooFormProFile::readFile($path);
				$path = './../modules/ukooformpro/mails/'.$iso_code.'/client'.Tools::getValue('id_ukooformpro_form').'.html';
				$client_email_tpl_html[$id_lang] = UkooFormProFile::readFile($path);
				$path = './../modules/ukooformpro/mails/'.$iso_code.'/client'.Tools::getValue('id_ukooformpro_form').'.txt';
				$client_email_tpl_txt[$id_lang] = UkooFormProFile::readFile($path);
			}
			$this->loadObject(true)->admin_email_tpl_html = $admin_email_tpl_html;
			$this->loadObject(true)->admin_email_tpl_txt = $admin_email_tpl_txt;
			$this->loadObject(true)->client_email_tpl_html = $client_email_tpl_html;
			$this->loadObject(true)->client_email_tpl_txt = $client_email_tpl_txt;
		}

		if (Tools::isEmpty($this->object->active))
			$this->object->active = 1;

		$this->context->smarty->assign(
			array(
				'id_ukooformpro_form' => $current_object->id_ukooformpro_form,
				'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
				'languages' => Language::getLanguages(true),
				'update' => $update,
				'tokken' => Tools::getAdminTokenLite('AdminUkooFormProElements'),
				'currentToken' => $this->token,
				'currentObject' => $current_object,
				'currentTab' => $this,
				'fieldFormList' => $field_form_list,
				'position' => count($field_form_list) + 1,
				'front_form_link' => $this->context->link->getModuleLink('ukooformpro', 'form', array('id_form' => (int)$current_object->id_ukooformpro_form)),
			)
		);

		$this->addJquery('colorpicker');
		$this->addJqueryUI('ui.datepicker');
		$this->addJqueryUI('ui.sortable');
		return parent::renderForm();
	}

	public function processUpdate()
	{
		$this->writeClientEmailTpl();
		$this->writeAdminEmailTpl();

		return parent::processUpdate();
	}

	public function processAdd()
	{
		$this->writeClientEmailTpl();
		$this->writeAdminEmailTpl();

		return parent::processAdd();
	}

	public function afterAdd($current_object)
	{
		$current_object = '';
		$id = Form::selectLastInsert();
		$path = './../modules/ukooformpro/views/templates/front/form_tpl/form_id'.$id[0]['id'].'.tpl';
		$path .= $current_object;
		$contents = '<p>{l s=\'Please modify your tpl file\' mod=\'ukooformpro\'}</p>';
		UkooFormProFile::createFile($path, $contents);
	}

	/**
	 * create and update client email tpl
	 */
	public function writeClientEmailTpl()
	{
		$language = Language::getLanguages(true);
		foreach (Tools::getValue('client_email_tpl_html') as $key => $client_email_tpl_html)
		{
			foreach ($language as $lang)
			{
				if ($lang['id_lang'] == $key)
					$iso_code = $lang['iso_code'];
			}
			$path = './../modules/ukooformpro/mails/'.$iso_code;
			if (!file_exists($path))
				UkooFormProFile::createDirectory($path, 0777, true);
			$path = './../modules/ukooformpro/mails/'.$iso_code.'/client'.Tools::getValue('id_ukooformpro_form').'.html';
			UkooFormProFile::createFile($path, $client_email_tpl_html);
		}
		foreach (Tools::getValue('client_email_tpl_txt') as $key => $client_email_tpl_txt)
		{
			foreach ($language as $lang)
			{
				if ($lang['id_lang'] == $key)
					$iso_code = $lang['iso_code'];
			}
			$path = './../modules/ukooformpro/mails/'.$iso_code;
			if (!file_exists($path))
				UkooFormProFile::createDirectory($path, 0777, true);
			$path = './../modules/ukooformpro/mails/'.$iso_code.'/client'.Tools::getValue('id_ukooformpro_form').'.txt';
			UkooFormProFile::createFile($path, $client_email_tpl_txt);
		}
	}

	/**
	 * create and update admin email tpl
	 */
	public function writeAdminEmailTpl()
	{
		$language = Language::getLanguages(true);
		foreach (Tools::getValue('admin_email_tpl_html') as $key => $admin_email_tpl_html)
		{
			foreach ($language as $lang)
			{
				if ($lang['id_lang'] == $key)
					$iso_code = $lang['iso_code'];
			}
			$path = './../modules/ukooformpro/mails/'.$iso_code;
			if (!file_exists($path))
				UkooFormProFile::createDirectory($path, 0777, true);
			$path = './../modules/ukooformpro/mails/'.$iso_code.'/admin'.Tools::getValue('id_ukooformpro_form').'.html';
			UkooFormProFile::createFile($path, $admin_email_tpl_html);
		}
		foreach (Tools::getValue('admin_email_tpl_txt') as $key => $admin_email_tpl_txt)
		{
			foreach ($language as $lang)
			{
				if ($lang['id_lang'] == $key)
					$iso_code = $lang['iso_code'];
			}
			$path = './../modules/ukooformpro/mails/'.$iso_code;
			if (!file_exists($path))
				UkooFormProFile::createDirectory($path, 0777, true);
			$path = './../modules/ukooformpro/mails/'.$iso_code.'/admin'.Tools::getValue('id_ukooformpro_form').'.txt';
			UkooFormProFile::createFile($path, $admin_email_tpl_txt);
		}
	}

	public function postProcess()
	{
		if (Tools::isSubmit('deleteukooformpro_form'))
			$this->deleteForm(Tools::getValue('id_ukooformpro_form'));
		else if (Tools::isSubmit('ukooformpro_formBox'))
		{
			$forms = Tools::getValue('ukooformpro_formBox');
			foreach ($forms as $form)
				$this->deleteForm($form);
		}
		else
			parent::postProcess();
	}

	public function deleteForm($id_ukooformpro_form)
	{
		if (Elements::deleteElements($id_ukooformpro_form))
			if (Form::deleteForm($id_ukooformpro_form))
			{
				UkooFormProFile::removeFile('../modules/ukooformpro/views/templates/front/form_tpl/form_id'.$id_ukooformpro_form.'.tpl');
				$langs = Language::getLanguages(true);
				foreach ($langs as $lang)
				{
					UkooFormProFile::removeFile('../modules/ukooformpro/mails/'.$lang['iso_code'].'/admin'.$id_ukooformpro_form.'.html');
					UkooFormProFile::removeFile('../modules/ukooformpro/mails/'.$lang['iso_code'].'/admin'.$id_ukooformpro_form.'.txt');
					UkooFormProFile::removeFile('../modules/ukooformpro/mails/'.$lang['iso_code'].'/client'.$id_ukooformpro_form.'.html');
					UkooFormProFile::removeFile('../modules/ukooformpro/mails/'.$lang['iso_code'].'/client'.$id_ukooformpro_form.'.txt');
				}
				$this->confirmations[] = $this->module->l('Delete successful');
			}
			else
			{
				$this->errors[] = Tools::displayError($this->module->l('An error happened while deletion.'));
				return false;
			}
	}

	public function setMedia()
	{
		parent::setMedia();

		$this->addJS(_PS_MODULE_DIR_.'ukooformpro/views/js/adminukooformpro.js');
		$this->addCSS(_PS_MODULE_DIR_.'ukooformpro/views/css/adminukooformpro.css');
	}

}
