<?php
/**
 * Ukoo Form Pro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

class UkooFormProFormModuleFrontController extends ModuleFrontController
{

	private $error, $values;
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
		if (!Tools::isSubmit('id_form') || Tools::isEmpty(Tools::getValue('id_form')))
			Tools::redirect('404');
		if (!$this->form = Form::selectForm(Tools::getValue('id_form'), $this->context->language->id))
			Tools::redirect('404');
	}

	public function initContent()
	{
		parent::initContent();

		$elements = Elements::selectElements(Tools::getValue('id_form'), $this->context->language->id);

		foreach ($elements as $key => $value)
		{
			// récupération des valeurs soumises, sauf pour le champ "texte libre"
			if (count($this->values) > 0 && (int)$value['type'] != 8)
				$elements[$key]['default_value'] = $this->values[$value['id_ukooformpro_elements']];
			$elements[$key]['attribute'] = unserialize($value['attribute']);
			// Traitement spécifique pour les checkbox, radio et select
			if ($value['type'] == 3 || $value['type'] == 4 || $value['type'] == 5)
			{
				$defaultvalue = explode(';', $value['default_value']);
				$tmp2 = array();
				foreach ($defaultvalue as $test)
				{
					if ($test != '')
					{
						$tmp = explode('=', $test);
						$tmp2[$this->parserElements($tmp[0])] = $this->parserElements($tmp[1]);
					}
				}
				$elements[$key]['default_value'] = $tmp2;
				if (count($this->values) > 0 && $this->values[$value['id_ukooformpro_elements']] != '')
					$elements[$key]['value'] = $this->values[$value['id_ukooformpro_elements']];
			}
			else
				$elements[$key]['default_value'] = $this->parserElements($elements[$key]['default_value']);
			$elements[$key]['hint'] = $this->parserElements($elements[$key]['hint']);
			$elements[$key]['label'] = $this->parserElements($elements[$key]['label']);
			$elements[$key]['validation_message'] = $this->parserElements($elements[$key]['validation_message']);
		}

		$form = $this->form[0];

		if ($form['meta_title'] == '')
			$form['meta_title'] = $this->module->l('Form');

		if ($form['use_custom_tpl'] == 1)
		{
			foreach ($elements as $key => $values)
			{
				$elements['elt'.$values['id_ukooformpro_elements']] = $elements[$key];
				unset($elements[$key]);
			}
			$this->setTemplate('form_tpl/form_id'.$form['id_ukooformpro_form'].'.tpl');
		}
		else
			$this->setTemplate('form.tpl');

		$this->context->smarty->assign(array(
			'form' => $form,
			'elements' => $elements,
			'values' => $this->values,
			'error' => $this->error,
			'meta_title' => $form['meta_title'],
			'meta_description' => $form['meta_description'],
			'fileupload' => Configuration::get('PS_CUSTOMER_SERVICE_FILE_UPLOAD')
		));

		foreach ($elements as $element)
		{
			if ($element['type'] == 7)
				$this->addJqueryUI('ui.datepicker');
		}
	}

	public function postProcess()
	{
		if (Tools::isSubmit('id_ukooformpro_form'))
		{
			if ($form = Form::selectForm(Tools::getValue('id_ukooformpro_form'), $this->context->language->id))
			{
				$form = $form[0];
				if ($elements = Elements::selectElements(Tools::getValue('id_ukooformpro_form'), $this->context->language->id))
				{
					foreach ($elements as $element)
					{
						if ($element['required'])
						{
							if ($element['type'] != 6 && ($element['name'] != '' && Tools::getValue($element['name']) == ''))
								$this->error[$element['id_ukooformpro_elements']] = 0;
							else if ($element['type'] != 6
								&& (Tools::isSubmit('elt'.$element['id_ukooformpro_elements'])
									&& Tools::getValue('elt'.$element['id_ukooformpro_elements']) == ''
									|| !Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
								$this->error[$element['id_ukooformpro_elements']] = 0;
							else if ($element['type'] == 6 && $element['name'] != '' && $_FILES[$element['name']['error']] == 4)
								$this->error[$element['id_ukooformpro_elements']] = 0;
							else if ($element['type'] == 6 && $_FILES['elt'.$element['id_ukooformpro_elements']]['error'] == 4)
								$this->error[$element['id_ukooformpro_elements']] = 0;
						}
						if ($element['name'] != '')
							$this->values[$element['id_ukooformpro_elements']] = Tools::getValue($element['name']);
						else
							$this->values[$element['id_ukooformpro_elements']] = Tools::getValue('elt'.$element['id_ukooformpro_elements']);

						if ($element['type'] == 10)
						{
							$captcha = new Cookie('captcha');
							if ($captcha->value != Tools::getValue('elt'.$element['id_ukooformpro_elements']))
								$this->error[$element['id_ukooformpro_elements']] = 1;
							unset($_POST['elt'.$element['id_ukooformpro_elements']]);
						}
						if ($element['type'] == 6)
						{
							$extensions_refuse = array('php', 'html', 'css', 'js');
							$attribute = unserialize($element['attribute']);

							if (!Tools::isEmpty($attribute['extensions']))
							{
								$extensions = explode(';', $attribute['extensions']);
								foreach ($extensions_refuse as $extention_refuse_key => $extention_refuse_value)
								{
									if (in_array($extention_refuse_value, $extensions))
										unset($extensions_refuse[$extention_refuse_key]);
								}
							}
							if ($_FILES['elt'.$element['id_ukooformpro_elements']]['error'] == 0)
							{
								$explode = explode('.', $_FILES['elt'.$element['id_ukooformpro_elements']]['name']);
								$extention = $explode[count($explode) - 1];
								$name = '';
								$count_explode = count($explode);
								for ($i = 0; $i < $count_explode - 1; $i++)
									$name .= $explode[$i];
								$_FILES['elt'.$element['id_ukooformpro_elements']]['names'] = $name;
								$_FILES['elt'.$element['id_ukooformpro_elements']]['extention'] = $extention;
								$_FILES['elt'.$element['id_ukooformpro_elements']]['tab_name'] = 'elt'.$element['id_ukooformpro_elements'];
								$_FILES['elt'.$element['id_ukooformpro_elements']]['id_ukooformpro_elements'] = $element['id_ukooformpro_elements'];

								if ((isset($extensions) && !in_array($extention, $extensions)) || (in_array($extention, $extensions_refuse)))
								{
									$this->error[$element['id_ukooformpro_elements']] = 0;
									unset($_FILES['elt'.$element['id_ukooformpro_elements']]);
								}
								if (!Tools::isEmpty($attribute['size']) && ($attribute['size'] * 1000) < $_FILES['elt'.$element['id_ukooformpro_elements']])
								{
									$this->error[$element['id_ukooformpro_elements']] = 0;
									unset($_FILES['elt'.$element['id_ukooformpro_elements']]);
								}
							}
							else
								unset($_FILES['elt'.$element['id_ukooformpro_elements']]);
						}
						if ($element['type'] == 1 && !Tools::isEmpty(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
						{
							$attribute = unserialize($element['attribute']);
							$validation_rule = $attribute['validation_rule'];

							if ($validation_rule == 1)
								if (!ValidateCore::isEmail(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 1;
							if ($validation_rule == 2)
								if (!ValidateCore::isPhoneNumber(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 2;
							if ($validation_rule == 3)
								if (!ValidateCore::isName(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 3;
							if ($validation_rule == 4)
								if (!ValidateCore::isOrderInvoiceNumber(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 4;
							if ($validation_rule == 5)
								if (!ValidateCore::isFloat(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 5;
							if ($validation_rule == 6)
								if (!ValidateCore::isInt(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 6;
							if ($validation_rule == 7)
								if (!ValidateCore::isUrl(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 7;
							if ($validation_rule == 8)
								if (!ValidateCore::isEan13(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 8;
							if ($validation_rule == 9)
								if (!ValidateCore::isUpc(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 9;
							if ($validation_rule == 10)
								if (!ValidateCore::isSiret(Tools::getValue('elt'.$element['id_ukooformpro_elements'])))
									$this->error[$element['id_ukooformpro_elements']] = 10;
						}
					}
					if (Tools::isEmpty($this->error))
					{
						$error = false;
						if ($form['admin_email_send'] == 1)
							if (!$this->emailAdmin($_POST, $form))
								$error = true;
						if ($form['client_email_send'] == 1)
							if (!$this->emailClient($_POST, $form))
								$error = true;
						if ($form['save_datas'] == 1)
						{
							if (!Submission::insertSubmission($_POST, $this->context->customer->id, $this->context->language->id))
								return $this->context->smarty->assign(array(
										'error_mail' => 1,
								));
							if (isset($_FILES))
							{
								foreach ($_FILES as $file)
								{
									$id = Submission::fileSubmission($file);
									if ($id != false)
									{
										$_FILES[$file['tab_name']]['name'] = 'file_'.$id[0]['LAST_INSERT_ID()'].'.'.$_FILES[$file['tab_name']]['extention'];
										$uploader = new Uploader($file['tab_name']);
										$uploader->setAcceptTypes(array($file['extention']));
										$uploader->setSavePath('./modules/ukooformpro/files/');
										$file = $uploader->process();
									}
								}
							}
						}
						if ($error)
							$this->context->smarty->assign(array(
								'error_mail' => 1,
							));
						else
							$this->context->smarty->assign(array(
								'ty_message' => 1,
							));
					}
				}
			}
		}
	}

	/**
	 * function for send email to client
	 * @param type $post
	 * @param type $form
	 */
	public function emailClient($post, $form)
	{
		$client_email_to = $this->parserTag($post, $form['client_email_to']);
		$client_email_to_array = explode(',', $client_email_to);
		$client_email_to_name = $this->parserTag($post, $form['client_email_to_name']);
		$client_email_to_name_array = explode(',', $client_email_to_name);
		$client_email_bcc = $this->parserTag($post, $form['client_email_bcc']);
		$client_email_from_mail = $this->parserTag($post, $form['client_email_from_mail']);
		$client_email_from_name = $this->parserTag($post, $form['client_email_from_name']);
		$client_email_subject = $this->parserTag($post, $form['client_email_subject']);
		$client_email_to = explode(',', $client_email_to);
		if (Tools::isEmpty($client_email_bcc))
			$client_email_bcc = null;
		else
			$client_email_bcc = explode(',', $client_email_bcc);
		if ($client_email_subject == '')
			$client_email_subject = $this->module->l('Message from ').$this->context->shop->name;
		foreach (Language::getLanguages() as $lang)
		{
			if ($lang['id_lang'] == $form['id_lang'])
			{
				$tpl_html = './modules/ukooformpro/mails/'.$lang['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.html';
				$tpl_txt = './modules/ukooformpro/mails/'.$lang['iso_code'].'/client'.Tools::getValue('id_ukooformpro_form').'.txt';
			}
		}
		$file = $tpl_html;
		$handle = fopen($file, 'r');
		if (filesize($file) == 0)
			$size = 1;
		else
			$size = filesize($file);
		$tpl = fread($handle, $size);
		fclose($handle);
		$template_path = './modules/ukooformpro/mails/';
		$file = $tpl_txt;
		$handle = fopen($file, 'r');
		if (filesize($file) == 0)
			$size = 1;
		else
			$size = filesize($file);
		$tpl .= ' '.fread($handle, $size);
		fclose($handle);
		$template_vars = $this->parserTplVar($post, $tpl);
		return Mail::Send(
			$this->context->language->id,
			'client'.Tools::getValue('id_ukooformpro_form'),
			$client_email_subject,
			$template_vars,
			$client_email_to_array,
			$client_email_to_name_array,
			$client_email_from_mail,
			$client_email_from_name,
			null,
			null,
			$template_path,
			false,
			null,
			$client_email_bcc);
	}

	/**
	 * function for send an email to admin
	 * @param type $post
	 * @param type $form
	 */
	public function emailAdmin($post, $form)
	{
		$file_attachment = null;
		if (isset($_FILES))
		{
			$file_attachment = array();
			$files_keys = array_keys($_FILES);
			foreach ($files_keys as $files_key)
			{
				$attachment = Tools::fileAttachment($files_key);
				$file_attachment = array_merge($file_attachment, array($attachment));
			}
		}

		if (Tools::isEmpty($form['admin_email_to']))
			$admin_email_to = Configuration::get('PS_SHOP_EMAIL');
		else
			$admin_email_to = $this->parserTag($post, $form['admin_email_to']);
		$admin_email_to_array = explode(',', $admin_email_to);
		$admin_email_to_name = $this->parserTag($post, $form['admin_email_to_name']);
		$admin_email_to_name_array = explode(',', $admin_email_to_name);
		$admin_email_bcc = $this->parserTag($post, $form['admin_email_bcc']);
		$admin_email_from_mail = $this->parserTag($post, $form['admin_email_from_mail']);
		$admin_email_from_name = $this->parserTag($post, $form['admin_email_from_name']);
		$admin_email_subject = $this->parserTag($post, $form['admin_email_subject']);
		$admin_email_to = explode(',', $admin_email_to);
		$admin_email_to_name = explode(',', $admin_email_to_name);
		if (Tools::isEmpty($admin_email_bcc))
			$admin_email_bcc = null;
		else
			$admin_email_bcc = explode(',', $admin_email_bcc);
		$admin_email_from_name = explode(',', $admin_email_from_name);

		if (Tools::isEmpty($admin_email_subject))
			$admin_email_to_name = $this->module->l('Message from').$this->context->shop->name;

		foreach (Language::getLanguages() as $lang)
		{
			if ($lang['id_lang'] == $form['id_lang'])
			{
				$tpl_html = './modules/ukooformpro/mails/'.$lang['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.html';
				$tpl_txt = './modules/ukooformpro/mails/'.$lang['iso_code'].'/admin'.Tools::getValue('id_ukooformpro_form').'.txt';
			}
		}
		$file = $tpl_html;
		$handle = fopen($file, 'r');
		if (filesize($file) == 0)
			$size = 1;
		else
			$size = filesize($file);
		$tpl = fread($handle, $size);
		fclose($handle);
		$template_path = './modules/ukooformpro/mails/';
		$file = $tpl_txt;
		$handle = fopen($file, 'r');
		if (filesize($file) == 0)
			$size = 1;
		else
			$size = filesize($file);
		$tpl .= ' '.fread($handle, $size);
		fclose($handle);
		$template_vars = $this->parserTplVar($post, $tpl);
		return Mail::Send(
			$this->context->language->id,
			'admin'.Tools::getValue('id_ukooformpro_form'),
			$admin_email_subject,
			$template_vars,
			$admin_email_to_array,
			$admin_email_to_name_array,
			$admin_email_from_mail,
			$admin_email_from_name,
			$file_attachment,
			null,
			$template_path,
			false,
			null,
			$admin_email_bcc);
	}

	public function parserTag($post, $subject)
	{
		if (preg_match_all('/\{elt(.+)}/U', $subject, $matches))
			foreach ($matches[1] as $matche)
				if (isset($post['elt'.$matche]))
					$subject = preg_replace('/\{elt'.$matche.'}/', $post['elt'.$matche], $subject);
		return $subject;
	}

	/**
	 * function for recover tag value
	 * @param type $post
	 * @param type $subject
	 * @return type
	 */
	public function parserTplVar($post, $subject)
	{
		$template_vars = '';
		if (preg_match_all('/\{elt(.+)}/U', $subject, $matches))
		{
			foreach ($matches[1] as $matche)
			{
				if (isset($post['elt'.$matche]))
				{
					if (is_array($post['elt'.$matche]))
					{
						if (count($post['elt'.$matche]) > 1)
						{
							$array_post = '<ul>';
							foreach ($post['elt'.$matche] as $post_loop)
								$array_post .= '<li>'.$post_loop.'</li>';
							$array_post .= '</ul>';
						}
						else
							$array_post = $post['elt'.$matche][0];
						$post['elt'.$matche] = $array_post;
					}
					$template_vars['{elt'.$matche.'}'] = $post['elt'.$matche];
				}
			}
			if (isset($template_vars))
				return $template_vars;
		}
	}

	public function parserElements($subject)
	{
		if (preg_match_all('/\{user:(.+)}/U', $subject, $matches))
			foreach ($matches[1] as $matche)
				if (isset($this->context->customer->$matche))
					$subject = preg_replace('/\{user:'.$matche.'}/', $this->context->customer->$matche, $subject);
				else
					$subject = preg_replace('/\{user:'.$matche.'}/', '', $subject);
		if (preg_match_all('/\{var:(.+)}/U', $subject, $matches))
			foreach ($matches[1] as $matche)
				if (Tools::isSubmit($matche))
					$subject = preg_replace('/\{var:'.$matche.'}/', Tools::getValue($matche), $subject);
				else
					$subject = preg_replace('/\{var:'.$matche.'}/', '', $subject);
		return $subject;
	}

	public function setMedia()
	{
		parent::setMedia();

		$this->addCSS(_PS_MODULE_DIR_.'ukooformpro/views/css/ukooformpro.css');
	}

}
