<?php
/**
 * Ajoute une fonctionnalités d'agenda à votre boutique
 *
 * @author    Jérôme PERAT / Corrections Guillaume Heid - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014-2015
 * @license   Ukoo - Tous droits réservés
 *
 * "In Ukoo we trust!"
 */

class UkooFormProDownloadFileModuleFrontController extends ModuleFrontController
{

	public function __construct()
	{
		parent::__construct();
		if (Tools::isSubmit('id_file') && Tools::isSubmit('securekey') && Tools::getValue('securekey') === ConfigurationCore::get('UKOOFORMPRO_0'))
		{
			$file = Submission::selectDownloadFile(Tools::getValue('id_file'));
			$local_file = './modules/ukooformpro/files/file_'.$file['id_ukooformpro_submission_files'].'.'.$file['extention'];
			$download_file = $file['original_name'];
			if (file_exists($local_file) && is_file($local_file))
			{
				header('Cache-control: private');
				header('Content-Type: '.$file['type']);
				header('Content-Length: '.filesize($local_file));
				header('Content-Disposition: filename='.$download_file);
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				readfile($local_file);
				exit;
			}
		}
		else
			Tools::redirect('404');
	}

	public function initContent()
	{

	}

}
