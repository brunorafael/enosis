/**
 * UkooFormPro
 *
 * @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
 * @copyright Ukoo 2014
 * @license   Ukoo - Tous droits réservés
 */

$(document).ready(function ()
{
	$('.config_row').hide();
	$('.link.active').removeClass('active');
	var currentConfigRow = $('#currentConfigRow').val();
	$('#config_row_' + currentConfigRow).show();
	$('#link_' + currentConfigRow).addClass('active');

	$('.management_tab').hide();
	$('.management_link.active').parent().removeClass('active');
	var currentConfigTab = $('#currentConfigTab').val();
	$('#management_tab_' + currentConfigTab).show();
	$('#management_link_' + currentConfigTab).parent().addClass('active');

	if ($('#desc-ukooformpro_submission-new').length)
		$('#desc-ukooformpro_submission-new').hide();

	//Sort list elements
	if ($("#insert_elements").length > 0)
	{
		$("#insert_elements").sortable({
			placeholder: "ui-state-highlight",
			handle: ".dragHandle",
			axis: "y",
			helper: function (e, tr)
			{
				var $originals = tr.children();
				var $helper = tr.clone();
				$helper.children().each(function (index)
				{
					// Set helper cell sizes to match the original sizes
					$(this).width($originals.eq(index).width())
				});
				return $helper;
			},
			update: function () {
				var sortedIDs = $(".sortable").sortable("toArray");
				$.ajax({
					url: './index.php?controller=AdminUkooFormProElements&ajax&action=sorted',
					cache: false,
					async: true,
					type: 'POST',
					scriptCharset: 'utf8',
					data: {'token': $('#tokenElement').val(),
						'id_ukooformpro_form': $('#id_ukooformpro_form').val(),
						'sortedIDs': sortedIDs
					},
					error: function (request, status, error) {
						alert(request.responseText);
					},
					success: function () {
						for (i = 0; i < sortedIDs.length; i++)
						{
							$('#position' + sortedIDs[i].substring(4)).text(i + 1);
						}
					}
				});
			}
		});
		$("#insert_elements").disableSelection();
	}
});

function displayConfigRow(row)
{
	$('.config_row').hide();
	$('.list-group-item.active').removeClass('active');
	$('#config_row_' + row).show();
	$('#link_' + row).addClass('active');
	$('#currentConfigRow').val(row);
}

function displayManagementTab(tab)
{
	$('.management_tab').hide();
	$('.tab-row.management.active').removeClass('active');
	$('#management_tab_' + tab).show();
	$('#management_link_' + tab).parent().addClass('active');
	$('#currentConfigTab').val(tab);
}

function displayElementTab(tab)
{
	$('.element_tab').hide();
	$('.tab-row.element.active').removeClass('active');
	$('#element_tab_' + tab).show();
	$('#element_link_' + tab).parent().addClass('active');
}

function ajaxElement(tab)
{
	document.getElementById('submitModal').disabled = 'disabled';
	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: tab,
		error: function (request, status, error) {
			document.getElementById('submitModal').disabled = '';
			alert(request.responseText);
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			document.getElementById('submitModal').disabled = '';
			$('#modules_elements').modal('hide')
			$('#insert_elements').html(data);
			$('#position').val($('.positions').length + 1);
		}
	});
}

function ajaxElementActive(id, active)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax&action=active',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {id: id,
			'active': active,
			'token': $('#tokenElement').val()
		},
		error: function (request, status, error) {
			alert(request.responseText);
		},
		success: function (data, textStatus, request) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			$('#active' + id).html(data);
		}
	});
}

function ajaxElementUpdate(id)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax&action=update',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {id: id,
			'token': $('#tokenElement').val()
		},
		error: function (request, status, error) {
			alert(request.responseText);
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			$('#modules_elements').html(data);
			$('#modules_elements').modal('show');
		}
	});
}

function ajaxElementModal(type)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax&action=modal',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {'type': type,
			'position': $('.positions').length + 1,
			'id_ukooformpro_form': $('#id_ukooformpro_form').val(),
			'token': $('#tokenElement').val()
		},
		error: function (request, status, error) {
			alert(request.responseText);
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			else
			{
				$('#modules_elements').html(data);
				$('#modules_elements').modal('show');
			}
		}
	});
}

function ajaxDelete(id_ukooformpro_elements)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax&action=delete',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {'token': $('#tokenElement').val(),
			'id_ukooformpro_form': $('#id_ukooformpro_form').val(),
			'id_ukooformpro_elements': id_ukooformpro_elements,
			'sortedIDs': $(".sortable").sortable("toArray")
		},
		error: function (request, status, error) {
			alert(request.responseText);
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			$('#insert_elements').html(data);
			$('#position').val($('.positions').length + 1);
		}
	});
}

function ajaxChangeStatus(status, token, submission)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProSubmission&ajax&action=changestatus',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {
			'token': token,
			'submission': submission,
			'status': status
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			if (data != '')
				alert('date');
		}
	});
}

function ajaxCreateCustomerFromSubmission(token, submission)
{
	$.ajax({
		url: './index.php?controller=AdminUkooFormProSubmission&ajax&action=createcustomerfromsubmission',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {
			'token': token,
			'submission': submission
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
		}
	});
}

function ajaxDeleteSelected(token)
{
	var form = document.getElementsByName('elements_groupeBox[]');
	form = $(form).serializeArray();

	$.ajax({
		url: './index.php?controller=AdminUkooFormProElements&ajax&action=deleteselected',
		cache: false,
		type: 'POST',
		scriptCharset: 'utf8',
		data: {
			'token': token,
			'form': form,
			'positions': $(".sortable").sortable("toArray"),
			'id_ukooformpro_form': $('#id_ukooformpro_form').val()
		},
		success: function (data) {
			if (data.substring(0, 15) == '<!DOCTYPE html>')
				location.reload();
			$('#insert_elements').html(data);
			$('#position').val($('.positions').length + 1);
		}
	});
}