{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="panel">
    <h3>{l s='Submission info' mod='ukooformpro'}</h3>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='Submission ID' mod='ukooformpro'}</label>
		<div class="col-lg-3">{$currentObject->id_ukooformpro_submission|escape:'html':'UTF-8'}</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='Form\'s name' mod='ukooformpro'}</label>
		<div class="col-lg-3">{$form_name.name|escape:'html':'UTF-8'}</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='Date of submission' mod='ukooformpro'}</label>
		<div class="col-lg-3">{$currentObject->date_submission|escape:'html':'UTF-8'}</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='Lang' mod='ukooformpro'}</label>
		<div class="col-lg-3">{$lang_iso|escape:'html':'UTF-8'}</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='ID\'s customer' mod='ukooformpro'}</label>
		<div class="col-lg-3">
			{if $currentObject->id_customer == 0}
				{l s='Unknow' mod='ukooformpro'}
			{else}
				{$currentObject->id_customer|escape:'html':'UTF-8'}<br/>
				<a class="btn btn-default" href="index.php?controller=AdminCustomers&id_customer={$currentObject->id_customer|escape:'html':'UTF-8'}&viewcustomer&token={$tokenCustomers|escape:'html':'UTF-8'}">
					<span>{l s='Acces to customer\'s information' mod='ukooformpro'}</span>
				</a>
			{/if}
		</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='IP\'s customer' mod='ukooformpro'}</label>
		<div class="col-lg-3">{$currentObject->customer_ip|escape:'html':'UTF-8'}</div>
    </div>
    <div class="group clearfix">
		<label class="col-lg-2">{l s='Status' mod='ukooformpro'}</label>
		<div class="col-lg-2">
			<form>
				<select class="" name="status[]" onchange="ajaxChangeStatus($(this).serializeArray(), '{$currentToken|escape:'html':'UTF-8'}', '{$currentObject->id_ukooformpro_submission|escape:'html':'UTF-8'}');">
					<option value="1" {if $currentObject->status == 1}selected{/if}>{l s='New' mod='ukooformpro'}</option>
					<option value="2" {if $currentObject->status == 2}selected{/if}>{l s='Read' mod='ukooformpro'}</option>
					<option value="3" {if $currentObject->status == 3}selected{/if}>{l s='Treat' mod='ukooformpro'}</option>
				</select>
			</form>
		</div>
    </div>
    <br/><br/>
    <h3>{l s='Submission elements' mod='ukooformpro'}</h3>
    {foreach from = $table_values item=table_value}
		<div class="group clearfix">
			<label class="col-lg-2">{$table_value.label|escape:'html':'UTF-8'}</label>
			<div class="col-lg-6">
				{if $table_value.type != 10}
					{if $table_value.type == 6}
						{if $table_value.id != ''}
							<a class="btn btn-default" target="_blank" href="index.php?controller=AdminUkooFormProDownloadFile&id_file={$table_value.id|escape:'html':'UTF-8'}&token={$tokken2|escape:'html':'UTF-8'}">
								<span>{l s='See file' mod='ukooformpro'}</span>
							</a>
						{else}
							<p>{l s='No file' mod='ukooformpro'}</p>
						{/if}
					{else}
						{if is_array($table_value.value)}
							<ul>
								{foreach from = $table_value.value item=value}
									<li>{$value|escape:'html':'UTF-8'}</li>
									{/foreach}
							</ul>
						{else}
							{$table_value.value|escape:'html':'UTF-8'}
						{/if}
					{/if}
				{/if}
			</div>
		</div>
    {/foreach}
    <div class="panel-footer">
		<a id="desc-category-back" class="btn btn-default" href="index.php?controller=AdminUkooFormProSubmission&token={$currentToken|escape:'html':'UTF-8'}">
			<i class="process-icon-back "></i>
			<span>{l s='Back to list' mod='ukooformpro'}</span>
		</a>
    </div>
</div>