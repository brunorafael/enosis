{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2015
* @license   Ukoo - Tous droits réservés
*}

<script type="text/javascript">
	$(document).ready(function ()
	{
		$('.datepicker').datepicker({
			prevText: '',
			nextText: '',
			dateFormat: 'yy-mm-dd '
		});
	});
</script>

<form method="post" class="defaultForm form-horizontal">
    <div class="panel">
		<div class="panel-heading">
			{l s='Export submission' mod='ukooformpro'}
		</div>
		<div class="form-wrapper">
			<div class="form-group">
				<label class="control-label col-lg-3 required">
					{l s='Form\'s name' mod='ukooformpro'}
				</label>
				<select name="id_ukooformpro_form" class="fixed-width-xl">
					{foreach from = $forms item = form}
						<option value="{$form.id_ukooformpro_form|escape:'html':'UTF-8'}">{$form.name|escape:'html':'UTF-8'} (id {$form.id_ukooformpro_form|escape:'html':'UTF-8'})</option>
					{/foreach}
				</select>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-3 required">{l s='From' mod='ukooformpro'}</label>
				<div class="input-group col-lg-3">
					<input type="text" class="datepicker" name="date_from" id="date_from"/>
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-3 required">{l s='To' mod='ukooformpro'}</label>
				<div class="input-group col-lg-3">
					<input type="text" class="datepicker input-medium col-lg-3" name="date_to" id="date_to"/>
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-default pull-right" name="submitExportUkooFormPro" id="ukooformpro_form_export_btn">
				<i class="process-icon-export"></i> {l s='Export' mod='ukooformpro'}
			</button>
		</div>
    </div>
</form>

{$header}
{$content}
{$footer}