{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<a href="{$href|escape:'html':'UTF-8'}" title="{$action|escape:'html':'UTF-8'}"{if isset($name)} name="{$name|escape:'html':'UTF-8'}"{/if} class="default" target="_blank">
    <i class="icon-AdminParentLocalization"></i> {$action|escape:'html':'UTF-8'}
</a>