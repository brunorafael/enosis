{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}
<div class="form-wrapper">
    <div class="panel">
		<div class="form-group">
			<label class="control-label col-lg-3 required">
				<span class="label-tooltip" data-toggle="tooltip" title="{l s='Write the name of your form' mod='ukooformpro'}">{l s='Name' mod='ukooformpro'}</span>
			</label>
			<div class="col-lg-8">
				<input type="text" name="name" id="name" required="required" value="{$currentTab->getFieldValue($currentObject, 'name')|escape:'html':'UTF-8'}">
			</div>
		</div>
		<input type="hidden" value="0" name="admin_email_send"/>
		<input type="hidden" value="0" name="client_email_send"/>
		<input type="hidden" value="1" name="active"/>
		<input type="hidden" value="0" name="save_datas"/>
		<input type="hidden" value="0" name="use_custom_tpl"/>
		<input type="hidden" value="0" name="show_ty_message"/>
		<div class="panel-footer">
			<button type="submit" name="submitAddukooformpro_formAndStay" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Save' mod='ukooformpro'}
			</button>
			<a href="index.php?controller=AdminUkooFormProManagement&token={$currentToken|escape:'htmlall':'UTF-8'}" class="btn btn-default" onclick="window.history.back();">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='ukooformpro'}
			</a>
		</div>
    </div>
</div>
