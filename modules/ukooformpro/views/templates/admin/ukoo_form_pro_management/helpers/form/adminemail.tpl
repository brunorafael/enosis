{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-horizontal col-lg-10 config_row" id="config_row_adminemail">
    <div class="form-group">
		<label class="control-label col-lg-3 required"> 
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Enabled if you send a email after submit Form' mod='ukooformpro'}">
				{l s='Send a email to admin' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			<span class="switch prestashop-switch fixed-width-lg">
				<input type="radio" name="admin_email_send" id="admin_email_send_enabled" value="1" {if $currentTab->getFieldValue($currentObject, 'admin_email_send') == 1}checked="checked"{/if}>
				<label class="t" for="admin_email_send_enabled"> {l s='Enabled' mod='ukooformpro'}</label>
				<input type="radio" name="admin_email_send" id="admin_email_send_disabled" value="0" {if $currentTab->getFieldValue($currentObject, 'admin_email_send') == 0}checked="checked"{/if}>
				<label class="t" for="admin_email_send_disabled"> {l s='Disabled' mod='ukooformpro'}</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
    </div>	
    <div class="form-group">
		<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each email adress by ,' mod='ukooformpro'}">
				{l s='Admin email TO' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_to_{$language.id_lang|intval}" type="text"  name="admin_email_to_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_to', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each name by ,' mod='ukooformpro'}">
				{l s='Admin email TO name' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_to_name_{$language.id_lang|intval}" type="text"  name="admin_email_to_name_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_to_name', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    {*<div class="form-group">
    <label class="control-label col-lg-3">
    <span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each email adress by ;' mod='ukooformpro'}">
    {l s='Admin email CC' mod='ukooformpro'}
    </span>
    </label>
    <div class="col-lg-8">
    {foreach from=$languages item=language}
    {if $languages|count > 1}
    <div class="row">
    <div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
    <div class="col-lg-9">
    {/if}
    <input id="admin_email_cc_{$language.id_lang|intval}" type="text"  name="admin_email_cc_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_cc', $language.id_lang|intval)|escape:'html':'UTF-8'}">
    {if $languages|count > 1}
    </div>
    <div class="col-lg-2">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    {$language.iso_code|escape:'html':'UTF-8'}
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
    {foreach from=$languages item=language}
    <li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
    {/foreach}
    </ul>
    </div>
    </div>
    </div>
    {/if}
    {/foreach}
    </div>
    </div>*}
    <div class="form-group">
		<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each email adress by ,' mod='ukooformpro'}">
				{l s='Admin email BCC' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_bcc_{$language.id_lang|intval}" type="text"  name="admin_email_bcc_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_bcc', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">
			{l s='Admin email from mail' mod='ukooformpro'}
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_from_mail_{$language.id_lang|intval}" type="text"  name="admin_email_from_mail_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_from_mail', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">
			{l s='Admin email from name' mod='ukooformpro'}
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_from_name_{$language.id_lang|intval}" type="text"  name="admin_email_from_name_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_from_name', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    {*<div class="form-group">
    <label class="control-label col-lg-3">
    <span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each email adress by ;' mod='ukooformpro'}">
    {l s='Admin email reply to mail' mod='ukooformpro'}
    </span>
    </label>
    <div class="col-lg-8">
    {foreach from=$languages item=language}
    {if $languages|count > 1}
    <div class="row">
    <div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
    <div class="col-lg-9">
    {/if}
    <input id="admin_email_replyto_mail_{$language.id_lang|intval}" type="text"  name="admin_email_replyto_mail_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_replyto_mail', $language.id_lang|intval)|escape:'html':'UTF-8'}">
    {if $languages|count > 1}
    </div>
    <div class="col-lg-2">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    {$language.iso_code|escape:'html':'UTF-8'}
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
    {foreach from=$languages item=language}
    <li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
    {/foreach}
    </ul>
    </div>
    </div>
    </div>
    {/if}
    {/foreach}
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-lg-3">
    <span class="label-tooltip" data-toggle="tooltipe" title="{l s='Separet each email adress by ;' mod='ukooformpro'}">
    {l s='Admin email reply to name' mod='ukooformpro'}
    </span>
    </label>
    <div class="col-lg-8">
    {foreach from=$languages item=language}
    {if $languages|count > 1}
    <div class="row">
    <div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
    <div class="col-lg-9">
    {/if}
    <input id="admin_email_replyto_name_{$language.id_lang|intval}" type="text"  name="admin_email_replyto_name_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_replyto_name', $language.id_lang|intval)|escape:'html':'UTF-8'}">
    {if $languages|count > 1}
    </div>
    <div class="col-lg-2">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    {$language.iso_code|escape:'html':'UTF-8'}
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
    {foreach from=$languages item=language}
    <li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
    {/foreach}
    </ul>
    </div>
    </div>
    </div>
    {/if}
    {/foreach}
    </div>
    </div>*}
    <div class="form-group">
		<label class="control-label col-lg-3">
			{l s='Admin email subject' mod='ukooformpro'}
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="admin_email_subject_{$language.id_lang|intval}" type="text"  name="admin_email_subject_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'admin_email_subject', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Admin email template (html)' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<textarea style="height: 250px;" id="admin_email_tpl_html_{$language.id_lang}" name="admin_email_tpl_html[{$language.id_lang}]" class="{if isset($class)}{$class}{else}textarea-autosize{/if} autoload_rte">{$currentTab->getFieldValue($currentObject, 'admin_email_tpl_html', $language.id_lang|intval)|escape:'html':'UTF-8'}</textarea>
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Admin email template (txt)' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<textarea style="height: 250px;" id="admin_email_tpl_txt{$language.id_lang}" name="admin_email_tpl_txt[{$language.id_lang}]" class="{if isset($class)}{$class}{else}textarea-autosize{/if}">{$currentTab->getFieldValue($currentObject, 'admin_email_tpl_txt', $language.id_lang|intval)|escape:'html':'UTF-8'}</textarea>
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
</div>