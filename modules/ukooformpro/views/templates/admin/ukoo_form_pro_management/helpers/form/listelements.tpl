{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

{if count($fieldFormList) == 0}
    <tr id='nosort'>
	<td class="list-empty" colspan="7">
	    <div class="list-empty-msg">
		<i class="icon-warning-sign list-empty-icon"></i>
		{l s='No Elements find' mod='ukooformpro'}
	    </div>
	</td>
    </tr>
{else}
    {foreach from=$fieldFormList item=field}
	<tr id="sort{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}" class="ui-state-default">
	    <td class="text-center">
		<input type="checkbox" name="elements_groupeBox[]" value="{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}" class="nonborder"/>
	    </td>
	    <td class="pointer left">
		{literal}{elt{/literal}{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}{literal}}{/literal}
	    </td>
	    <td class="pointer left">
		<label for="elt{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		       {if $field.hint !=''}title="{$field.hint|escape:'html':'UTF-8'}"{/if}>
		    {$field.label|escape:'html':'UTF-8'}
		    {if $field.required==1}
			<sup>{if $currentObject->required_sign[$id_lang_default] != ''}{$currentObject->required_sign[$id_lang_default]|escape:'html':'UTF-8'}{else}*{/if}</sup>
		    {/if} 
		</label>
	    </td>
	    <td class="pointer left" style="width: 40%;">
		{if $field.type == 1}
		    <input type="text" value="{$field.default_value|escape:'html':'UTF-8'}"/>
		{elseif $field.type == 2}
		    <textarea>{$field.default_value|escape:'html':'UTF-8'}</textarea>
		{elseif $field.type == 3}
		    <ul class="checkbox_group">
			{foreach from=$field.default_value item=value key=label}
			    <li>
				<input type="checkbox" value="{$value|escape:'html':'UTF-8'}"/>
				<label>{$label|escape:'html':'UTF-8'}</label>
			    </li>
			{/foreach}
		    </ul>
		{elseif $field.type == 4}
		    <ul class="radio_group">
			{foreach from=$field.default_value item=value key=label}
			    <li>
				<input type="radio" value="{$value|escape:'html':'UTF-8'}"/>
				<label>{$label|escape:'html':'UTF-8'}</label>
			    </li>
			{/foreach}
		    </ul>
		{elseif $field.type == 5}
		    <select class="select_group">
			{foreach from=$field.default_value item=value key=label}
			    <option value="{$value|escape:'html':'UTF-8'}">
				{$label|escape:'html':'UTF-8'}
			    </option>
			{/foreach}
		    </select>
		{elseif $field.type == 6}
		    <div>
			<input id="PS_LOGO" type="file" name="PS_LOGO" class="hide"/>
			<div class="dummyfile input-group">
			    <span class="input-group-addon">
				<i class="icon-file"></i>
			    </span>
			    <input id="PS_LOGO-name" type="text" name="" readonly>
			    <span class="input-group-btn">
				<button id="PS_LOGO-selectbutton" type="button" class="btn btn-default">
				    <i class="icon-folder-open"></i>
				    {l s=' Add File ' mod='ukooformpro'}
				</button>
			    </span>
			</div>
		    </div>
		{elseif $field.type == 7}
		    <script type="text/javascript">
			{literal}$(function () {
				$('#elt{/literal}{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}{literal}').datepicker();
			    });{/literal}
			</script>
			<input type="text"
			       name="elt{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			       id="elt{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			{if isset($field.additional_attribute)}{$field.additional_attribute}{/if}
				/>
		    {elseif $field.type == 8}
				<p>{$field.default_value}</p>
			{elseif $field.type == 9}
			    <input type="text" readonly value="{$field.default_value|escape:'htmlall':'UTF-8'}"/>
			{elseif $field.type == 10}
				{$message = $field.attribute.refresh_text[$default_language]}
				<div>
				    <img id='captchaimg' src="../modules/ukooformpro/captcha.php?background={$field.attribute.background_color|escape:'html':'UTF-8'}&amp;text={$field.attribute.text_color|escape:'html':'UTF-8'}">
				    {*<label>{if $field.label != ''}{$field.label|escape:'html':'UTF-8'}{else}{l s='Enter the code above here :' mod='ukooformpro'}{/if}</label><br/>*}
				    <br/>
				    <a href='#'>{if $message!=''}{$message|escape:'html':'UTF-8'}{else}{l s='Can\'t read the image? click here' mod='ukooformpro'}{/if}</a>
				    <input type="text"
				    {if isset($field.additional_attribute)}{$field.additional_attribute}{/if}
				    id="elt{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}"
				    name ="captcha_code"
				    />
			    </div>
			{elseif $field.type == 11}
				<button type="button" value="{$field.default_value|escape:'html':'UTF-8'}">
					{$field.label|escape:'html':'UTF-8'}
				</button>					
			{/if}
			</td>
			<td class="pointer dragHandle fixed-width-xs center">
				<div class="dragGroup">
				<div class="positions" id="position{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}">{$field.position|escape:'html':'UTF-8'}</div>
				</div>
			</td>
			<td class="pointer fixed-width-xs text-center" id="active{$field.id_ukooformpro_elements|escape:'html':'UTF-8'}">
				{include file='./activeelement.tpl'}
			</td>
			<td class="text-right">
				<div class="btn-group-actionu">
				<div class="btn-group pull-right">
					<a href="#" class="btn btn-default" title="{l s='Edit' mod='ukooformpro'}" onclick="ajaxElementUpdate({$field.id_ukooformpro_elements})">
					<i class="icon-pencil"></i>
					{l s='Edit' mod='ukooformpro'}
					</a>
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="icon-caret-down"></i>
					</button>
					<ul class="dropdown-menu">
					<li>
						<a href="#" onclick="ajaxDelete({$field.id_ukooformpro_elements|escape:'html':'UTF-8'})" class="delete" title="{l s='Delete' mod='ukooformpro'}">
						<i class="icon-trash"></i>
						{l s='Delete' mod='ukooformpro'}
						</a>
					</li>
					</ul>
				</div>
				</div>
			</td>
			</tr>
	{/foreach}
{/if}

{if count($fieldFormList) > 1}
	<script>
		$(document).ready(function () {
		$('#bulk').show();
		});
	</script>
{else}
	<script>
		$(document).ready(function () {
		$('#bulk').hide();
		});
	</script>
{/if}