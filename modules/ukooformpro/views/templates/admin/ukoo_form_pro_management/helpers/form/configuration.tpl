{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="row">
    <div class="list-group col-lg-2">
		<a class="list-group-item" id="link_generalinfortmation" href="javascript:displayConfigRow('generalinfortmation');">{l s='General information' mod='ukooformpro'}</a>
		<a class="list-group-item" id="link_adminemail" href="javascript:displayConfigRow('adminemail');">{l s='Admin Email' mod='ukooformpro'}</a>
		<a class="list-group-item" id="link_clientemail" href="javascript:displayConfigRow('clientemail');">{l s='Client Email' mod='ukooformpro'}</a>

    </div>
    {include file='./generalconfiguration.tpl'}
    {include file='./adminemail.tpl'}
    {include file='./clientemail.tpl'}
</div>