{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}
<script type="text/javascript">
	var iso = '{$iso|addslashes}';
	var pathCSS = '{$smarty.const._THEME_CSS_DIR_|addslashes}';
	var ad = '{$ad|addslashes}';

	$(document).ready(function ()
	{
		tinySetup({
			editor_selector: "autoload_rte"
		});
	});
</script>
<script src="../js/jquery/plugins/jquery.colorpicker.js" type="text/javascript"></script>
<form action="{$currentIndex|escape:'htmlall':'UTF-8'}&amp;token={$currentToken|escape:'htmlall':'UTF-8'}" id="ukooformpro_form_form" method="post" enctype="multipart/form-data" class="form-horizontal" novalidate>
    <input type="hidden" value="generalinfortmation" id="currentConfigRow"/>
    <input type="hidden" value="elements" id="currentConfigTab"/>
    <input type="hidden" value="{$id_ukooformpro_form|escape:'html':'UTF-8'}" id="id_ukooformpro_form" name="id_ukooformpro_form">
    <input type="hidden" value="{$tokken|escape:'html':'UTF-8'}" id="tokenElement" name="tokken"/>
{if $update == true}{include file='./update.tpl'}{else}{include file='./add.tpl'}{/if}
</form>
<div class="modal fade" id="modules_elements"></div>
<script>
	$(document).ready(function () {
		$('#modules_elements').appendTo('#main #content');
	});
</script>