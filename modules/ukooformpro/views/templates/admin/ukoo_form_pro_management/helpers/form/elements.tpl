{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="row">
    <div class="list-group col-lg-2">
		<a class="list-group-item elt1" id="elements_link_textbox" href="#"  onclick="ajaxElementModal(1)">{l s='TextBox' mod='ukooformpro'}</a>
		<a class="list-group-item elt2" id="elements_link_textarea" href="#" onclick="ajaxElementModal(2)">{l s='TextArea' mod='ukooformpro'}</a>
		<a class="list-group-item elt3" id="elements_link_checkbox" href="#" onclick="ajaxElementModal(3)">{l s='CheckBox' mod='ukooformpro'}</a>
		<a class="list-group-item elt4" id="elements_link_radio" href="#" onclick="ajaxElementModal(4)">{l s='Radio' mod='ukooformpro'}</a>
		<a class="list-group-item elt5" id="elements_link_select" href="#" onclick="ajaxElementModal(5)">{l s='Select' mod='ukooformpro'}</a>
		<a class="list-group-item elt6" id="elements_link_fileupdate" href="#" onclick="ajaxElementModal(6)">{l s='File Upload' mod='ukooformpro'}</a>
		<a class="list-group-item elt7" id="elements_link_calendar" href="#" onclick="ajaxElementModal(7)">{l s='Calendar' mod='ukooformpro'}</a>
		<a class="list-group-item elt8" id="elements_link_freetext" href="#" onclick="ajaxElementModal(8)">{l s='Free Text' mod='ukooformpro'}</a>
		<a class="list-group-item elt9" id="elements_link_hiddenfield" href="#" onclick="ajaxElementModal(9)">{l s='Hidden Field' mod='ukooformpro'}</a>
		<a class="list-group-item elt10" id="elements_link_captcha" href="#" onclick="ajaxElementModal(10)">{l s='Captcha' mod='ukooformpro'}</a>
		<a class="list-group-item elt11" id="elements_link_reset" href="#" onclick="ajaxElementModal(11)">{l s='Button' mod='ukooformpro'}</a>
    </div>
    <div class="table-responsive clearfix col-lg-10">
		<table class="table ukooformpro_elments">
			<thead>
				<tr nodrag nodrop>
					<th class="center fixed-width-xs"></th>
					<th class="title_box">Tag</th>
					<th class="title_box">{l s='Label' mod='ukooformpro'}</th>
					<th class="title_box" style="width: 40%;">{l s='Preview' mod='ukooformpro'}</th>
					<th class="title_box center">{l s='Position' mod='ukooformpro'}</th>
					<th class="title_box center">{l s='Active' mod='ukooformpro'}</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="insert_elements" class="sortable">
				{include file='./listelements.tpl'}			
			</tbody>
		</table>
    </div>
    <div class="row" id="bulk">
		<div class="col-lg-6">
			<div class="btn-group bulk-actions">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					{l s='Bulk actions' mod='ukooformpro'}
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a href="#" onclick="javascript:checkDelBoxes($(this).closest('form').get(0), 'elements_groupeBox[]', true);
								return false;">
							<i class="icon-check-sign"></i>
							{l s='Select all' mod='ukooformpro'}
						</a>
					</li>
					<li>
						<a href="#" onclick="javascript:checkDelBoxes($(this).closest('form').get(0), 'elements_groupeBox[]', false);
								return false;">
							<i class="icon-check-empty"></i>
							{l s='Unselect all' mod='ukooformpro'}
						</a>
					</li>
					<li class='divider'></li>
					<li>
						<a href="#" onclick="if (confirm('Delete selected items?'))
									ajaxDeleteSelected('{$tokken|escape:'htmlall':'UTF-8'}');">
							<i class="icon-trash"></i>
							{l s='Delete selected' mod='ukooformpro'}
						</a>
					</li>
				</ul>
			</div>
		</div>
    </div>
</div>