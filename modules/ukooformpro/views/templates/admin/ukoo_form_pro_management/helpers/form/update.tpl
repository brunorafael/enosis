{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-wrapper">
    <div class="productTabs">
		<ul class="tab nav nav-tabs">
			<li class="tab-row management">
				<a class="tab-page" id="management_link_elements" href="javascript:displayManagementTab('elements');"> {l s='Elements' mod='ukooformpro'}</a>
			</li>
			<li class="tab-row management">
				<a class="tab-page" id="management_link_configuration" href="javascript:displayManagementTab('configuration');"> {l s='Configuration' mod='ukooformpro'}</a>
			</li>
		</ul>
    </div>
    <div class="panel management_tab" id="management_tab_elements">
		{include file='./elements.tpl'}
		<div class="panel-footer">
			<button type="submit" name="submitAddukooformpro_form" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Save' mod='ukooformpro'}
			</button>
			<button type="submit" name="submitAddukooformpro_formAndStay" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Save and stay' mod='ukooformpro'}
			</button>
			<a href="index.php?controller=AdminUkooFormProManagement&token={$currentToken|escape:'htmlall':'UTF-8'}" class="btn btn-default" onclick="window.history.back();">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='ukooformpro'}
			</a>
		</div>
    </div>
    <div class="panel management_tab" id="management_tab_configuration">
		{include file='./configuration.tpl'}
		<div class="panel-footer">
			<button type="submit" name="submitAddukooformpro_form" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Save' mod='ukooformpro'}
			</button>
			<button type="submit" name="submitAddukooformpro_formAndStay" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Save and stay' mod='ukooformpro'}
			</button>
			<a href="index.php?controller=AdminUkooFormProManagement&token={$currentToken|escape:'htmlall':'UTF-8'}" class="btn btn-default" onclick="window.history.back();">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='ukooformpro'}
			</a>
		</div>
    </div>
</div>

<li id="show_form">
    <a class="toolbar_btn btn-link" href="{$front_form_link|escape:'html':'UTF-8'}" target="_blank">
		<i class="process-icon-preview previewUrl"></i>
		{l s='Show Form' mod='ukooformpro'}
    </a>
</li>

<script>
	$(document).ready(function () {
		$('#show_form').prependTo('#toolbar-nav');
	});
</script>