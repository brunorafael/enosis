{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

{if $field.active == 1}
    <a class="list-action-enable  action-enabled" href="#" onclick="ajaxElementActive({$field.id_ukooformpro_elements|escape:'html':'UTF-8'}, {$field.active|escape:'html':'UTF-8'});">
		<i class="icon-check"></i>
		<i class="icon-remove hidden"></i>
    </a>
{else}
    <a class="list-action-enable  action-disabled" href="#" onclick="ajaxElementActive({$field.id_ukooformpro_elements|escape:'html':'UTF-8'}, {$field.active|escape:'html':'UTF-8'});">
		<i class="icon-check hidden"></i>
		<i class="icon-remove"></i>
    </a>
{/if}