{**
* UkooFormPro
*
* @author   Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}
<script type="text/javascript">
	var iso = '{$iso|addslashes}';
	var pathCSS = '{$smarty.const._THEME_CSS_DIR_|addslashes}';
	var ad = '{$ad|addslashes}';

	$(document).ready(function ()
	{
		tinySetup({
			editor_selector: "autoload_rte"
		});
	});
</script>

<div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title" id="modal-title">
				{if $type == 1}
					{l s='TextBox' mod='ukooformpro'}
				{else if $type == 2}
					{l s='TextArea' mod='ukooformpro'}
				{else if $type == 3}
					{l s='CheckBox' mod='ukooformpro'}
				{else if $type == 4}
					{l s='Radio' mod='ukooformpro'}
				{else if $type == 5}
					{l s='Select' mod='ukooformpro'}
				{else if $type == 6}
					{l s='File Upload' mod='ukooformpro'}
				{else if $type == 7}
					{l s='Calendar' mod='ukooformpro'}
				{else if $type == 8}
					{l s='Free Text' mod='ukooformpro'}
				{else if $type == 9}
					{l s='Hidden Field' mod='ukooformpro'}
				{else if $type == 10}
					{l s='Captcha' mod='ukooformpro'}
				{else if $type == 11}
					{l s='Button' mod='ukooformpro'}
				{/if}{$module_dir}
			</h3>
		</div>
		<div class="modal-body panel">
			<form metod="post" action="#" class="form-horizontal" id="ukooformpro_elements" onsubmit="tinyMCE.triggerSave();
					ajaxElement($(this).serialize());
					return false;">
				<input type="hidden" value="{$type|escape:'html':'UTF-8'}" id="type" name="type"/>
				<input type="hidden" value="{$id_ukooformpro_form|escape:'html':'UTF-8'}" id="form_id" name="id_ukooformpro_form"/>
				<input type="hidden" value="{$tokken|escape:'html':'UTF-8'}" id="token" name="token"/>
				<input type="hidden" value="{$position|escape:'html':'UTF-8'}" id="position" name="position"/>
				<input type="hidden" value="{if $id_ukooformpro_elements != ''}{$id_ukooformpro_elements|escape:'html':'UTF-8'}{else}0{/if}" id="id_ukooformpro_elements" name="id_ukooformpro_elements"/>
				<input type="hidden" value="{if $active == ''}1{else}{$active|escape:'html':'UTF-8'}{/if}" id="active" name="active"/>
				{if $type == 8 || $type == 9 || $type == 11}
					<input type="hidden" value="0" name="required"/>
				{/if}
				<div class="elementsTabs">
					<ul class="tab nav nav-tabs">
						<li class="tab-row element active">
							<a class="tab-page" id="element_link_general" href="javascript:displayElementTab('general');">{l s='General' mod='ukooformpro'}</a>
						</li>
						{if $type != 8 && $type != 9 && $type != 11}
							<li class="tab-row element">
								<a class="tab-page" id="element_link_validation" href="javascript:displayElementTab('validation');">{l s='Validation' mod='ukooformpro'}</a>
							</li>
						{/if}
						<li class="tab-row element">
							<a class="tab-page" id="element_link_attributes" href="javascript:displayElementTab('attributes');">{l s='Attributes' mod='ukooformpro'}</a>
						</li>
					</ul>
				</div>
				<div class="panel element_tab" id="element_tab_general">
					<div class="form-horizontal">
						{if $type != 8 && $type != 9}
							<div class="form-group" id="labelmodal">
								<label class="control-label col-lg-3">{l s='Label' mod='ukooformpro'}</label>
								<div class="col-lg-9">
									{foreach from=$languages item=language}
										{if $languages|count > 1}
											<div class="row">
												<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
													<div class="col-lg-9">
													{/if}
													<input id="label_{$language.id_lang|intval}" type="text"  name="label_{$language.id_lang|intval}" value="{$label[$language.id_lang|intval]|escape:'html':'UTF-8'}" maxlength="255">
													{if $languages|count > 1}
													</div>
													<div class="col-lg-2">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
															{$language.iso_code|escape:'html':'UTF-8'}
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															{foreach from=$languages item=language}
																<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
																{/foreach}
														</ul>
													</div>
												</div>
											</div>
										{/if}
									{/foreach}
								</div>
							</div>
							{if $type == 11}
								<div class="form-group modalform" id="button_type">
									<label class="col-lg-3 control-label">{l s='Type' mod='ukooformpro'}</label>
									<div class="col-lg-8">
										<select class="form-control fixed-width-lg" id="element_flow" name="button_type">
											<option value="1" {if $button_type == 1}selected{/if}>{l s='Submit' mod='ukooformpro'}</option>
											<option value="2" {if $button_type == 2}selected{/if}>{l s='Reset' mod='ukooformpro'}</option>
											<option value="3" {if $button_type == 3}selected{/if}>{l s='Button' mod='ukooformpro'}</option>
										</select>
									</div>
								</div>
							{/if}
							{if $type == 10}
								<div class="form-group" id="refresh_captcha">
									<label class="control-label col-lg-3">{l s='Refresh message' mod='ukooformpro'}</label>
									<div class="col-lg-9">
										{foreach from=$languages item=language}
											{if $languages|count > 1}
												<div class="row">
													<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
														<div class="col-lg-9">
														{/if}
														<input id="refresh_captcha_{$language.id_lang|intval}" type="text"  name="refresh_captcha_{$language.id_lang|intval}" value="{$refresh_captcha[$language.id_lang|intval]|escape:'html':'UTF-8'}">
														{if $languages|count > 1}
														</div>
														<div class="col-lg-2">
															<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
																{$language.iso_code|escape:'html':'UTF-8'}
																<span class="caret"></span>
															</button>
															<ul class="dropdown-menu">
																{foreach from=$languages item=language}
																	<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
																	{/foreach}
															</ul>
														</div>
													</div>
												</div>
											{/if}
										{/foreach}
									</div>
								</div>
							{/if}
							<div class="form-group" id="hint">
								<label class="control-label col-lg-3">{l s='Hint' mod='ukooformpro'}</label>
								<div class="col-lg-9">
									{foreach from=$languages item=language}
										{if $languages|count > 1}
											<div class="row">
												<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
													<div class="col-lg-9">
													{/if}
													<textarea style="height: 125px" id="hint_{$language.id_lang|intval}" name="hint_{$language.id_lang|intval}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}" maxlength="255">{$hint[$language.id_lang|intval]|escape:'html':'UTF-8'}</textarea>
													{if $languages|count > 1}
													</div>
													<div class="col-lg-2">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
															{$language.iso_code|escape:'html':'UTF-8'}
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															{foreach from=$languages item=language}
																<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
																{/foreach}
														</ul>
													</div>
												</div>
											</div>
										{/if}
									{/foreach}
								</div>
							</div>
						{/if}
						{if $type == 1 || $type == 2 || $type == 3 || $type == 4 || $type == 5 || $type == 8 || $type == 9 || $type == 11}
							<div class="form-group modalform">
								{if $type == 1 || $type == 2 || $type == 9 || $type == 11}
									<label class="control-label col-lg-3" id="default_value">{l s='Default value' mod='ukooformpro'}</label>
								{/if}
								{if $type == 3 || $type == 4 || $type == 5}
									<label class="control-label col-lg-3" id="option">
										<span class="label-tooltip" data-toggle="tooltip" title="{l s='label=value;' mod='ukooformpro'}">
											{l s='Option' mod='ukooformpro'}
										</span>
									</label>
								{/if}
								{if $type == 8}
									<label class="control-label col-lg-3" id="text">{l s='Text' mod='ukooformpro'}</label>
								{/if}
								<div class="col-lg-9">
									{foreach from=$languages item=language}
										{if $languages|count > 1}
											<div class="row">
												<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
													<div class="col-lg-9">
													{/if}
													<textarea style="height: 125px;" id="default_value_{$language.id_lang}" name="default_value_{$language.id_lang}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}{if $type == 8} autoload_rte{/if}" >{$default_value[$language.id_lang|intval]|escape:'html':'UTF-8'}</textarea>
													{if $languages|count > 1}
													</div>
													<div class="col-lg-2">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
															{$language.iso_code|escape:'html':'UTF-8'}
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															{foreach from=$languages item=language}
																<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
																{/foreach}
														</ul>
													</div>
												</div>
											</div>
										{/if}
									{/foreach}
								</div>
							</div>
						{/if}
					</div>
				</div>
				{if $type != 8 && $type != 9 && $type != 11}
					<div class="panel element_tab" id="element_tab_validation" style="display: none;">
						<div class="form-horizontal">
							{if $type != 10}
								<div class="form-group" id="required">
									<label class="control-label col-lg-3">{l s='Required' mod='ukooformpro'}</label>
									<div class='col-lg-8'>
										<span class="switch prestashop-switch fixed-width-lg">
											<input type="radio" name="required" id="element_required_yes" value="1" {if $required == 1}checked="checked"{/if}>
											<label class="t" for="element_required_yes">{l s='Yes' mod='ukooformpro'}</label>
											<input type="radio" name="required" id="element_required_no" value="0" {if $required != 1}checked="checked"{/if}>
											<label class="t" for="element_required_no">{l s='No' mod='ukooformpro'}</label>
											<a class="slide-button btn"></a>
										</span>
									</div>
								</div>
							{else}
								<input type="hidden" name="required" value="0"/>
							{/if}
							{if $type == 1}
								<div class="form-group" id="validation_rule">
									<label class="control-label col-lg-3">{l s='Validation rule' mod='ukooformpro'}</label>
									<div class="col-lg-8">
										<select name="validation_rule" class="fixed-width-xl">
											<option value="0">{l s='No validation' mod='ukooformpro'}</option>
											<option value="1" {if $validation_rule == 1}selected="selected"{/if}>{l s='is Email' mod='ukooformpro'}</option>
											<option value="2" {if $validation_rule == 2}selected="selected"{/if}>{l s='is Phone number' mod='ukooformpro'}</option>
											<option value="3" {if $validation_rule == 3}selected="selected"{/if}>{l s='is Name' mod='ukooformpro'}</option>
											<option value="4" {if $validation_rule == 4}selected="selected"{/if}>{l s='is Order invoice number' mod='ukooformpro'}</option>
											<option value="5" {if $validation_rule == 5}selected="selected"{/if}>{l s='is Float' mod='ukooformpro'}</option>
											<option value="6" {if $validation_rule == 6}selected="selected"{/if}>{l s='is Integer' mod='ukooformpro'}</option>
											<option value="7" {if $validation_rule == 7}selected="selected"{/if}>{l s='is Url' mod='ukooformpro'}</option>
											<option value="8" {if $validation_rule == 8}selected="selected"{/if}>{l s='is EAN13' mod='ukooformpro'}</option>
											<option value="9" {if $validation_rule == 9}selected="selected"{/if}>{l s='is UPC' mod='ukooformpro'}</option>
											<option value="10" {if $validation_rule == 10}selected="selected"{/if}>{l s='is Siret' mod='ukooformpro'}</option>
										</select>
									</div>
								</div>
							{/if}
							<div class="form-group" id="validation_message">
								<label class="control-label col-lg-3">{l s='Validation message' mod='ukooformpro'}</label>
								<div class="col-lg-9">
									{foreach from=$languages item=language}
										{if $languages|count > 1}
											<div class="row">
												<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
													<div class="col-lg-9">
													{/if}
													<textarea style="height: 125px;" id="validation_message_{$language.id_lang}" name="validation_message_{$language.id_lang}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}" maxlength="255">{$validation_message[$language.id_lang|intval]|escape:'html':'UTF-8'}</textarea>
													{if $languages|count > 1}
													</div>
													<div class="col-lg-2">
														<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
															{$language.iso_code|escape:'html':'UTF-8'}
															<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															{foreach from=$languages item=language}
																<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
																{/foreach}
														</ul>
													</div>
												</div>
											</div>
										{/if}
									{/foreach}
								</div>
							</div>
						</div>
					</div>
				{/if}
				<div class="panel element_tab" id="element_tab_attributes" style="display: none;">
					<div class="form-horizontal">
						<div class="form-group" id="classmodal">
							<label class="control-label col-lg-3">{l s='Class' mod='ukooformpro'}</label>
							<div class="col-lg-8">
								<input type="text" name="class" value='{$class|escape:'html':'UTF-8'}'>
							</div>
						</div>
						{if $type == 5}
							<div class="form-group modalform" id="size">
								<label class="control-label col-lg-3">{l s='Size' mod='ukooformpro'}</label>
								<div class="col-lg-8">
									<input type="text" name="size" value="{$size|escape:'html':'UTF-8'}">
								</div>
							</div>
						{/if}
						{if $type == 1}
							<div class="form-group modalform" id="max_size">
								<label class="control-label col-lg-3">{l s='Max size' mod='ukooformpro'}</label>
								<div class="col-lg-8">
									<input type="text" name="max_size" value="{$max_size|escape:'html':'UTF-8'}">
								</div>
							</div>
						{/if}
						{if $type == 2}
							<div class="form-group modalform" id="cols">
								<label class="control-label col-lg-3">{l s='Cols' mod='ukooformpro'}</label>
								<div class="col-lg-8">
									<input type="text" name="cols" value="{$cols|escape:'html':'UTF-8'}">
								</div>
							</div>
							<div class="form-group modalform" id="rows">
								<label class="control-label col-lg-3">{l s='Rows' mod='ukooformpro'}</label>
								<div class="col-lg-8">
									<input type="text" name="rows" value="{$rows|escape:'html':'UTF-8'}">
								</div>
							</div>
						{/if}
						{if $type == 5}
							<div class="form-group modalform" id="multiple">
								<label class="control-label col-lg-3">{l s='Multiple' mod='ukooformpro'}</label>
								<div class='col-lg-8'>
									<span class="switch prestashop-switch fixed-width-lg">
										<input type="radio" name="multiple" id="element_multiple_yes" value="1" {if $multiple == 1}checked="checked"{/if}/>
										<label class="t" for="element_multiple_yes">{l s='Yes' mod='ukooformpro'}</label>
										<input type="radio" name="multiple" id="element_multiple_no" value="0" {if $multiple != 1}checked="checked"{/if}/>
										<label class="t" for="element_multiple_no">{l s='No' mod='ukooformpro'}</label>
										<a class="slide-button btn"></a>
									</span>
								</div>
							</div>
						{/if}
						{if $type == 3 || $type == 4}
							<div class="form-group modalform" id="flow">
								<label class="control-label col-lg-3">{l s='Flow' mod='ukooformpro'}</label>
								<div class='col-lg-8'>
									<select class="form-control fixed-width-lg" id="element_flow" name="flow">
										<option value="1" {if $flow != 2}selected{/if}>{l s='Horizontal' mod='ukooformpro'}</option>
										<option value="2" {if $flow == 2}selected{/if}>{l s='Vertical' mod='ukooformpro'}</option>
									</select>
								</div>
							</div>
						{/if}
						{if $type == 6}
							<div class="form-group modalform" id="file_size">
								<label class="control-label col-lg-3">{l s='File size (KB)' mod='ukooformpro'}</label>
								<div class='col-lg-8'>
									<input type="text" name="file_size" value="{$file_size|escape:'html':'UTF-8'}"/>
								</div>
							</div>
							<div class="form-group modalform" id="extensions">
								<label class="col-lg-3 control-label">
									<span class="label-tooltip" data-toglle="tooltip" title="{l s='Separated by semicolons' mod='ukooformpro'}">
										{l s='Accepted extensions' mod='ukooformpro'}
									</span>
								</label>
								<div class="col-lg-8">
									<textarea name="extensions"  style="height: 125px;">{$extensions|escape:'html':'UTF-8'}</textarea>
								</div>
							</div>
							{*							<div class="form-group modalform" id="destination">
							<label class="col-lg-3 control-label">{l s='Destination' mod='ukooformpro'}</label>
							<div class="col-lg-8">
							<input type="text" name="destination" value="{$destionation|escape:'html':'UTF-8'}"/>
							</div>
							</div>*}
						{/if}
						{if $type == 10}
							<div class="form-group" id="background_color">
								<label class="control-label col-lg-3">{l s='Background color' mod='ukooformpro'}</label>
								<div class="form-group">
									<div class="col-lg-2">
										<div class="row">
											<div class="input-group">
												<input type="color"
													   data-hex="true"
													   class="color mColorPickerInput"
													   name="background_color"
													   {if $background_color == ''}
														   value="#ffffff"
													   {else}
														   value="#{$background_color|escape:'html':'UTF-8'}"
													   {/if}
													   id="color_0"/>
												<span class="mColorPickerTrigger input-group-addon" 
													  style="cursor:pointer;" 
													  id="icp_color_0"
													  data-mcolorpicker="true">
													<img src="../img/admin/color.png"
														 style="border:0;margin:0 0 0 3px"
														 align="absmiddle">
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" id="text_color">
								<label class="control-label col-lg-3">{l s='Text color' mod='ukooformpro'}</label>
								<div class="form-group">
									<div class="col-lg-2">
										<div class="row">
											<div class="input-group">
												<input type="color"
													   data-hex="true"
													   class="color mColorPickerInput"
													   name="text_color"
													   {if $text_color == ''}
														   value="#000000"
													   {else}
														   value="#{$text_color|escape:'html':'UTF-8'}"
													   {/if}
													   id="color_1"/>
												<span class="mColorPickerTrigger input-group-addon" 
													  style="cursor:pointer;" 
													  id="icp_color_1"
													  data-mcolorpicker="true">
													<img src="../img/admin/color.png"
														 style="border:0;margin:0 0 0 3px"
														 align="absmiddle">
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>						
						{/if}
						<div class="form-group" id="additional_attribute">
							<label class="control-label col-lg-3">{l s='Additional Attributes' mod='ukooformpro'}</label>
							<div class="col-lg-8">
								<textarea name="additional_attribute"  style="height: 125px;">{$additional_attribute}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button type="submit" name="submitAddukooformpro_elements" class="btn btn-default pull-right" id="submitModal">
						<i class="process-icon-save"></i> {l s='Save' mod='ukooformpro'}
					</button>
					<a href="#" onclick="$('#modules_elements').modal('hide')" class="btn btn-default" onclick="window.history.back();">
						<i class="process-icon-cancel"></i> {l s='Cancel' mod='ukooformpro'}
					</a>
				</div>
			</form>
		</div>
    </div>
</div>
