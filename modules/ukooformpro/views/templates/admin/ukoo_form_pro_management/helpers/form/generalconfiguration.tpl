{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-horizontal col-lg-10 config_row" id="config_row_generalinfortmation">
    <div class="form-group">
		<label class="control-label col-lg-3 required">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Write the name of your form' mod='ukooformpro'}">{l s='Name' mod='ukooformpro'}</span>
		</label>
		<div class="col-lg-8">
			<input type="text" name="name" id="name" required="required" value="{$currentTab->getFieldValue($currentObject, 'name')|escape:'html':'UTF-8'}">
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3 required"> 
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='If enabled, the form will be active on the font office' mod='ukooformpro'}">
				{l s='Active' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			<span class="switch prestashop-switch fixed-width-lg">
				<input type="radio" name="active" id="active_enabled" value="1" {if $currentTab->getFieldValue($currentObject, 'active') == 1}checked="checked"{/if}>
				<label class="t" for="active_enabled"> {l s='Enabled' mod='ukooformpro'}</label>
				<input type="radio" name="active" id="active_disabled" value="0" {if $currentTab->getFieldValue($currentObject, 'active') == 0}checked="checked"{/if}>
				<label class="t" for="active_disabled"> {l s='Disabled' mod='ukooformpro'}</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3 required"> 
			<span class='label-tooltip' data-toggle="tooltip" title="{l s='If enabled, the submission form will be save in the data base' mod='ukooformpro'}"/>
			{l s='Save datas' mod='ukooformpro'}
			</span>
		</label>				
		<div class="col-lg-8">
			<span class="switch prestashop-switch fixed-width-lg">
				<input type="radio" name="save_datas" id="save_datas_enabled" value="1" {if $currentTab->getFieldValue($currentObject, 'save_datas') == 1}checked="checked"{/if}>
				<label class="t" for="save_datas_enabled"> {l s='Enabled' mod='ukooformpro'}</label>
				<input type="radio" name="save_datas" id="save_datas_disabled" value="0" {if $currentTab->getFieldValue($currentObject, 'save_datas') != 1}checked="checked"{/if}>
				<label class="t" for="save_datas_disabled"> {l s='Disabled' mod='ukooformpro'}</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
    </div>
    {if $update == true}
		<div class="form-group">
			<label class="control-label col-lg-3 required">
				<span>
					{l s='Use custom tpl' mod='ukooformpro'}
				</span>	
			</label>
			<div class="col-lg-8">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="use_custom_tpl" id="use_custom_tpl_enabled" value="1" {if $currentTab->getFieldValue($currentObject, 'use_custom_tpl') == 1}checked="checked"{/if}>
					<label class="t" for="use_custom_tpl_enabled"> {l s='Enable' mod='ukooformpro'}</label>
					<input type="radio" name="use_custom_tpl" id="use_custom_tpl_disabled" value="0" {if $currentTab->getFieldValue($currentObject, 'use_custom_tpl') != 1}checked="checked"{/if}>
					<label class="t" for="use_custom_tpl_disabled"> {l s='Disabled' mod='ukooformpro'}</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">
				<span>
					{l s='TPL address : ' mod='ukooformpro'}
				</span>
			</label>
			<div class="col-lg-6">
				<input type="text" readonly value="/modules/ukooformpro/views/templates/front/form_tpl/form_id{$currentObject->id_ukooformpro_form|escape:'html':'UTF-8'}.tpl"/>
			</div>
		</div>
    {/if}
    <div class="form-group">
		<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='This is the title off the form to be show' mod='ukooformpro'}">
				{l s='Title' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="title_{$language.id_lang|intval}" type="text"  name="title_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'title', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Meta title' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="meta_title_{$language.id_lang|intval}" type="text"  name="meta_title_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'meta_title', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Meta description' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="meta_description_{$language.id_lang|intval}" type="text"  name="meta_description_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'meta_description', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The sign you want to show when is a required field' mod='ukooformpro'}">
				{l s='Required sign' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="required_sign_{$language.id_lang|intval}" type="text"  name="required_sign_{$language.id_lang|intval}" 
								   {if $currentTab->getFieldValue($currentObject, 'required_sign', $language.id_lang|intval) != ''}
									   value="{$currentTab->getFieldValue($currentObject, 'required_sign', $language.id_lang|intval)|escape:'html':'UTF-8'}"
								   {else}
									   value="*"
								   {/if}
								   >
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Attibute class' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="attr_class_{$language.id_lang|intval}" type="text"  name="attr_class_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'attr_class', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Attribute id' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="attr_id_{$language.id_lang|intval}" type="text"  name="attr_id_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'attr_id', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Attribute name' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="attr_name_{$language.id_lang|intval}" type="text"  name="attr_name_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'attr_name', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Attribute action' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="attr_action_{$language.id_lang|intval}" type="text"  name="attr_action_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'attr_action', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Other attributes' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<input id="other_attr_{$language.id_lang|intval}" type="text"  name="other_attr_{$language.id_lang|intval}" value="{$currentTab->getFieldValue($currentObject, 'other_attr', $language.id_lang|intval)|escape:'html':'UTF-8'}">
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3">{l s='Error message' mod='ukooformpro'}</label>
        <div class="col-lg-8">
            {foreach from=$languages item=language}
                {if $languages|count > 1}
                    <div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<textarea style="height: 250px;" id="error_message_{$language.id_lang|escape:'html':'UTF-8'}" name="error_message_{$language.id_lang|escape:'html':'UTF-8'}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}">{$currentTab->getFieldValue($currentObject, 'error_message', $language.id_lang|intval)|escape:'html':'UTF-8'}</textarea>
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
                    </div>
                {/if}
            {/foreach}
        </div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3 required"> 
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Show thank you message' mod='ukooformpro'}">
				{l s='Show thank you message' mod='ukooformpro'}
			</span>
		</label>
		<div class="col-lg-8">
			<span class="switch prestashop-switch fixed-width-lg">
				<input type="radio" name="show_ty_message" id="show_ty_message_enabled" value="1" {if $currentTab->getFieldValue($currentObject, 'show_ty_message') == 1}checked="checked"{/if}>
				<label class="t" for="show_ty_message_enabled"> {l s='Enabled' mod='ukooformpro'}</label>
				<input type="radio" name="show_ty_message" id="show_ty_message_disabled" value="0" {if $currentTab->getFieldValue($currentObject, 'show_ty_message') != 1}checked="checked"{/if}>
				<label class="t" for="show_ty_message_disabled"> {l s='Disabled' mod='ukooformpro'}</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
    </div>
    <div class="form-group">
		<label class="control-label col-lg-3">{l s='Thank You message' mod='ukooformpro'}</label>
		<div class="col-lg-8">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="row">
						<div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $id_lang_default}style="display:none"{/if}>
							<div class="col-lg-9">
							{/if}
							<textarea style="height: 250px;" id="ty_message_{$language.id_lang|escape:'html':'UTF-8'}" name="ty_message_{$language.id_lang|escape:'html':'UTF-8'}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}">{$currentTab->getFieldValue($currentObject, 'ty_message', $language.id_lang|intval)|escape:'html':'UTF-8'}</textarea>
							{if $languages|count > 1}
							</div>
							<div class="col-lg-2">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									{$language.iso_code|escape:'html':'UTF-8'}
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									{foreach from=$languages item=language}
										<li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});" tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a></li>
										{/foreach}
								</ul>
							</div>
						</div>
					</div>
				{/if}
			{/foreach}
		</div>
    </div>
</div>