{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-group ukooformpro_textbox {if $element.required==1}required{/if} {$element.class|escape:'html':'UTF-8'} col-md-6">
    {include file='./label.tpl'}
    <input type="text" class="form-control{if $element.required==1} is_required{/if}"
		   name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		   id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		   {$element.additional_attribute}
		   {if $element.attribute.max_size != ''}maxlength="{$element.attribute.max_size|escape:'html':'UTF-8'}"{/if}
		   value="{$element.default_value|escape:'html':'UTF-8'}"
		   />
    {include file='./error.tpl'}
</div>			