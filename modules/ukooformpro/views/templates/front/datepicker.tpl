{**
* UkooFormPro
*
* @author   Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<script type="text/javascript">
    {literal}$(function () {
			$('#elt{/literal}{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}{literal}').datepicker();
		});{/literal}
    </script>
    <div class="form-group ukooformpro_date {$element.class|escape:'html':'UTF-8'}">
		{include file='./label.tpl'}
		<br />
		<input type="text"
			   name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			   id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			   {$element.additional_attribute}
			   value="{$element.default_value|escape:'html':'UTF-8'}"
			   />
    </div> 
    {include file='./error.tpl'}