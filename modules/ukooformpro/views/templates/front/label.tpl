{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<label {if $element.type|intval != 3 && $element.type|intval != 4 && $element.type|intval != 5}for="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"{/if}
																							   {if $element.hint !=''}title="{$element.hint|escape:'html':'UTF-8'}"{/if}>
    {$element.label|escape:'html':'UTF-8'}
    {if $element.required==1}
		<sup>{if $form.required_sign != ''}{$form.required_sign|escape:'html':'UTF-8'}{else}*{/if}</sup>
    {/if} 
</label>