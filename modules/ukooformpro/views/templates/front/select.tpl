{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-group selector1 ukooformpro_select {if $element.required==1}required{/if} {$element.class|escape:'html':'UTF-8'}">
    {include file='./label.tpl'}
    <select	
		class="form-control"
		name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}[]"
		{if $element.attribute.multiple == 1}multiple{/if}
		{if $element.attribute.size != ''}size="{$element.attribute.size|escape:'html':'UTF-8'}"{/if}
		{$element.additional_attribute}>
		{foreach from=$element.default_value item=value key=label name='myloop'}
			<option
				id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}-{$smarty.foreach.myloop.iteration|escape:'html':'UTF-8'}" 
				value="{$value|escape:'html':'UTF-8'}"
				{if isset($element.value)}
					{if in_array($value, $element.value)}selected{/if}
				{/if}
				>
				{$label|escape:'html':'UTF-8'}
			</option>
		{/foreach}
    </select>
    {include file='./error.tpl'}
</div>