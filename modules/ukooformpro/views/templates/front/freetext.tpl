{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div 
    {$element.additional_attribute}
    class="form-group ukooformpro_freetext {$element.class|escape:'html':'UTF-8'} col-md-12 clear" 
    id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
    {$element.additional_attribute}>
    {$element.default_value}
</div>