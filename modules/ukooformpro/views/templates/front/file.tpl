{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<p class="form-group ukooformpro_fileupload {$element.class|escape:'html':'UTF-8'}">
    {include file='./label.tpl'}
    <input name="fil{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}" type="hidden"/>
    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
    <input class="form-control" type ="file"
           {if $element.attribute.multiple == 1}multiple{/if}
           name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
           id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
    {if isset($element.additional_attribute) && !empty($element.additional_attribute)}{$element.additional_attribute}{/if}
    />
{include file='./error.tpl'}
</p>