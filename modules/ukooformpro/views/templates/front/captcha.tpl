{**
* UkooFormPro
*
* @author   Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

{literal}
    <script type="text/javascript">
		function refreshCaptcha() {
			var background = document.getElementById('background').value;
			var text = document.getElementById('text').value;
			$("#captchaimg").attr("src", $("#captchaimg").attr("src").substring(0, $("#captchaimg").attr("src").lastIndexOf("?")) + "?background=" + background + "&text=" + text + "&rand=" + Math.random(999));
		}
    </script>
{/literal}

{$message = $element.attribute.refresh_text[$element.id_lang]}

<div class="form-group ukooformpro_captcha {$element.class|escape:'html':'UTF-8'} {$element.class|escape:'html':'UTF-8'}">
    <div>
		<label>{if $element.label != ''}{$element.label|escape:'html':'UTF-8'}{else}{l s='Enter the code above here :' mod='ukooformpro'}{/if}</label><br />
		<input type="hidden" value="{$element.attribute.background_color|escape:'html':'UTF-8'}" id="background">
		<input type="hidden" value="{$element.attribute.text_color|escape:'html':'UTF-8'}" id="text">
        <img id="captchaimg" alt="{if $element.label != ''}{$element.label|escape:'html':'UTF-8'}{else}{l s='Enter the code above here :' mod='ukooformpro'}{/if}" src="{$modules_dir}ukooformpro/captcha.php?background={$element.attribute.background_color|escape:'html':'UTF-8'}&amp;text={$element.attribute.text_color|escape:'html':'UTF-8'}">
        <br />
        <a href='javascript: refreshCaptcha();'>{if $message!=''}{$message|escape:'html':'UTF-8'}{else}{l s='Can\'t read the image? click here' mod='ukooformpro'}{/if}</a>
        <br />
        <input type="text"
			   {$element.additional_attribute}
			   id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}" 
			   name = "elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			   autocomplete="off"
			   />
    </div>
    <p
		class="ukooformpro_error"
		id="ukooformpro_error{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		{if !isset($error[$element.id_ukooformpro_elements])}style="display: none"{/if}
		>
		{if $element.validation_message != ''}
			{$element.validation_message|escape:'html':'UTF-8'}
		{else}
			{l s='Please enter the correct image characters' mod='ukooformpro'}
		{/if}
    </p>
</div>