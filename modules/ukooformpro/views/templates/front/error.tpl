{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<p
    class="ukooformpro_error"
    id="ukooformpro_error{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
    {if !isset($error[$element.id_ukooformpro_elements])}style="display: none"{/if}
    >
    {if $element.validation_message != ''}
		{$element.validation_message|escape:'html':'UTF-8'}
    {else if $error[$element.id_ukooformpro_elements] == 0}
		{l s='This field is required' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 1}
		{l s='This field require a validate Email' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 2}
		{l s='This field require a validate Phone number' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 3}
		{l s='This field require a validate Name' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 4}
		{l s='This field require a validate Order invoice number' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 5}
		{l s='This field require a validate Float number' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 6}
		{l s='This field require a validate Integer number' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 7}
		{l s='This field require a validate Url' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 8}
		{l s='This field require a validate EAN13 code' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 9}
		{l s='This field require a validate UPC code' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 10}
		{l s='This field require a validate SIRET number' mod='ukooformpro'}
    {else if $error[$element.id_ukooformpro_elements] == 11}
		{l s='This email address does not exist, or seems to be wrong.' mod='ukooformpro'}
    {/if}
</p>