{**
* UkooFormPro
*
* @author   Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<input 
    class="ukooformpro_hiddenfield {$element.class|escape:'html':'UTF-8'}"
    type="hidden"
    name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
    value="{$element.default_value|escape:'html':'UTF-8'}"
    />