{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

{if $element.attribute.button_type == 1}
    <button 
		{$element.additional_attribute}
		type="submit"
		name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		value="{$element.default_value|escape:'html':'UTF-8'}"
		class="button btn btn-default button-medium {$element.class|escape:'html':'UTF-8'}"
		><span>{$element.label|escape:'html':'UTF-8'}<i class="icon-chevron-right right"></i></span></button>
			{else if $element.attribute.button_type == 2}
				{$button_type = 'reset'}
    <button 
		{$element.additional_attribute}
		type="reset"
		name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		value="{$element.default_value|escape:'html':'UTF-8'}"
		class="button btn btn-default button-medium exclusive {$element.class|escape:'html':'UTF-8'}"
		><span>{$element.label|escape:'html':'UTF-8'}<i class="icon-refresh right"></i></span></button>
			{else}
				{$button_type = 'button'}
    <button 
		{$element.additional_attribute}
		type="button"
		name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
		value="{$element.default_value|escape:'html':'UTF-8'}"
		class="{$element.class|escape:'html':'UTF-8'}"
		><span>{$element.label|escape:'html':'UTF-8'}</span></button>
{/if}