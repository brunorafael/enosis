{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-group ukooformpro_checkbox {if $element.required==1}required{/if} {$element.class|escape:'html':'UTF-8'}" >
    {include file='./label.tpl'}
    <ul>
		{foreach from=$element.default_value item=value key=label name='myloop'}
			<li {if $element.attribute.flow == 1}style="display: inline-block;"{/if}>
				<input
					{$element.additional_attribute}
					type="checkbox" 
					id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}-{$smarty.foreach.myloop.iteration|escape:'html':'UTF-8'}" 
					value="{$value|escape:'html':'UTF-8'}"
					name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}[]"
					{if isset($element.value)}
						{if in_array($value, $element.value)}checked{/if}
					{/if}
					/>
				<label for="elt{$element.id_ukooformpro_elements}-{$smarty.foreach.myloop.iteration}">{$label|escape:'html':'UTF-8'}</label>
			</li>
		{/foreach}
    </ul>
    {include file='./error.tpl'}
</div>