{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

{capture name=path}
    {if $form.title == ''}
		{l s='Form' mod='ukooformpro'}
    {else}
		{$form.title|escape:'html':'UTF-8'}
    {/if}
{/capture}
<div class="box">
    {if $form.title != ''}
		<h1 class="page-subheading">{$form.title|escape:'html':'UTF-8'}</h1>
    {/if}

    {*<p class="required">
    <sup>{if $form.required_sign != ''}{$form.required_sign|escape:'html':'UTF-8'}{else}*{/if}</sup>
    Required field
    </p>*}
    {if isset($error_mail)}
		<p class="error-msg error">
			{l s='An error has occurred during the form\'s send.' mod='ukooformpro'}<br/>
			{l s='Please try again later' mod='ukooformpro'}
		</p>
    {else if isset($ty_message)}
		<p class="box_icon-infos">
			{if $form.show_ty_message == 1 && $form.ty_message != ''}
				{$form.ty_message}
			{else}
				{l s='Thank you. Your form has been submitted' mod='ukooformpro'}
			{/if}
		</p>
    {else}
		<form action="{if isset($form.attr_action) && !empty($form.attr_action)}{$form.attr_action}{else}{$request_uri}{/if}"
              {if isset($form.attr_name) && !empty($form.attr_name)} name="{$form.attr_name}"{/if}
              method="post"
              id="{if isset($form.attr_id) && !empty($form.attr_id)}{$form.attr_id}{else}ukooformpro{/if}"
              class="std lang{$form.id_lang|escape:'html':'UTF-8'} contact-form-box ukooformpro{$form.id_ukooformpro_form|escape:'html':'UTF-8'} {if isset($form.attr_class) && !empty($form.attr_class)}{$form.attr_class}{/if}"
              enctype="multipart/form-data"
              {if isset($form.other_attr) && !empty($form.other_attr)} {$form.other_attr}{/if} >
			<input type = "hidden" name="id_ukooformpro_form" value = "{$form.id_ukooformpro_form|escape:'html':'UTF-8'}">
			<fieldset>	
				{foreach from = $elements item =element}
					{if $element.type == 1}
						{include file='./textbox.tpl'}
					{else if $element.type == 2}
						{include file='./textarea.tpl'}
					{else if $element.type == 3}
						{include file='./checkbox.tpl'}
					{else if $element.type == 4}
						{include file='./radio.tpl'}
					{else if $element.type == 5}
						{include file='./select.tpl'}
					{else if $element.type == 6}
						{include file='./file.tpl'}
					{else if $element.type == 7}
						{include file='./datepicker.tpl'}
					{else if $element.type == 8}
						{include file='./freetext.tpl'}
					{else if $element.type == 9}
						{include file='./hiddenfield.tpl'}
					{else if $element.type == 10}
						{include file='./captcha.tpl'}
					{else if $element.type == 11}
						{include file='./button.tpl'}
					{/if}			
				{/foreach}
			</fieldset>
		</form>
    {/if}
</div>
{strip}
    <script type="text/javascript">
		$.uniform.defaults.fileDefaultHtml = '{l s='No file selected' mod='ukooformpro'}';
		$.uniform.defaults.fileButtonHtml = '{l s='Choose File' mod='ukooformpro'}';
    </script>
{/strip}