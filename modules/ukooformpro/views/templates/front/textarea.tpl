{**
* UkooFormPro
*
* @author    Jérôme PERAT - Ukoo <modules@ukoo.fr>
* @copyright Ukoo 2014
* @license   Ukoo - Tous droits réservés
*}

<div class="form-group ukooformpro_textarea {if $element.required==1}required{/if} {$element.class|escape:'html':'UTF-8'} col-md-12 clear">
    {include file='./label.tpl'}
    <textarea class="form-control "
			  name="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			  id="elt{$element.id_ukooformpro_elements|escape:'html':'UTF-8'}"
			  {$element.additional_attribute}
			  {if $element.attribute.cols != ''}cols="{$element.attribute.cols|escape:'html':'UTF-8'}"{/if}
			  {if $element.attribute.rows != ''}rows="{$element.attribute.rows|escape:'html':'UTF-8'}"{/if}
			  >{$element.default_value|escape:'html':'UTF-8'}</textarea>
    {include file='./error.tpl'}
</div>