<?php
/**
* default.php.
*
* @author    Bruno Rafael
* @copyright 2016 Bruno Rafael
* @license   license.txt
*
* 2016 Bruno Rafael - All rights reserved.
*
* DISCLAIMER
* Changing this file will render any support provided by us null and void.
*/

class DevismodFormModuleFrontController extends ModuleFrontController
{

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        
        
    }

    public function postProcess()
    {
        
        $return = array();

        if (isset($_FILES['devismod_file']) && $_FILES['devismod_file']['size'] > 0) {
            $filename = $_FILES['devismod_file']['name'];
            $maxsize = (int)Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE');
            if ($_FILES['devismod_file']['size'] > $maxsize * 1024 * 1024) {
                $return['errors'][] = $this->module->l('File is too large');
                
            }

            $pathinfo = pathinfo($filename);
            $msg_error = 'File has an invalid extension, it should be one of %s';
            $ext = (isset($pathinfo['extension'])) ? $pathinfo['extension'] : null;
            $allowedExtensions = (array)explode(', ', Configuration::get('PRODUCTCONTACT_FILE_EXT'));
            if (!in_array(Tools::strtolower($ext), $allowedExtensions)) {
                $return['errors'][] = sprintf($this->module->l($msg_error), implode(', ', $allowedExtensions));
                
            }

            if (!isset($return['errors'])) {
                do {
                    $uniqid = sha1(microtime());
                
                } while (file_exists(_PS_DOWNLOAD_DIR_.$this->module->name.'/'.$uniqid.'.'.$ext));

                $file_copy = _PS_DOWNLOAD_DIR_.$this->module->name.'/'.$uniqid.'.'.$ext;
                if (!copy($_FILES['devismod_file']['tmp_name'], $file_copy)) {
                    $return['errors'][] = $this->module->l('Failed to copy the file.');
                    
                } else {
                    @unlink($_FILES['devismod_file']['tmp_name']);
                    
                }
            }

            $return['success'] = (isset($return['errors']) || !isset($uniqid)) ? false : $uniqid.'.'.$ext;
        }

        if (Tools::isSubmit('devismod_product')) {
            $devismod_type = ((string)Tools::getValue('devismod_type'));
            
            $not_type = !Validate::isGenericName($devismod_type);
            if (!$devismod_type  || empty($devismod_type) || $not_type) {
                $return['errors'][] = $this->module->l('Invalid type');
                
            }

            $devismod_company = ((string)Tools::getValue('devismod_company'));
            if (!Validate::isGenericName($devismod_company)) {
                $return['errors'][] = $this->module->l('Invalid company');
                
            }

            $devismod_activity = ((string)Tools::getValue('devismod_activity'));
            if (!Validate::isGenericName($devismod_activity)) {
                $return['errors'][] = $this->module->l('Invalid activity');
                
            }
                
            $devismod_dni = ((string)Tools::getValue('devismod_dni'));
            if (!Validate::isDniLite($devismod_dni)) {
                $return['errors'][] = $this->module->l('Invalid dni');
                
            }

            $devismod_firstname = ((string)Tools::getValue('devismod_firstname'));
            $not_firstname = !Validate::isName($devismod_firstname);
            if (!$devismod_firstname  || empty($devismod_firstname) || $not_firstname) {
                $return['errors'][] = $this->module->l('Invalid firstname');
                
            }

            $devismod_lastname = ((string)Tools::getValue('devismod_lastname'));
            $not_lastname = !Validate::isName($devismod_lastname);
            if (!$devismod_lastname  || empty($devismod_lastname) || $not_lastname) {
                $return['errors'][] = $this->module->l('Invalid lastname');
                
            }

            $devismod_address = ((string)Tools::getValue('devismod_address'));
            if (!Validate::isAddress($devismod_address)) {
                $return['errors'][] = $this->module->l('Invalid address');
                
            }

            $devismod_postcode = ((string)Tools::getValue('devismod_postcode'));
            $not_postcode = !Validate::isPostCode($devismod_postcode);
            if (!$devismod_postcode  || empty($devismod_postcode) || $not_postcode) {
                $return['errors'][] = $this->module->l('Invalid post code');
                
            }

            $devismod_city = ((string)Tools::getValue('devismod_city'));
            $not_cityname = !Validate::isCityName($devismod_city);
            if (!$devismod_city  || empty($devismod_city) || $not_cityname) {
                $return['errors'][] = $this->module->l('Invalid city');
                
            }

            $devismod_country = ((string)Tools::getValue('devismod_country'));
            $not_country = !Validate::isGenericName($devismod_country);
            if (!$devismod_country  || empty($devismod_country) || $not_country) {
                $return['errors'][] = $this->module->l('Invalid country');
                
            }

            $devismod_phone = ((string)Tools::getValue('devismod_phone'));
            if (!Validate::isPhoneNumber($devismod_phone)) {
                $return['errors'][] = $this->module->l('Invalid phone');
                
            }

            $devismod_fax = ((string)Tools::getValue('devismod_fax'));
            if (!Validate::isPhoneNumber($devismod_fax)) {
                $return['errors'][] = $this->module->l('Invalid fax');
                
            }

            $devismod_email = ((string)Tools::getValue('devismod_email'));
            $not_email = !Validate::isEmail($devismod_email);
            if (!$devismod_email  || empty($devismod_email) || $not_email) {
                $return['errors'][] = $this->module->l('Invalid email');
                
            }

            $devismod_comment = ((string)Tools::getValue('devismod_comment'));
            $not_comment = !Validate::isMessage($devismod_comment);
            if (!$devismod_comment  || empty($devismod_comment) || $not_comment) {
                $return['errors'][] = $this->module->l('Invalid comment');
                
            }

            $devismod_filename = ((string)Tools::getValue('devismod_filename'));
            if ($devismod_filename) {
                if (!Validate::isUnixName($devismod_filename)) {
                    $return['errors'][] = $this->module->l('Invalid filename');
                    
                }
                
            }

            $devismod_product = ((string)Tools::getValue('devismod_product'));
            $not_product = !Validate::isUnsignedId($devismod_product);
            if (!$devismod_product  || empty($devismod_product) || $not_product) {
                $return['errors'][] = $this->module->l('Invalid product id');
                
            }

            $id_lang = (int)$this->context->language->id;
            $iso = Language::getIsoById($id_lang);
            $product = new Product((int)$devismod_product, false, $id_lang);
            if (!Validate::isLoadedObject($product)) {
                $return['errors'][] = $this->module->l('Invalid product id');
                
            }

            if (!isset($return['errors'])) {
                $link = new Link();

                $attachment = null;
                if ($devismod_filename) {
                    $filepath = _PS_DOWNLOAD_DIR_.$this->module->name.'/'.$devismod_filename;
                    if (file_exists($filepath)) {
                        $attachment['content'] = Tools::file_get_contents($filepath);
                        $attachment['name'] = $devismod_filename;
                        $attachment['mime'] = self::getMime($devismod_filename);
                    }
                }
                
                if (Configuration::get('PS_MAIL_TYPE') == Mail::TYPE_TEXT) {
                    $product_address = $devismod_address;

                } else {
                    $product_address = Tools::nl2br($devismod_address);

                }
                $templateVars = array(
                    '{product}' => (is_array($product->name) ? $product->name[$id_lang] : $product->name),
                    '{product_link}' => $link->getProductLink($product),
                    '{devismod_type}' => $devismod_type,
                    '{devismod_company}' => $devismod_company,
                    '{devismod_activity}' => $devismod_activity,
                    '{devismod_dni}' => $devismod_dni,
                    '{lastname}' => $devismod_lastname,
                    '{firstname}' => $devismod_firstname,
                    '{devismod_address}' => $product_address,
                    '{devismod_postcode}' => $devismod_postcode,
                    '{devismod_city}' => $devismod_city,
                    '{devismod_country}' => $devismod_country,
                    '{devismod_phone}' => $devismod_phone,
                    '{devismod_fax}' => $devismod_fax,
                    '{email}' => $devismod_email,
                    '{message}' => (Configuration::get('PS_MAIL_TYPE') == Mail::TYPE_TEXT) ? $devismod_comment : Tools::nl2br($devismod_comment)
                );

                if (file_exists(_PS_MODULE_DIR_.$this->module->name.'/mails/'.$iso.'/devismod.txt') && file_exists(_PS_MODULE_DIR_.$this->module->name.'/mails/'.$iso.'/devismod.html')) {
                    if (!Mail::Send((int)Configuration::get('PS_LANG_DEFAULT'), 'devismod', Mail::l('Product contact form', $id_lang), $templateVars, ((string)Configuration::get('PRODUCTCONTACT_EMAIL')), ((string)Configuration::get('PS_SHOP_NAME')), ((string)$devismod_email), ((string)$devismod_firstname).((string)$devismod_lastname), $attachment, null, _PS_MODULE_DIR_.$this->module->name.'/mails/')) {
                        $return['errors'][] = $this->module->l('Failed to send email');
                        
                    }
                    
                }
            }

            $return['success'] = (isset($return['errors'])) ? false : $this->module->l('Thanks for report.');
        }
        die(Tools::jsonEncode($return));
    }
    
    private static function getMime($filename)
    {
        $pathinfo = pathinfo($filename);
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        if (function_exists('mime_content_type')) {
            $mimetype = mime_content_type($filename);
            return $mimetype;
            
        } elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
            
        } elseif (array_key_exists($pathinfo['extension'], $mime_types)) {
            return $mime_types[$pathinfo['extension']];
        
        } else {
            return 'application/octet-stream';
        }
    }
}
