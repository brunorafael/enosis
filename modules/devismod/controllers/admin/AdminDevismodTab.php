<?php
/**
* AdminDevismodTabController.php.
*
* @author    Bruno Rafael
* @copyright 2016 Bruno Rafael
* @license   license.txt
*
* 2016 Bruno Rafael - All rights reserved.
*
* DISCLAIMER
* Changing this file will render any support provided by us null and void.
*/

class AdminDevismodTabController extends ModuleAdminController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->module->name);
    }

    /**
     * Add a new tab of dashboard "look $trads"
     */
    public static function addDevismodTab()
	{
		/* Before creating a new tab "AdminDevismodTab" we need to remove any existing "AdminDevismodTab" tab */
		if ($id_tab = Tab::getIdFromClassName('AdminDevismodTab')) {
			$tab = new Tab((int)$id_tab);
            $tab->delete();           
        }

        $trads = array(
			'fr' => 'Enosis devis',
			'en' => 'Enosis quotations'
		);
		
		/* If the "AdminDevismodTab" tab does not exist yet, create it */
        if (!$id_tab = Tab::getIdFromClassName('AdminDevismodTab')) {
            $tab = new Tab();
            $tab->class_name = 'AdminDevismodTab';
            $tab->module = 'devismod';
            $tab->active = 1;
            // $tab->id_parent = (int)Tab::getIdFromClassName('AdminTools');
            foreach (Language::getLanguages(false) as $lang) {
                $tab->name[(int)$lang['id_lang']] = (isset($trads[$lang['iso_code']]) ? $trads[$lang['iso_code']] : $trads[$lang['en']]);
           	}  
            return $tab->save();
        }
        return false;	

	}

    /**
     * Delete the tab of dashboard Enosis devis/Enosis quotations
     */
	public static function delDevismodTab()
	{
		$remove_id = Tab::getIdFromClassName('AdminDevismodTab');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}
        
}