<?php
/**
* devismod.php.
*
* @author    Bruno Rafael
* @copyright 2016 Bruno Rafael
* @license   license.txt
*
* 2016 Bruno Rafael - All rights reserved.
*
* DISCLAIMER
* Changing this file will render any support provided by us null and void.
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once dirname(__FILE__).'/controllers/admin/AdminDevismodTab.php';


class Devismod extends PaymentModule
{

    protected $available_tabs_lang = array();
    protected $available_tabs = array();
    protected $tab_display = 'devismod';

    

    public function __construct()
    {
        $this->name = 'devismod';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'brunorafaelap@gmail.com';
        $this->need_instance = 0;

        // Modules needed for install
        $this->dependencies = array();
        // e.g. $this->dependencies = array('blockcart', 'blockcms');

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Enosis devis');
        $this->description = $this->l('This module allows you to accept quotes on your Prestashop store.');

        $this->confirmUninstall = $this->l('Warning! Uninstalling this module, you will delete all associated attachments.');

        $this->limited_countries = array('FR');
        $this->limited_currencies = array('EUR');

        /* Backward compatibility */
        if (_PS_VERSION_ < '1.5') {
            require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
        }

    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false) {
            $this->_errors[] = $this->l('This module is not available in your country');
            return false;
        }

        include(dirname(__FILE__).'/sql/install.php');
        
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        return parent::install() &&
            mkdir(_PS_DOWNLOAD_DIR_.$this->name, 0755) &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('payment') &&
            $this->registerHook('displayFooterProduct') &&
            $this->registerHook('displayProductButtons') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('actionPaymentConfirmation') &&
            $this->registerHook('displayAdminOrderContentOrder') &&
            $this->registerHook('displayPaymentReturn') &&
            AdminDevismodTabController::addDevismodTab() &&
            Configuration::updateValue('DEVISMOD_MODE', false) &&
            Configuration::updateValue('DEVISMOD_EMAIL', Configuration::get('PS_SHOP_EMAIL')) &&
            Configuration::updateValue('DEVISMOD_TERMS', array((int)Configuration::get('PS_LANG_DEFAULT') => "")) &&
            Configuration::updateValue('DEVISMOD_FILE_EXT', implode(', ', array(
                'txt', 'rtf', 'doc', 'docx', 'pdf', 'xls', 'png', 'jpeg', 'gif', 'jpg'
            )));
    }

    public function uninstall()
    {

        if (file_exists(_PS_DOWNLOAD_DIR_.$this->name)) {
            $folder = new DirectoryIterator(_PS_DOWNLOAD_DIR_.$this->name);
            foreach ($folder as $file) :
                if ($file->isFile() && !$file->isDot()) {
                    unlink($file->getPathname());
                }
            endforeach;
                    
            rmdir(_PS_DOWNLOAD_DIR_.$this->name);
        }

        
        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall() &&
        AdminDevismodTabController::delDevismodTab() &&
        Configuration::deleteByName('DEVISMOD_MODE') &&
        Configuration::deleteByName('DEVISMOD_TERMS') &&
        Configuration::deleteByName('DEVISMOD_EMAIL') &&
        configuration::deleteByName('DEVISMOD_FILE_EXT');
    }

  
    /*public function clearCache()
    {
        parent::_clearCache('backOfficeHeader.tpl', 'template_1');
        
    }*/

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = null;


        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submit'.$this->name)) == true) {
            
            if (!Tools::getValue('DEVISMOD_EMAIL')  || empty(Tools::getValue('DEVISMOD_EMAIL')) || !Validate::isEmail(Tools::getValue('DEVISMOD_EMAIL'))) {
                $output .= $this->displayError($this->l('Invalid email'));

            }


            $DEVISMOD_TERMS = array();
            $languages = Language::getLanguages();
            foreach ($languages as $language) {
                $lang = (int)$language['id_lang'];
                $DEVISMOD_TERMS[$lang] = Tools::getValue('DEVISMOD_TERMS_'.$lang);
                if (!Validate::isCleanHtml($DEVISMOD_TERMS[$lang])) {
                    $output .= $this->displayError(sprintf($this->l('Invalid terms for %s'), $language['name']));
                    unset($DEVISMOD_TERMS[$lang]);
                }
            }

            if (!Tools::getValue('DEVISMOD_FILE_EXT') || empty(Tools::getValue('DEVISMOD_FILE_EXT')) || !Validate::isCleanHtml(Tools::getValue('DEVISMOD_FILE_EXT'))) {
                $output .= $this->displayError($this->l('Invalid list of allowed extensions'));

            }


            if (!$output) {
                Configuration::updateValue('DEVISMOD_TERMS', $DEVISMOD_TERMS, true);
                $output.$this->postProcess();
                $output .= $this->displayConfirmation($this->l('Settings updated'));

            }
            

        }

        $this->available_tabs_lang = array(
            'Documents' => $this->l('Documents'),
            'Settings' => $this->l('Settings'),
            'Form' => $this->l('Form'),
            'ListOfQuotations' => $this->l('List of quotations'),           
            
        );

        $this->available_tabs = array(
            'Documents' => 0, 
            'Settings' => 1,
            'Form' => 2,
            'ListOfQuotations' => 3,
            );


        /**
        * list tabs.
        */
        $module_tabs = array();

        foreach ($this->available_tabs as $module_tab => $value) {
           $module_tabs[$module_tab] = array(
            'id' => $module_tab,
            'name' => $this->available_tabs_lang[$module_tab],
            'href' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&action='.$module_tab,
            
            );
        }      


        $this->context->smarty->assign('module_dir', $this->_path);

        
        $this->context->smarty->assign(array(
            'form_config' => $this->renderForm(),
            'module_tabs' => $module_tabs,
            ));

           return $output.$this->display(__FILE__, 'views/templates/admin/enosis.tpl');
        
        
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
            
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit'.$this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name;
         $helper->token = Tools::getAdminTokenLite('AdminModules');
       

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        

        return $helper->generateForm(array($this->getConfigForm()));
    }
    

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'DEVISMOD_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'hint' => $this->l('Entrez un adresse e-mail valide, par exemple: my@email.com'),
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'label' => $this->l('Email'),
                        'desc' => $this->l('Enter a valid email address for receive customer request.'),
                        'name' => 'DEVISMOD_EMAIL',
                        'required' => true
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Terms'),
                        'desc' => $this->l('Terms display on the top of the form.'),
                        'autoload_rte' => true,
                        'lang' => true,
                        'col' => 6,
                        'name' => 'DEVISMOD_TERMS',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Extensions for attachment'),
                        'desc' => $this->l('List of allowed extensions use to validate attachment.'),
                        'name' => 'DEVISMOD_FILE_EXT',
                        'size' => 60,
                        'col' => 6,
                        'required' => true
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        
        return array(
            'DEVISMOD_MODE' => Configuration::get('DEVISMOD_MODE'),
            'DEVISMOD_EMAIL' => Configuration::get('DEVISMOD_EMAIL'),
            'DEVISMOD_TERMS' => (array)Configuration::getInt('DEVISMOD_TERMS', true),
            'DEVISMOD_FILE_EXT' => Configuration::get('DEVISMOD_FILE_EXT'),
            
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));

        }
       
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookDisplayBackOfficeHeader()
    {
        
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
       
        

    }

    public function hookDisplayHeader($params)
    {
        if (Configuration::get('DEVISMOD_MODE') == true) {
            $this->context->controller->addCSS($this->_path.'views/css/front.css', 'all');
            $this->context->controller->addjQueryPlugin('ajaxfileupload');
            if (preg_match('/pack/', $_SERVER['REQUEST_URI'])) {
                $this->context->controller->addJS($this->_path.'views/js/front.js');
                
            }
        }
        return false;
    }
    
    public function hookDisplayProductButtons($params)
    {   
        if (Configuration::get('DEVISMOD_MODE') == true) {
            return $this->display(__FILE__, 'button.tpl');
           
        }
        return false;
    }


   public function hookDisplayFooterProduct($params)
    {   
        if (Configuration::get('DEVISMOD_MODE') == true) {
            $address = false;
            if ($this->context->customer->isLogged()) {
               
                $id_address = (int)Address::getFirstCustomerAddressId((int)$this->context->customer->id);
                $address = new Address($id_address);
                if (!Validate::isLoadedObject($address)) {
                    $address = false;

                } else {
                    $address->address1 = ($address->address2) ? $address->address1.chr(13).$address->address2 : $address->address1;
                }
            }
            $this->context->smarty->assign(array(
                'product' => $params['product'],
                'customer' => ($this->context->customer->isLogged()) ? $this->context->customer : false,
                'address' => $address,
                'cover' => Product::getCover((int)$params['product']->id),
                'terms' => Configuration::get('DEVISMOD_TERMS', (int)$this->context->language->id)
            ));
            return $this->display(__FILE__, 'form.tpl');
        }
        return false;
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false) {
            return false;
        }

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'payment.tpl');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        
        $order = $params['objOrder'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR')) {
            $this->smarty->assign('status', 'ok');
        }

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'confirmation.tpl');
    }

    public function hookActionPaymentConfirmation()
    {
        /* Place your code here. */
    }

    public function hookDisplayAdminOrderContentOrder()
    {
        /* Place your code here. */
    }

    public function hookDisplayPaymentReturn()
    {
        /* Place your code here. */
    }
}
