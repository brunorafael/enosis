{**
 * redirect.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}

<div>
	<h3>{l s='Redirect your customer' mod='devismod'}:</h3>
	<ul class="alert alert-info">
			<li>{l s='This action should be used to redirect your customer to the website of your payment processor' mod='devismod'}.</li>
	</ul>
	
	<div class="alert alert-warning">
		{l s='You can redirect your customer with an error message' mod='devismod'}:
		<a href="{$link->getModuleLink('devismod', 'redirect', ['action' => 'error'], true)|escape:'htmlall':'UTF-8'}" title="{l s='Look at the error' mod='devismod'}">
			<strong>{l s='Look at the error message' mod='devismod'}</strong>
		</a>
	</div>
	
	<div class="alert alert-success">
		{l s='You can also redirect your customer to the confirmation page' mod='devismod'}:
		<a href="{$link->getModuleLink('devismod', 'confirmation', ['cart_id' => $cart_id, 'secure_key' => $secure_key], true)|escape:'htmlall':'UTF-8'}" title="{l s='Confirm' mod='devismod'}">
			<strong>{l s='Go to the confirmation page' mod='devismod'}</strong>
		</a>
	</div>
</div>
