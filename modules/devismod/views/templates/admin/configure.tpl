{**
 * configure.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}
 <div class="form-horizontal col-lg-10 config_row" id="config_row_configure">
{$form_config}
</div>