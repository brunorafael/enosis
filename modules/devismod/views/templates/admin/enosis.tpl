{**
 * enosis.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}

<div class="panel">
	<div class="row devismod-header">
	<div class="col-md-1"></div>
		<img src="{$module_dir|escape:'html':'UTF-8'}views/img/enosis-devis_logo.png" class="col-xs-6 col-md-3 text-center" id="payment-logo" />
		<div class="col-xs-6 col-md-4 text-center">
			<h4>{l s='Online payment processing' mod='devismod'}</h4>
			<h4>{l s='Fast - Secure - Reliable' mod='devismod'}</h4>
		</div>
		<div class="col-xs-12 col-md-4 text-center">
			<a href="#" onclick="javascript:return false;" class="btn btn-primary" id="create-account-btn">{l s='Create an account now!' mod='devismod'}</a><br />
			{l s='Already have an account?' mod='devismod'}<a href="#" onclick="javascript:return false;"> {l s='Log in' mod='devismod'}</a>
		</div>
	</div>
	<hr />	
</div>

<div class="row">
    <div class="list-group col-lg-2" id="menuDevismod">
		<a class="list-group-item" id="link_documents" href="javascript:displayConfigRow('documents');"><i class="icon-book"></i>{$module_tabs['Documents'].name|escape:'html':'UTF-8'}</a>
		<a class="list-group-item" id="link_configure" href="javascript:displayConfigRow('configure');"><i class="icon-cogs"></i>{$module_tabs['Settings'].name|escape:'html':'UTF-8'}</a>
		<a class="list-group-item" id="link_form" href="javascript:displayConfigRow('form');"><i class="icon-edit"></i>{$module_tabs['Form'].name|escape:'html':'UTF-8'}</a>
		<a class="list-group-item" id="link_quotations" href="javascript:displayConfigRow('quotations');"><i class="icon-list"></i>{$module_tabs['ListOfQuotations'].name|escape:'html':'UTF-8'}</a>
	</div>
	<div id="config_content">
	    {include file='./documents.tpl'}
	    {include file='./configure.tpl'}
	    {include file='./form.tpl'}
	    {include file='./quotations.tpl'}
	</div>
</div>
