{**
 * configure.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}
 
 <div class="form-horizontal col-lg-10 config_row" id="config_row_quotations">
	<div class="panel">
		<h3><i class="icon icon-list"></i> {l s='Quotations' mod='devismod'}</h3>
	<p>
	    &raquo; {l s='Content of the quotation list' mod='devismod'}
	</p>
		
	</div>
</div>