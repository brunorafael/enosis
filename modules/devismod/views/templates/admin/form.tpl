{**
 * formulaire.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}
<div class="form-horizontal col-lg-10 config_row" id="config_row_form">
	<div class="panel">
		<h3><i class="icon icon-edit"></i> {l s='Form' mod='devismod'}</h3>
	<p>
	    &raquo; {l s='Content of the form' mod='devismod'}
	</p>
		
	</div>
</div>