{**
 * configure.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}
<div class="form-horizontal col-lg-10 config_row" id="config_row_documents">
	<div class="panel">
		<h3><i class="icon icon-book"></i> {l s='Documentation' mod='devismod'}</h3>
		<p>
			&raquo; {l s='You can get a PDF documentation to configure this module' mod='devismod'} :
			<ul>
				<li><a href="#" target="_blank">{l s='English' mod='devismod'}</a></li>
				<li><a href="#" target="_blank">{l s='French' mod='devismod'}</a></li>
			</ul>
		</p>
	</div>
</div>