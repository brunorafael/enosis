{**
 * button.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}
 
<p class="devismod_link">
	<a href="#devismod_form" class="exclusive" style="width: 240px;">
		<span>{l s='Ask for a quote' mod='devismod'}</span>
	</a>
</p>