{**
 * form.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}

<div style="display:none">
	<div id="devismod_form">
			<h2>{l s='Contact us about this' mod='devismod'}</h2>
			<div class="product clearfix">
				{if $cover}<img src="{$link->getImageLink($product->link_rewrite, intval($cover['id_image']), 'home_default')|escape:'htmlall':'UTF-8'}" alt="{$product->name|escape:'htmlall':'UTF-8'}" class="col-md-6" />{/if}
				<div class="product_desc col-md-6">
					<p class="product_name"><strong>{$product->name|escape:'html':'UTF-8'}</strong></p>
					{$product->description_short}
					{if $product->show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<p class="price_container"><span class="price">{if !$priceDisplay}{convertPrice price=$product->getPrice()}{else}{convertPrice price=$product->getPrice(false)}{/if}</span></p>{else}<div style="height:21px;"></div>{/if}
				</div>
			</div>			
			<div class="devismod_form_content">
				<div class="devismod_waiting"><img src="{$img_ps_dir|escape:'htmlall':'UTF-8'}loadingAnimation.gif" alt="{l s='Please wait' mod='devismod'}" /></div>
				<div id="devismod_form_error"></div>
				<form action="{$link->getModuleLink('devismod', 'form')}" method="post" id="devismod_frm" enctype="multipart/form-data">
					<div class="devismod_form_container">
						<div class="intro_form">{$terms}</div>
						<div class="left">
						<p class="devismod_form">
							<label for="devismod_type">{l s='You are' mod='devismod'} <sup class="required">*</sup> :</label>
							<select id="devismod_type" name="devismod_type" />
								<option>{l s='Customer' mod='devismod'}</option>
								<option>{l s='Company' mod='devismod'}</option>
								<option>{l s='Individual' mod='devismod'}</option>
								<option>{l s='Reseller' mod='devismod'}</option>
							</select>
						</p>
						<p class="devismod_form">
							<label for="devismod_company">{l s='Company' mod='devismod'} :</label>
							<input id="devismod_company" name="devismod_company" type="text" value="{if $address}{$address->company|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_activity">{l s='Business sector' mod='devismod'} :</label>
							<input id="devismod_activity" name="devismod_activity" type="text" value="" />
						</p>
						<p class="devismod_form">
							<label for="devismod_firstname">{l s='Firstname' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_firstname" name="devismod_firstname" type="text" value="{if $customer}{$customer->firstname|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_lastname">{l s='Lastname' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_lastname" name="devismod_lastname" type="text" value="{if $customer}{$customer->lastname|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_address">{l s='Address' mod='devismod'} :</label>
							<textarea cols="60" rows="3" name="devismod_address" id="devismod_address">{if $address}{$address->address1|escape:'htmlall':'UTF-8'}{/if}</textarea>
						</p>
						<p class="devismod_form">
							<label for="devismod_postcode">{l s='Postal Code' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_postcode" name="devismod_postcode" type="text" value="{if $address}{$address->postcode|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_city">{l s='City' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_city" name="devismod_city" type="text" value="{if $address}{$address->city|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_country">{l s='Country' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_country" name="devismod_country" type="text" value="{if $address}{$address->country|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						</div>
						<div class="right">
						<p class="devismod_form">
							<label for="devismod_dni">{l s='DNI / NIF / NIE' mod='devismod'} :</label>
							<input id="devismod_dni" name="devismod_dni" type="text" value="{if $address}{if $address->dni}{$address->dni|escape:'htmlall':'UTF-8'}{/if}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_phone">{l s='Phone' mod='devismod'} :</label>
							<input id="devismod_phone" name="devismod_phone" type="text" value="{if $address}{if $address->phone_mobile}{$address->phone_mobile|escape:'htmlall':'UTF-8'}{else}{$address->phone|escape:'htmlall':'UTF-8'}{/if}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_fax">{l s='Fax' mod='devismod'} :</label>
							<input id="devismod_fax" name="devismod_fax" type="text" value="" />
						</p>
						<p class="devismod_form">
							<label for="devismod_email">{l s='Email' mod='devismod'} <sup class="required">*</sup> :</label>
							<input id="devismod_email" name="devismod_email" type="text" value="{if $customer}{$customer->email|escape:'htmlall':'UTF-8'}{/if}" />
						</p>
						<p class="devismod_form">
							<label for="devismod_file">{l s='Attachment' mod='devismod'} :</label>
							<input id="devismod_file" name="devismod_file" type="file" value="" />
							<input id="devismod_filename" name="devismod_filename" type="hidden" value="" />
						</p>
						<p class="devismod_form">
							<label for="devismod_comment">{l s='Comment' mod='devismod'} <sup class="required">*</sup> :</label>
							<textarea cols="60" rows="3" name="devismod_comment" id="devismod_comment"></textarea>
						</p>
						</div>
						<p class="txt_required"><sup class="required">*</sup> {l s='Required fields' mod='devismod'}</p>
					</div>
					<p class="submit">
						<input id="devismod_product" name="devismod_product" type="hidden" value="{$product->id|escape:'htmlall':'UTF-8'}" />
						<input type="button" class="devismod_cancel" value="{l s='Cancel' mod='devismod'}" />&nbsp;{l s='or' mod='devismod'}&nbsp;
						<input id="devismod_submit" class="button" name="devismod_submit" type="submit" value="{l s='Send' mod='devismod'}" />
					</p>
				</div>
			</form>
	</div>
</div>