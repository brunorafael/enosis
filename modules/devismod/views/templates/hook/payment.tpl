{**
 * payment.tpl.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 *}

<div class="row">
	<div class="col-xs-12 col-md-6">
		<p class="payment_module" id="devismod_payment_button">
			{if $cart->getOrderTotal() < 2}
				<a href="">
					<img src="{$domain|cat:$payment_button|escape:'html':'UTF-8'}" alt="{l s='Pay with my payment module' mod='devismod'}" />
					{l s='Minimum amount required in order to pay with my payment module:' mod='devismod'} {convertPrice price=2}
				</a>
			{else}
				<a href="{$link->getModuleLink('devismod', 'redirect', array(), true)|escape:'htmlall':'UTF-8'}" title="{l s='Pay with my payment module' mod='devismod'}">
					<img src="{$module_dir|escape:'htmlall':'UTF-8'}/logo.png" alt="{l s='Pay with my payment module' mod='devismod'}" width="32" height="32" />
					{l s='Pay with my payment module' mod='devismod'}
				</a>
			{/if}
		</p>
	</div>
</div>
