/**
 * back.js.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 */

$(function(){

// Action in Menu Admin
	var adminMenu = $('.menu').children();
	
	adminMenu.on('click', function(){
		adminMenu.removeClass('active');
		$(this).addClass('active');
	})

	if(getUrl('configure') == 'devismod'){
	adminMenu.removeClass('active');
	$('#maintab-AdminDevismodTab').addClass('active');
	}
	
// Message alert.
	setInterval(function(){
		$('.alert').fadeOut('slow');
	}, 5000);

// show only content configuration module.
	$('.config_row').hide();
	$('#config_row_configure').show();
	$('#link_configure').addClass('active');
})

function displayConfigRow(row)
{
	$('.config_row').hide();
	$('.list-group-item.active').removeClass('active');
	$('#config_row_' + row).show();
	$('#link_' + row).addClass('active');
	$('#currentConfigRow').val(row);
}

function getUrl(param){

	console.log(param);
	var vars = {};
	console.log(window.location.href);
	window.location.search.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp

		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);
	return vars[param];
}
