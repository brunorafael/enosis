/**
 * back.js.
 *
 * @author    Bruno Rafael
 * @copyright 2016 Bruno Rafael
 * @license   license.txt
 *
 * 2016 Bruno Rafael - All rights reserved.
 *
 * DISCLAIMER
 * Changing this file will render any support provided by us null and void.
 */

function ajaxFileUpload()
{
	$.ajaxFileUpload
	({
		url:$("#devismod_frm").attr("action"), 
		secureuri:false,
		fileElementId:'devismod_file',
		dataType: 'json',
		success: function (data, status)
		{
			if (data.success == false) {
				var errors = '';
				for (var i = 0; i < data.errors.length; i++) {
					errors = errors + '<li>' + data.errors[i] + '</li>';
				}
				$("#devismod_form_error").html('<ol class="errors">'+errors+'</ol>');
				$('#devismod_submit').removeAttr('disabled');
				$('.devismod_waiting').hide();
			} else {
				$("#devismod_filename").val(data.success);
				devismodSend();
			}
		}
	});
}
function devismodSend() {

	$.post($("#devismod_frm").attr("action"), $("#devismod_frm").serialize()).done(function(data) {
		$('.devismod_waiting').hide();
		var result = jQuery.parseJSON(data);
		if (result.success == false) {
			var errors = '';
			for (var i = 0; i < result.errors.length; i++) {
				errors = errors + '<li>' + result.errors[i] + '</li>';
			}
			$("#devismod_form_error").html('<ol class="errors">'+errors+'</ol>');
			$('#devismod_submit').removeAttr('disabled');
		} else {
			$("#devismod_form_error").html('<p class="success">'+result.success+'</p>');
			$("#devismod_frm").hide();
		}
	});
}

$(document).ready(function(){	

		$('.devismod_link a').fancybox({
				closeClick  : false,
			});
		
	
	$('.devismod_cancel').click(function(){
		$.fancybox.close();
	});

	$('#devismod_submit').click(function(){
		$('#devismod_submit').attr('disabled', 'disabled');
		$("#devismod_form_error").empty();
		$('.devismod_waiting').show();
		if ($("#devismod_file").val()) 
			ajaxFileUpload();
		else
			devismodSend();
		$('html, body').animate({ scrollTop: $("#devismod_form").offset().top }, 500);
		return false;
	});
});