<?php
 
include_once(dirname(__FILE__).'../../../orderfiles.php');
class orderfilescustomerfilesModuleFrontController extends ModuleFrontController{	
	public function initContent(){
		$module=new orderfiles();
		$this->var=$module->getconf();
		parent::initContent();
        
        global $smarty;
        $ext=explode(",",Configuration::get('OF_FTYPES'));
        $smarty->assign('extensions',$ext);
		
		if (isset($_POST['delfile'])){
			if (isset($_POST['fid'])){
			    $this->photodeletecustomer($_POST['fid'],$this->context->customer->id);
			}
		}
        
        if (isset($_POST['savefile'])){
			if ($_POST['pty']=="customer"){
		        $module->updatefilecustomer($_POST['fid'],mysql_escape_string($_POST['description']),mysql_escape_string($_POST['title']));
			}
		}        
        
		if (isset($this->context->customer->id)){
		
				$this->context->smarty->assign(array(
				'mod' => $this,
				'setup' => $this->var, 
				'order' => $order,
				'files' => $this->get_files($this->context->customer->id),
				'link' => $this->context->link,
				'customer' => $this->context->customer,
                'idcustomer' => $this->context->customer->id));
                
				$this->setTemplate('customerfilesmanager.tpl');
		} 
        else 
        {
			$this->setTemplate('access-denied.tpl');
		}
	}	
	
	public function currency_sign($id){
		$currency=new CurrencyCore($id);
		return $currency->sign;
	}
	
	public function get_files($customer){
	    $db = Db::getInstance(_PS_USE_SQL_SLAVE_); 
        $query = "SELECT * FROM `"._DB_PREFIX_."orderfiles_customers` WHERE idcustomer='$customer'";
        $array['tocustomer'] = $db->ExecuteS($query);
        return $array;
	}
	
    public function generatekey($length = 5, $chars) {
        $last="";
        $validCharacters = $chars;
        $validCharNumber = strlen($validCharacters);
        $result = "";
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            while ($last==$index){
                $index = mt_rand(0, $validCharNumber - 1);        
            }
            $result .= $validCharacters[$index];
            $last = $index;
        }
        return $result;
    }
    
    public function photodeletecustomer($idphoto,$idcustomer){
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_); 
        $query = "SELECT * FROM `"._DB_PREFIX_."orderfiles_customers` WHERE id='$idphoto' AND idcustomer='$idcustomer'";
        $array = $db->ExecuteS($query);
        if (isset($array['0'])){
            $array['0']['filetype']=pathinfo($array['0']['filename'], PATHINFO_EXTENSION);            
            unlink("modules/orderfiles/customerfiles/{$array['0']['filename']}");
            $db = Db::getInstance(); 
            $query = "DELETE FROM `"._DB_PREFIX_."orderfiles_customers` WHERE id='$idphoto' AND idcustomer='$idcustomer'";
            $db->Execute($query);     
        }
    }    	
	
	public function extension($filename){
		return pathinfo($filename, PATHINFO_EXTENSION);
	}			
}