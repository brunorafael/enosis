<?php
include(_PS_MODULE_DIR_.'orderfiles/orderfiles.php');

class customerFilesController extends ModuleAdminController {
    public $msg;
    public function __construct(){
        $this->orderfiles=new orderfiles();
        $this->bootstrap = true;
        
  	    parent::__construct();
    }
    
    public function delete($id){
        $sql = 'DELETE FROM `'._DB_PREFIX_.'orderfiles_customers` WHERE id = "'.$id.'"';
    	Db::getInstance()->execute($sql);
        return $this->l('removed');
    }
    
    
    public function renderList(){
        
  		if (Tools::isSubmit('deleteorderfiles_customers') != false){
  			$this->msg=$this->delete(Tools::getValue('id'));	
  		}
        
        	$fields_list = array(
			'id' => array(
				'title' => $this->l('File ID'),
				'type' => 'text',
			),
			'customer' => array(
				'title' => $this->l('Customer'),
				'type' => 'text',
			),
            'email' => array(
				'title' => $this->l('Email'),
				'type' => 'text',
			),
            'filename' => array(
				'title' => $this->l('File name'),
				'type' => 'text',
			),
            'title' => array(
				'title' => $this->l('Title'),
				'type' => 'text',
			),
            'description' => array(
				'title' => $this->l('Description'),
				'type' => 'text',
			),
		);
        
        global $cookie;
		$helper = new HelperList();
		$helper->shopLinkType = '';
        $helper->simple_header = true;
		$helper->identifier = 'id';
		$helper->actions = array('delete','details');
		$helper->show_toolbar = false;
		$helper->table = "orderfiles_customers";
		$helper->token = Tools::getAdminTokenLite('customerFiles');
		$helper->currentIndex = AdminController::$currentIndex;
        $helper->module = $this->orderfiles;
		$files = $this->getLinks();
		return "<div style=\"clear:both; display:block;margin-bottom:20px; s\">".$this->msg."</div>".$helper->generateList($files, $fields_list);
        
    }
    
    
	public function getLinks()
	{
		$result = array();
		// Get id and url

		$sql = 'SELECT * FROM `'._DB_PREFIX_.'orderfiles_customers` ORDER BY id DESC';
    	$files = Db::getInstance()->executeS($sql);

		$i = 0;
		foreach ($files as $file)
		{
		    $customer = new Customer($file['idcustomer']);
			$result[$i]['id'] = $file['id'];
            $result[$i]['customer'] = $customer->firstname." ".$customer->lastname;
            $result[$i]['email'] = $customer->email;
            $result[$i]['filename'] = $file['filename'];
            $result[$i]['title'] = $file['title'];
            $result[$i]['description'] = $file['description'];
            $i++;
		}

		return $result;
	}

    
    
    public function deletecustomersfile($idfile){
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_); 
        $query = "SELECT * FROM `"._DB_PREFIX_."orderfiles_customers` WHERE id='$idfile'";
        $array = $db->ExecuteS($query);
        if (isset($array['0'])){        
            unlink("../modules/customerfiles/{$array['0']['filename']}");
            $db = Db::getInstance(); 
            $query = "DELETE FROM `"._DB_PREFIX_."orderfiles_customers` WHERE id='$idfile'";
            $db->Execute($query);     
        }
    }
    
	public function extension($filename){
		return pathinfo($filename, PATHINFO_EXTENSION);
	}
  	
  	public function lastfiles(){
  	    $ret=''; 
  		$db = Db::getInstance(_PS_USE_SQL_SLAVE_); 
        $query = "SELECT * FROM `"._DB_PREFIX_."orderfiles` ORDER BY id DESC LIMIT 6";
        $array = $db->ExecuteS($query);
        if (count($array)>0){
        	$ret.= '<h4>'.$this->l('Last files').'</h4>';
        	foreach ($array as $key=>$file){
        		$ordercore=new OrderCore($file['id_order']);
        		$orders=$ordercore->getOrdersWithInformations();
        		$ret.= '
				<div style="margin:5px; padding:5px; display:inline-block; border:1px solid #c0c0c0;">
				<a href="../modules/orderfiles/files/'.$file['id_order'].'/'.$file['filename'].'" target="_blank">
				'.$file['filename'].'<br/><b>'.$orders[0]['email'].'</b>
				</a>
				</div>';        		
        	}
        }
        return $ret;
  	}
  	

    public function generatekey($length = 5, $chars) {
        $last="";
        $validCharacters = $chars;
        $validCharNumber = strlen($validCharacters);
        $result = "";
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            while ($last==$index){
                $index = mt_rand(0, $validCharNumber - 1);        
            }
            $result .= $validCharacters[$index];
            $last = $index;
        }
        return $result;
    }
}
?>