{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My account' mod='orderfiles'}</a><span class="navigation-pipe">{$navigationPipe}</span><span class="navigation-pipe">{$navigationPipe}</span>{l s='Files manager' mod='orderfiles'}{/capture}



{* 



{include file="$tpl_dir./breadcrumb.tpl"}



 *}



<h2 style="margin-top:20px;">{l s='Files manager' mod='orderfiles'}</h2>



    <div class="filesmanager_contents">



	{if empty($files['tocustomer'])}



		<p class="alert alert-info">{l s='No files' mod='orderfiles'}</p>



	{else}



		{foreach from=$files['tocustomer'] key=id item=file}



        <div class="bootstrap">



            <form method="post" action="{$link->getModuleLink('orderfiles', 'customerfiles')}">



    			<div style="{if isset($smarty.post.pty)}{if $smarty.post.pty=="order" && $smarty.post.fid==$file.id && isset($smarty.post.editfile)}border:2px solid blue;{else}border:1px solid #c0c0c0;{/if}{else}border:1px solid #c0c0c0;{/if} padding:10px; position:relative; display:block; clear:both; overflow:hidden; margin-bottom:10px;">



                    {if $file.adminfile==1}<div class='alert alert-info'><!--{l s='This file was uploaded by admin' mod='orderfiles'}-->Le document à été envoyé par ENOSIS THERMIQUE</div>{/if}



    				<img src="{$modules_dir}orderfiles/img/file.png" style="display:inline-block; float:left; margin-right:10px;"/>



    				<div style="display:inline-block; float:left; padding-bottom:30px;">



    					{if isset($smarty.post.pty)}{if $smarty.post.pty=="customer" && $smarty.post.fid==$file.id && isset($smarty.post.editfile)}{l s='title' mod='orderfiles'}<br/><input type="text" name="title" value="{$file.title}"/>{else}<b>{$file.title}</b>{/if}{else}<b>{$file.title}</b>{/if} {$file.filename} - <a href="{$content_dir}modules/orderfiles/download.php?t=customerfiles&cpt={$idcustomer}&f={$file.filename}" target="_blank"><strong>{l s='download' mod='orderfiles'}</strong></a>



    					<p style="margin-top:5px; display:block; clear:both; width:420px; line-height:20px;">{if isset($smarty.post.pty)}{if $smarty.post.pty=="customer" && $smarty.post.fid==$file.id && isset($smarty.post.editfile)}{l s='description' mod='orderfiles'}<br/><textarea name="description">{$file.description}</textarea>{else}{$file.description} &nbsp;{/if}{else}{$file.description} &nbsp;{/if}</p>



    						<input type="hidden" name="cid" value="{$idcustomer}"/>



    						<input type="hidden" name="fid" value="{$file.id}"/>



                            <input type="hidden" name="pty" value="customer"/>



							<!-- LAURA // mise en comm
                            <div style="position:absolute; right:10px; bottom:10px;">

                                {if isset($smarty.post.pty)}{if $smarty.post.pty=="customer" && $smarty.post.fid==$file.id && isset($smarty.post.editfile)}<input type="submit" name="savefile" value="{l s='Save' mod='orderfiles'}" class="button" />{/if}{/if}

                                <input type="submit" name="editfile" value="{l s='Edit' mod='orderfiles'}" class="button" />

        						<input type="submit" name="delfile" value="{l s='Delete' mod='orderfiles'}" class="button"/>

                            </div>-->


    				</div>



    			</div>



            </form>



        </div>



		{/foreach}



	{/if}



    </div>







{if Configuration::get('OF_AJAXUPLOAD')==1}



        <script type="text/javascript">



            {literal}



                $(function() {



                    var uploadObj = $("#fileuploader").uploadFile({



                        url:baseDir+"modules/orderfiles/ajax/upload.php",



                        multiple:true,



                        autoSubmit:true,



                        fileName:"file",



                        maxFileSize:{/literal}{Configuration::get('OF_MAX_FILE_SIZE')}{literal}*1024,



                        allowedTypes:"{/literal}{if $extensions!=""}{foreach from=$extensions item=ext name=loop}{$ext}{if !$smarty.foreach.loop.last},{/if}{/foreach}{else}*{/if}{literal}",



                        showStatusAfterSuccess:true,



                        formData: {"cid":"{/literal}{$idcustomer}{literal}","auptype":"customer"},



                        dragDropStr: "<span><b>{/literal}{l s='Drag & Drop files here' mod='orderfiles'}{literal}</b></span>",



                        abortStr:"{/literal}{l s='Abort' mod='orderfiles'}{literal}",



                        cancelStr:"{/literal}{l s='Cancel' mod='orderfiles'}{literal}",



                        doneStr:"{/literal}{l s='Done!' mod='orderfiles'}{literal}",



                        multiDragErrorStr: "{/literal}{l s='Several Drag & Drop files are not allowed.' mod='orderfiles'}{literal}",



                        extErrorStr:"{/literal}{l s='is not allowed. Allowed extensions:' mod='orderfiles'}{literal}",



                        sizeErrorStr:"{/literal}{l s='is not allowed. Allowed max size:' mod='orderfiles'}{literal}",



                        uploadErrorStr:"{/literal}{l s='Upload is not allowed' mod='orderfiles'}{literal}",



                        onSuccess:function(files,obj,xhr,pd)



                        {



                        	$('.filesmanager_contents').append(obj);



                        }



                        });



                });



            {/literal}



        </script>



        <div id="fileuploader">{l s='Upload' mod='orderfiles'}</div>



{/if}