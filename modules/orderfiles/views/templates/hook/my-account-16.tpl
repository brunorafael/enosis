{if Configuration::get("OF_CACCOUNT")==1}
<li><a href="{$link->getModuleLink('orderfiles', 'myfiles')}" title="{l s='Upload own files for your orders' mod='orderfiles'}"><i class="icon-cloud-upload"></i><span>{l s='Files uploader' mod='orderfiles'} {l s='(orders)' mod='orderfiles'}</span></a></li>
{/if}

{if Configuration::get("OF_ADDUPLOADFORM")==1}
<li><a href="{$link->getModuleLink('orderfiles', 'customerfiles')}" title="{l s='Upload files not associated with your orders' mod='orderfiles'}"><i class="icon-cloud-upload"></i><span>{l s='Files uploader' mod='orderfiles'} {l s='(custom files)' mod='orderfiles'}</span></a></li>
{/if}