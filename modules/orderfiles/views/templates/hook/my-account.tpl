{if Configuration::get("OF_CACCOUNT")==1}

<li><a href="{$link->getModuleLink('orderfiles', 'myfiles')}" title="{l s='Upload own files for your orders' mod='orderfiles'}">{l s='Files uploader' mod='orderfiles'}</a></li>

{/if} 

{if Configuration::get("OF_ADDUPLOADFORM")==1}

<li><a href="{$link->getModuleLink('orderfiles', 'customerfiles')}" title="{l s='Upload own files' mod='orderfiles'}">{l s='Files uploader (not to orders)' mod='orderfiles'}</a></li>

{/if} 