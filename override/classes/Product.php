<?php

// BOOBEAR
class Product extends ProductCore
{
    public $bbio;
	public $etude_reg;
	public $controle_travaux;
	
    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
      Product::$definition['fields']['bbio'] = array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString');
	  Product::$definition['fields']['etude_reg'] = array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString');
	  Product::$definition['fields']['controle_travaux'] = array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString');
      parent::__construct($id_product, $full, $id_lang, $id_shop, $context);
    }
	
}



?>